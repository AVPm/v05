<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class crops/and resizes images without losing the important parts of images!
 * It calculates horizontal and vertical images centeral points and then crops or resizes images with that ratio
 * This class is a CodeIgniter Controller class but you can convert it to any other classes you wish easily
 * 
 * info@fouad.ir
 */

class Image_cropper
{
	
	/**
	 * Image processor
	 * @param int $nw, Resize to this width
	 * @param int $nh, Resize to this height
	 * @param string $source, Image name on the server
	 * @param string $stype, Source image type (jpg, etc)
	 * @param string $type, big: you want to crop the original image, else: you want to resize the image
	 */
	function preview($nw, $nh, $source, $filename, $stype, $type = "little") {
		header('Content-type: image/jpg');
		
		$nw = (int)$nw;
		$nh = (int)$nh;
		
		$source = constant('FILE_INCLUDE_PATH').'/uploads/' . $source;
                
               // print_r($source) ;
               // die();
		
		$size = getimagesize($source);
		
		$w = $size[0];
		$h = $size[1];
		switch($stype)
		{
			case 'gif':
				$simg = imagecreatefromgif($source);
				break;
			case 'jpg':
                        case 'jpeg':
				$simg = imagecreatefromjpeg($source);
				break;
			case 'png':
				$simg = imagecreatefrompng($source);
				break;
		}
		$dimg = imagecreatetruecolor($nw, $nh);
		
		/**
		 * Here we get the center of image
		 */
		$wm = $w/$nw;
		$hm = $h/$nh;
		$h_height = $nh/2;
		$w_height = $nw/2;
		$ow = ($nw - $w) / 2;
		$oh = ($nh - $h) / 2;		
		if($type == "big")
		{
			imagecopy($dimg,$simg,0,0,-$ow,-$oh,$nw,$nh);
			imagejpeg($dimg);
		}
		else
		{
			/**
			 * If image is horizontal
			 */
			if($w> $h)
			{
				$adjusted_width = $w / $hm;
				$half_width = $adjusted_width / 2;
				$int_width = $half_width - $w_height;
				imagecopyresampled($dimg,$simg,-$int_width,0,0,0,$adjusted_width,$nh,$w,$h);
			}
			/**
			 * If image is vertical
			 */
			elseif(($w <$h) || ($w == $h))
			{
				$adjusted_height = $h / $wm;
				$half_height = $adjusted_height / 2;
				$int_height = $half_height - $h_height;
				imagecopyresampled($dimg,$simg,0,-$int_height,0,0,$nw,$adjusted_height,$w,$h);
			}
			else
			{
				imagecopyresampled($dimg,$simg,0,0,0,0,$nw,$nh,$w,$h);
			}
			//imagejpeg($dimg, './uploads/'.$filename.'_thumb'.'jpg');
                        imagejpeg($dimg, constant('FILE_INCLUDE_PATH').'/uploads/image_thumb.jpg');
		}
	}
}