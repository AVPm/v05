<?php
// ****************************************************************************
// 
//     User's project videos view
//
// ****************************************************************************
//session_start();
$sessionid=session_id();
$currAlbumId='';
$albumInfoArr=  array();
$userid=($this->session->userdata('user_id')>0) ? $this->session->userdata('user_id') : '';
if(!empty($albumsArr)){
    $newarr = current($albumsArr);
    $currAlbumId=$newarr['id'];
    
    foreach($albumsArr as $id => $value){
        if($value['id']==$curr_albumid){
            $albumInfoArr['thumbnail']=$value['thumbnail'];
            $albumInfoArr['name']=$value['name'];
            //$albumInfoArr['description']=$value['description'];
        }
    }
}


?>
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>/css/project_page.css"  />
<script type="text/javascript" src="<?=base_url(); ?>/js/script_supersized_mod.js"></script>
<script type="text/javascript" src="<?=base_url(); ?>/js/pixastic.js"></script>
<script src="<?=base_url(); ?>/js/avp_new.js" type="text/javascript"></script>
    <script src="<?=base_url(); ?>/js/jquery.ui.touch-punch.min.js"></script>
<script>
    var curr_project='<?=@$projectId; ?>';
        var curr_album='<?=@$curr_albumid; ?>';
        var base_url='<?=base_url(); ?>';  
        var imageid='<?=@$videoid; ?>';
        var pageAction='webpageupload';
        var image_version="";
        var imageid_version="";
        var upload_url= "<?=base_url(); ?>project/album/webpageupload";	// Relative to the SWF file
        var V04_PATH='<?=V04_PATH; ?>';  
</script>
<script>
    $(document).ready(function(){
        resizeDiv();
        $("#albumlisting").niceScroll();
        $("#commentListing").niceScroll();
        $("#imagesListing").niceScroll({horizrailenabled: false});
    });
    
    $( window ).resize(function() {
        resizeDiv();
    });
</script>
<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<script type="text/javascript">
    window.onload = function()
    {
        CKEDITOR.replace( 'comment', {
                removePlugins: 'bidi,div,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley/maximize',
                //removeButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',
                format_tags: 'p;h1;h2;h3;pre;address',
                height: 250

        } );
        
        CKEDITOR.replace( 'comment_edit', {
                removePlugins: 'bidi,div,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley/maximize',
                //removeButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',
                format_tags: 'p;h1;h2;h3;pre;address',
                height: 250

        } );
        

        CKEDITOR.on('dialogDefinition', function( ev ){
            var dialogName = ev.data.name;
            var dialogDefinition = ev.data.definition;
            if ( dialogName == 'link' ){
                // Get a reference to the "Target" tab.
                        var targetTab = dialogDefinition.getContents( 'target' ); 
                        // Set the default value for the target field.
                        var targetField = targetTab.get( 'linkTargetType' );
                        targetField['default'] = '_blank';
                 }
        });
    }

    //////////end of validations////////////////
</script>

<?php
// add album popup
    if($userid>0):
?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="add_album" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Add Folder</h4>
                        </div>
                        <div class="modal-body">
                                <form action="" enctype="multipart/form-data" method="post" name="addfolder" id="addfolder">
                                <?php
                                   /*
                                    * hidden field with value of project id
                                    */
                                       $data = array(
                                       'name'        => 'projectid_hidden',
                                       'id'          => 'projectid_hidden',
                                       'value'       => $projectId,
                                       'type'        => 'hidden',
                                       );

                                       echo form_input($data);

                                       //echo form_label('Album Name: ', 'album_name'); 
                                       echo '<label class="control-label">Album Name:</label>';
                                       echo '<br>';
                                       $data = array(
                                                       'name'        => 'folder_name',
                                                       'id'          => 'folder_name',
                                                       'class'  =>  'form-control',
                                                     );
                                       echo form_input($data);

                                       ?>
                                       <br>
                                       <label class="control-label">Thumbnail:</label><br>
                                       <input type="file" class="filestyle" data-icon="false" name="thumbnail" id="fileselecter" />
                                       
                                                        
                                       <?php /* Thumbnail Type: <br><input type="radio" onfocus="this.blur();" value="png" name="thumbnail_type" class="radio_thumbnailtype" checked="checked">PNG &nbsp; <input type="radio" onfocus="this.blur();" value="jpg" name="thumbnail_type" class="radio_thumbnailtype">JPG<br>*/ ?>
                               </form>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#addfolder').submit();">Save Folder</button>
                        </div>
                </div>
        </div>
</div>
<?php 
endif; 
// add album popup end
?>

<?php
//edit album popup
    if(!empty($albumInfoArr) && $userid>0):
?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit_album" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Edit Folder</h4>
                                <a style="position: absolute; top: 12px; right: 45px;" class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removealbum(<?=$curr_albumid; ?>, 'videos')">Remove Album</a>
                        </div>
                        <div class="modal-body">
                                <?php

                                    echo form_open_multipart("", array('id'=>'editalbum'));
                                    /*
                                     * hidden field with value of project id
                                     */
                                    $data = array(
                                    'name'        => 'album_hidden',
                                    'id'          => 'album_hidden',
                                    'value'       => $curr_albumid,
                                    'type'        => 'hidden',
                                    );

                                    echo form_input($data);

                                    /*
                                     * input field for album name
                                     */
                                    //echo form_label('Album Name: ', 'edit_album_name'); 
                                    echo '<label class="control-label">Album Name:</label>';
                                    echo '<br>';
                                    $data = array(
                                                    'name'        => 'edit_album_name',
                                                    'id'          => 'edit_album_name',
                                                    'value'       => $albumInfoArr['name'],
                                                    'class'  =>  'form-control',
                                                  );
                                    echo form_input($data);

                                    echo '<br>';

                                   
                                    /*
                                     * hidden field with value of album thumbnail
                                     */
                                    $data = array(
                                    'name'        => 'album_thumbnail_hidden',
                                    'id'          => 'album_thumbnail_hidden',
                                    'value'       => $albumInfoArr['thumbnail'],
                                    'type'        => 'hidden',
                                    );

                                    echo form_input($data);

                                ?>
                                <label class="control-label">Thumbnail:</label><br>
                                       <input type="file" class="filestyle" data-icon="false" name="edit_thumbnail" id="fileselecter" />
                            <br>

                                    <label class="control-label">Remove Current Folder Thumbnail:</label><br>
                                    
                                    <input type="checkbox" value="delete" name="deletethumb" id="deletethumb">
                                       <label class="margin-zero" for="deletethumb"><span></span></label>
                                       <br>
                                
                            <?php echo form_close(); ?>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#editalbum').submit();">Edit Album</button>
                        </div>
                </div>
        </div>
</div>
<?php
    endif; 
//edit album popup ends
?>
<!-- album sorting form start -->
<?php if($userid>0): ?>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="sort_album" class="modal fade modal-full-pad in">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Sort Album</h4>
                        </div>
                        <div class="modal-body">
                                <div id="containmentbox" class="sort_area">
                                    <ul id="sortbox" class="sortable ui-sortable" style="">
                                        <?php
                                        foreach ($albumsArr as $album){
                                            $thumbnail=($album['thumbnail']!='') ? V04_PATH.'uploads/'.$album['thumbnail'] : V04_PATH.'images/imageicon.png';
                                            echo '<li id="id_'.$album['id'].'" style="">
                                                    <img width="160" height="160" class="selector" src="'.$thumbnail.'">
                                                    <div class="album_name">'.$album['name'].'</div>
                                                </li>';
                                        }
                                        ?>

                                     </ul>  
                                       <div class="clear">&nbsp;</div>
                             	
                                   </div>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="saveOrder('webpages');">Save Album</button>
                        </div>
                </div>
        </div>
</div>

<?php endif; ?>
<!-- album images sorting form ends -->

<!-- add comment form start -->
<?php if($userid>0): ?>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addalbumCommentDiv" class="modal fade modal-full-pad in">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Add Comment</h4>
                        </div>
                        <div class="modal-body">
                                <div id="containmentbox" class="sort_area">
                                    <?php
                                    echo form_open("", array('id'=>'addcommentfrm')); 

                                    $data = array(
                                                'name'        => 'album_comment_hidden',
                                                'id'          => 'album_comment_hidden',
                                                //'value'       => $curr_albumid,
                                                'value'       => $videoid,
                                                'type'        => 'hidden',
                                                );

                                    echo form_input($data);
                                    /*            
                                    echo form_label('Stagename: ', 'stagename'); 
                                    echo '<br>';
                                    $data = array(
                                                    'name'        => 'stagename',
                                                    'id'          => 'stagename',
                                                    'value'       => $user_stagename,
                                                    'readonly'    => 'readonly'
                                                  );
                                    echo form_input($data);

                                    echo '<br>';
                                     * 
                                     */

                                   // echo form_label('Comment: ', 'comment'); 
                                    $data = array(
                                            'name'        => 'comment',
                                            'id'          => 'comment',
                                            'rows'        => '7',
                                            'cols'        => '36',
                                          );

                                    echo form_textarea($data);

                                    echo form_close(); 
                                    ?>
                                   </div>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#addcommentfrm').submit();">Save Comment</button>
                        </div>
                </div>
        </div>
</div>

<?php endif; ?>
<!-- add comment form ends -->


<!-- edit comment form start -->
<?php 
$lastCommentArr= array();
foreach ($album_comments as $key => $value) {
    if($videoid==$key){
        $lastCommentArr=$value[0];
        break;
    }
}
if($userid>0 && !empty($lastCommentArr)):

?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="editCommentDiv" class="modal fade modal-full-pad in">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Edit Comment</h4>
                                <a style="position: absolute; top: 12px; right: 45px;" class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removecomment('videos',<?=@$lastCommentArr['id']; ?>)">Remove Comment</a>
                        </div>
                        <div class="modal-body">
                                <div id="containmentbox" class="sort_area">
                                    <?php
                                    echo form_open("", array('id'=>'editcommentfrm'));
                                    $data = array(
                                                'name'        => 'commentid_hidden',
                                                'id'          => 'commentid_hidden',
                                                'value'       => @$lastCommentArr['id'],
                                                'type'        => 'hidden',
                                                );

                                    echo form_input($data);

                                   // echo form_label('Comment: ', 'comment'); 
                                    $data = array(
                                            'name'        => 'comment_edit',
                                            'id'          => 'comment_edit',
                                            'rows'        => '7',
                                            'cols'        => '36',
                                            'value'       => @$lastCommentArr['comment']
                                          );

                                    echo form_textarea($data);

                                    echo form_close(); 
                                    ?>
                                   </div>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#editcommentfrm').submit();">Save Comment</button>
                        </div>
                </div>
        </div>
</div>

<?php endif; ?>
<!-- edit comment form ends -->

<?php
    if($userid>0):
        
?>
<!-- add video in album popup start -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="add_image_popup" class="modal fade modal-full-pad in">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Add Webpage</h4>
                        </div>
                        <div class="modal-body">
                                <div id="containmentbox" class="sort_area">
                                    <form action="" enctype="multipart/form-data" method="post" id="form1">

                                        <div id="fileuploader">
                                            <iframe src="<?=base_url('includes/file_uploader')  ?>" style="height: 250px; width:100%" frameborder="0" ></iframe> 
                                        </div>    


                                    </form>
                                       <div class="clear">&nbsp;</div>
                             
                                   </div>
                        </div>
                        <div class="modal-footer">
                                <button class="btn btn-default" type="button" onclick="window.location.href='/project/webpages/<?=$projectId; ?>/<?=$curr_albumid; ?>';">Close</button>
                                <button class="btn btn-primary" type="button" id="donelink" style="display: none" onclick="window.location.href='<?=base_url(); ?>project/webpages/<?=$projectId; ?>/<?=$curr_albumid; ?>';">Save Images</button>
                        </div>
                </div>
        </div>
</div>
    
<!-- add images in album popup ends -->
<?php
endif;
?>

<?php
//edit album images popup
    if($videoid>0 && $userid>0):
?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit_album_image" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Edit Webpage</h4>
                                <a style="position: absolute; top: 12px; right: 45px;" class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removealbumimage('webpages', '<?=$videoid; ?>')">Delete Webpage</a>
                        </div>
                        <div class="modal-body">
                            <?php 
                            $videotitle=($video_title=='') ? $video_name : $video_title;
                                echo form_open_multipart("", array('id'=>'editimage'));

                                $data = array(
                                            'name'        => 'project_videoid_hidden',
                                            'id'          => 'project_videoid_hidden',
                                            'value'       => $videoid,
                                            'type'        => 'hidden',
                                            );

                                echo form_input($data);

                                //echo form_label('Title: ', 'image_title'); 
                                echo '<label class="control-label">Title:</label>';
                                echo '<br>';
                                $data = array(
                                                'name'        => 'video_title',
                                                'id'          => 'video_title',
                                                'value'       => $videotitle,
                                                'class'  =>  'form-control',
                                              );
                                echo form_input($data);

                                echo '<br>';

                                //echo form_label('additional Info: ', 'additional_info'); 
                                echo '<label class="control-label">Additional Info:</label>';
                                $data = array(
                                        'name'        => 'additional_info',
                                        'id'          => 'additional_info',
                                        'rows'        => '7',
                                        'cols'        => '36',
                                        'value'       => $video_info,
                                        'class'  =>  'form-control',
                                      );

                                    echo form_textarea($data);
                                /*
                                * hidden field with value of project thumbnail
                                */
                               $data = array(
                               'name'        => 'video_thumbnail_hidden',
                               'id'          => 'video_thumbnail_hidden',
                               'value'       => $video_thumb,
                               'type'        => 'hidden',
                               );

                               echo form_input($data);
                               
                               echo '<br>';
                               
                               echo '<label class="control-label">Thumbnail:</label><br>
                                       <input type="file" class="filestyle" data-icon="false" name="edit_thumbnail" id="fileselectersvideothumb" />';
                               
                               echo '<label class="control-label">Remove Current Folder Thumbnail:</label>';

                                $data = array(
                                    'name'        => 'deletethumb',
                                    'id'          => 'deletethumb',
                                    'value'       => 'delete',
                                    );

                                echo form_checkbox($data);  
                                
                                echo '<label class="margin-zero" for="deletethumb"><span></span></label>';
                                
                                echo '<br>';
                                
                            ?>
                            <div style="float: left">
                                <label class="control-label">Width:</label> <input type="text" class="form-control" name="iframe_width" value="<?=$iframe_width; ?>" style="width: 70px;">
                            </div>
                            <div style="float: left; margin-left: 20px;">
                                &nbsp; <label class="control-label">Height:</label> <input type="text" name="iframe_height" class="form-control" value="<?=$iframe_height; ?>" style="width: 70px;">
                            </div>
                            <div class="clear">&nbsp;</div> 
                            <?php echo form_close(); ?>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#editimage').submit();">Edit Webpage</button>
                        </div>
                </div>
        </div>
</div>
<?php
    endif; 
//edit album images popup ends
?>

<div class="col-md-9 nopad-right" id="centerImg" style="padding-left: 7px;">
        <!-- panel -->
        <div class="panel panel-piluku">
            <!--
            <div class="panel-heading">
                <h3 class="panel-title">
                    Webpage

              </h3>
          </div>
            -->
          <div class="panel-body" style="height: 371px;">
              <div class="row main-chart-parent" style="height: 100%">
                <table style="width:100%;height: 100%">
                    <tr>
                        <td align="center" valign="middle" style="vertical-align: middle; height: 100%">
                            <?php
                            
                            if($videoid>0){
                                $foldername=$this->db->select('foldername')->get_where('webpages', array('id' => $videoid))->row()->foldername;
                                //$uploadedUserid=$this->db->select('userid')->get_where('webpages', array('id' => $videoid))->row()->userid;
                                $folder=$foldername.'_'.$videoid;
                                if(file_exists(constant('FILE_INCLUDE_PATH_V04').'/uploads/webpages/'.$folder)){
                                    if(file_exists(constant('FILE_INCLUDE_PATH_V04').'/uploads/webpages/'.$folder.'/index.html')){
                                        //$html=file_get_contents(base_url('uploads/webpages/'.$folder.'/index.html'));
                                        $link=V04_PATH.'uploads/webpages/'.$folder.'/index.html';
                                    }
                                    else if(file_exists(constant('FILE_INCLUDE_PATH_V04').'/uploads/webpages/'.$folder.'/'.$foldername.'/index.html')){
                                        //$html=file_get_contents(base_url('uploads/webpages/'.$folder.'/'.$foldername.'/index.html'));
                                        $link=V04_PATH.'uploads/webpages/'.$folder.'/'.$foldername.'/index.html';
                                    }
                                   
                                
                                //$vimeovideolink = "http://player.vimeo.com/video/".$vimeo_id."?badge=0&portrait=0&byline=0&rel=0";
                                $videothumb=($video_thumb=='') ? V04_PATH.'/images/popup_pic1.jpg' :  V04_PATH.'uploads/'.$video_thumb;
                            ?>
                            <img src="<?=$videothumb; ?>" border="0" width="250" height="250" align="absmiddle">
                            <?php
                            /*
                                <iframe src="<?=$link;?>" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                             * 
                             */
                            ?>
                            <?php
                                }
                            }
                            else{
                                echo '<div id="flashvideoplayer"></div>';
                            }
                             
                            ?>
                        </td>
                    </tr>
                </table>
                
            </div>
            <!-- /row -->
        </div>
    </div>
    <!-- /panel -->
</div>
    <!-- col-md-6 -->

<div class="col-md-3 nopad-right" id="albumsList"style="padding-right: 9px;">
        <!-- panel -->
        <div class="panel panel-piluku">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if($project_status!='viewer' && $project_status!='none'){ ?>
                        <?php if($userid>0):  ?>
                            <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#add_album" > + Add </a>
                            <?php if($curr_albumid>0){ ?>
                            <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#edit_album"> I Edit </a>
                            <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#sort_album"> <> Sort </a>
                            <?php } 
                        endif;
                        ?>
                    <?php } ?>

              </h3>
          </div>
        <div class="panel-body" id="albumlisting" style="height: 371px;">
              <div class="row main-chart-parent" style="height: inherit; ">
                    <div id="centerlisting_albums">
                        <?php
                        if(!empty($albumsArr)){
                            $i=1;
                            $j=1;
                            foreach ($albumsArr as $album){
                                $classname=($curr_albumid==$album['id'] && $curr_albumid>0) ? 'linkons' : 'linkoff';
                                $i= ($i>2) ? 1 : $i;

                                if( strlen($j)==1 )
                                 {
                                 $count='0'.$j;
                                 }
                                 elseif( strlen($j)>=2 )
                                 {
                                 $count=$j;
                                 }

                            ?>
                        <div class="albumcontainer<?=$i;?>" id="sidenavi_<?=$album['id']; ?>" onclick="window.location.href='<?=base_url(); ?>project/webpages/<?=$projectId; ?>/<?=$album['id']; ?>'">


                                <div class="ac1">

                                  <div class="ac1num">
                                    <?=$count; ?>                          
                                  </div>
                                  <div class="ac1name">
                                    <a onfocus="this.blur();" class="<?=$classname; ?>" href="<?=base_url(); ?>project/webpages/<?=$projectId; ?>/<?=$album['id']; ?>"><?=$album['name']; ?></a>
                                  </div>
                                  <div class="ac1version">

                                  </div>
                                </div>

                                <div class="ac2">
                                  Album
                                  <br>
                                  Name
                                </div>
                                <?php
                                    $thumbnail=($album['thumbnail']!='') ? V04_PATH.'uploads/'.$album['thumbnail'] : V04_PATH.'images/imageicon.png';
                                ?>
                                <div class="ac3">
                                  <a onfocus="this.blur();" href="<?=base_url(); ?>project/webpages/<?=$projectId; ?>/<?=$album['id']; ?>">
                                    <img width="98" height="98" border="0" alt="" src="<?=$thumbnail;?>">
                                  </a>
                                </div>


                              </div>
                            <?php
                                $i++;
                                $j++;
                                }
                            }
                            ?>
                    </div>  
            </div>
            <!-- /row -->
        </div>
    </div>
    <!-- /panel -->
</div>
    
<div class="col-md-9 nopad-right" id="imagesList" style="padding-left: 7px;">
        <!-- panel -->
        <div class="panel panel-piluku">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if($project_status!='viewer' && $project_status!='none'){ ?>
                    <?php if($userid>0):  ?>
                        <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#add_image_popup" id="showImgAdd"> + Add </a>
                        <?php if($videoid>0){  ?>
                        <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#edit_album_image"> I Edit </a> 
                        <?php } 
                    endif;
                    ?>
                    <?php } ?>
                </h3>
          </div>
        <div class="panel-body" style="height: 152px;">
            <div class="row main-chart-parent">
                <?php
                if(!empty($project_videos)){
                    $videothumb=($video_thumb=='') ? V04_PATH.'/images/popup_pic1.jpg' :  V04_PATH.'uploads/'.$video_thumb;
                ?>
                <div class="bottomside" id="videoThumbnail">
                    <img width="130" height="130" id="sideimage_3653" style="display:block;" class="buttomsideimage" src="<?=$videothumb;?>">
                </div>
                <?php
                    }
                 ?>
                <?php
                    if(!empty($project_videos)){
                ?>
                <div style="height: 138px; margin-right: 5px; overflow-x: hidden" id="imagesListing">
                    <table class="table table-bordered" style="margin-left: 5px;">
                    <thead>
                            <tr>
                               
                                    <th>
                                        
                                    </th>
                                    <th>
                                        <?php
                                        if($sortColumn=='name'){
                                            $style='color: #fff;';
                                            if($sortby=='asc'){
                                                $sort='desc';
                                                $classname='sort-desc';
                                            }
                                            else{
                                                $sort='asc';
                                                $classname='sort-asc';
                                            }
                                        }
                                        else{
                                            $sort='desc';
                                            //$classname='order_not';
                                            $classname='sort-desc';
                                            $style='';
                                        }
                                        //$sort=()
                                        ?>
                                        <a href="<?=base_url('project/webpages/'.$projectId.'/'.$curr_albumid.'/'.$videoid.'/name/'.$sort) ?>" class="<?=$classname;?>" style="<?=$style;?>">Name</a>
                                    </th>
                                    <th>
                                        <?php
                                            if($sortColumn=='user'){
                                                $style='color: #fff;';
                                                if($sortby=='asc'){
                                                    $sort='desc';
                                                    $classname='sort-desc';
                                                }
                                                else{
                                                    $sort='asc';
                                                    $classname='sort-asc';
                                                }
                                            }
                                            else{
                                                $sort='desc';
                                                $classname='order_not';
                                                $style='';
                                            }
                                        ?>
                                        <a href="<?=base_url('project/webpages/'.$projectId.'/'.$curr_albumid.'/'.$videoid.'/user/'.$sort) ?>" class="<?=$classname;?>" style="<?=$style;?>">User</a>
                                    </th>
                                    <th>
                                        <?php
                                            if($sortColumn=='date'){
                                                $style='color: #fff;';
                                                if($sortby=='asc'){
                                                    $sort='desc';
                                                    $classname='sort-desc';
                                                }
                                                else{
                                                    $sort='asc';
                                                    $classname='sort-asc';
                                                }
                                            }
                                            else{
                                                $sort='desc';
                                                $classname='order_not';
                                                $style='';
                                            }
                                        ?>
                                        <a href="<?=base_url('project/webpages/'.$projectId.'/'.$curr_albumid.'/'.$videoid.'/date/'.$sort) ?>" class="<?=$classname;?>" style="<?=$style;?>">Date</a>
                                    </th>
                                    <th>
                                        <?php
                                            if($sortColumn=='size'){
                                                $style='color: #fff;';
                                                if($sortby=='asc'){
                                                    $sort='desc';
                                                    $classname='sort-asc';
                                                }
                                                else{
                                                    $sort='asc';
                                                    $classname='sort-desc';
                                                }
                                            }
                                            else{
                                                $sort='desc';
                                                $classname='order_not';
                                                $style='';
                                            }
                                        ?>
                                        <a href="<?=base_url('project/webpages/'.$projectId.'/'.$curr_albumid.'/'.$videoid.'/size/'.$sort) ?>" class="<?=$classname;?>" style="<?=$style;?>">Size</a>
                                    </th>
                            </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($project_videos as $videos) {
                                $videonamereplace=str_replace('_', ' ', $videos['title']);
                                $videoName=(strlen($videonamereplace)>100) ? substr($videonamereplace, 0, 40).'...' : $videonamereplace;
                                $classname=($videos['id']==$videoid) ? 'pl_conton' : 'pl_cont';
                                $filesize=$videos['size']/1024;
                                $filesize=round($filesize, 2);
                                $filesize=$filesize.'MB';
                                $classSelected=($videoid==$videos['id']) ? 'contentrow-selected' : '';
                        ?>
                            <tr id="bottom_<?=$videos['id'] ?>" onmouseover="showComments(<?=$videos['id'] ?>)">
                                    <th scope="row">
                                        <a onfocus="this.blur();" href="<?=base_url(); ?>project/download/webpage/<?=$videos['id'] ?>" class="pl_download">&nbsp;</a>
                                    </th>
                                    <td onmouseover="changeThumbnail('<?=$videos['thumbnail']; ?>')">
                                            <a href="<?=base_url(); ?>project/webpages/<?=$projectId?>/<?=$curr_albumid;?>/<?=$videos['id']?>" title="<?=$videos['title']; ?>" onclick="openwebpage('<?=$link; ?>', '<?=$videos['iframe_height'] ?>', '<?=$videos['iframe_width'] ?>')"><?=$videoName; ?> </a>
                                    </td>
                                    <td><?=$videos['stage_name']; ?></td>
                                    <td><?=date('Y M d, h:i A', strtotime($videos['added_date'])); ?></td>
                                    <td><?=$filesize; ?></td>
                                    

                            </tr>
                            <?php
                            }
                            ?>

                    </tbody>
            </table>
                </div>
                
                <?php
                 }
                  
                ?>
            </div>
            <!-- /row -->
        </div>
    </div>
    <!-- /panel -->
</div>
    
<div class="col-md-3 nopad-right" id="commentList" style="padding-right: 9px;">
        <!-- panel -->
        <div class="panel panel-piluku">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if($project_status!='viewer' && $project_status!='none'){ ?>
                        <?php if($userid>0 && $videoid>0): ?>
                        <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#addalbumCommentDiv"> + Add </a>
                        <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#editCommentDiv"> I Edit </a>
                        <?php else:
                        echo '&nbsp;'    ;
                            endif; 
                        ?>
                    <?php } ?>
                </h3>
                
          </div>
        <div class="panel-body" style="height: 152px;" id="commentListing">
              <div class="row main-chart-parent" style="height: inherit">
                <?php
                $i=1;
                $html='';
                foreach ($album_comments as $key => $value) {
                    $display=($videoid==$key) ? 'display:block' : '';
                    $html.='<div id="commentsVideo'.$key.'" class="showCommentsVideo" style="'.$display.'">';
                    $j=1;
                    foreach ($value as $arrkey => $arrvalue) {
                        if( strlen($j)==1 ){
                            $count='0'.$j;
                        }
                        else{
                            $count=$j;
                        }
                        $html.= '<div class="commentnormal">
                                    <div class="commentnames">'.$count.' - '.$arrvalue['stage_name'].' ('.date('d.m.y, h:i A', strtotime($arrvalue['added_date'])).') :</div>
                                    <div class="commentmessage"> '.$arrvalue['comment'].'</div>
                                </div>';
                        $j++;
                    }
                    $html.='</div>';
                    
                }
                
                echo $html;
                
                ?>
            </div>
            <!-- /row -->
        </div>
    </div>
    <!-- /panel -->
</div>
<script>
        $(function() {
            $( ".sortable" ).sortable();
            $( ".sortable" ).disableSelection();

          });
          
        $(window).load(function(){
            resizeDiv();
            /*
            if(curr_album>0)
                $("#centerlisting_albums").mCustomScrollbar("scrollTo","#sidenavi_"+curr_album);
            
            if(imageid>0)
                $("#project_videos").mCustomScrollbar("scrollTo","#bottom_"+imageid);
            */
            
        })
        
</script>
