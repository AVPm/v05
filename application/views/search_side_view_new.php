<?php
// ****************************************************************************
// 
//     User's project list view on top of page in horizontal scroll div
//
// ****************************************************************************

/*
 * User own projects list
 */
$select_projectId=(isset($select_projectId) && $select_projectId>0) ? $select_projectId : 0; 
?>
<script>
    function resizeSearch(){
        var wh = $(window).height();
        //wh;
        var topmenuh=$('.top-bar').height();
        //topmenuh;
        var headingDivh=$('#centerImg .panel-heading').height();

        var filter=$('#searchtopFilter').height();

        var h=wh-topmenuh-headingDivh-filter-80;
        //h;
        //filter;
        //h;
        $('#SearchList #commentListing #recent_content').css('height', h+'px');
    }
    $(document).ready(function(){
        resizeSearch();
        //showSearchResult();
        $("#recent_content").niceScroll();
    });
    $(window).resize(function() {
        $("#recent_content").niceScroll();
        resizeSearch();
    });
</script>
    
<div class="col-md-3 nopad-right pull-right" id="SearchList" >
        <!-- panel -->
        <div class="panel panel-piluku">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Search
                </h3>
                
          </div>
        <div class="panel-body" style="" id="commentListing">
            <div class="row main-chart-parent" style="height: inherit">
                <div id="searchtopFilter">
                    <label><a href="javascript:void(0)" class="but_right" onfocus="this.blur();" onclick="showSearch()" id="searchtxt">Search</a></label>
                    <div id="searchDiv" style="display: none">
                        <input type="text" id="input_search" name="input_search" value="" class="input_search form-control pull-left" style="margin-left: 5px; width: 175px;" onkeypress="handleEvent(event)">
                        <a href="javascript:void(0)" class="but_search btn btn-primary pull-right" onclick="showSearchResult()" style="margin-right:5px;">Search</a>

                    </div>
                    <div class="clear" style="clear: both; height: 5px;"></div>
                    <label><a href="javascript:void(0)" class="but_down" onfocus="this.blur();">Whats New</a></label>

                    <div class="styled-select filter_width">		
                        <select size="1" id="identifier_project" name="identifier_project" class="form-control pull-left" onchange="showSearchResult();" style="width:127px; margin-right: 5px;">    
                            <option value="" selected="selected">Projects (All)</option> 
                            <?php
                                if(count($all_projects)>0){
                                    foreach ($all_projects as $key => $value) {
                                        echo '<option value="'.$value['id'].'">Project ('.$value['identifier'].')</option>';
                                    }
                                }
                            ?>

                        </select>
                    </div>

                    <div class="styled-select filter_width">			
                            <select size="1" id="itemtype_filter" name="itemtype_filter" class="form-control pull-left" onchange="showSearchResult();" style="width:127px;">    
                                    <option value="" selected="selected">Type (All)</option>
                                    <option value="images">Type (Images)</option>
                                    <option value="videos">Type (Videos)</option>
                                    <option value="files">Type (Files)</option>
                                </select>
                    </div>        
                    <div class="clear"></div>
                    <div class="styled-select filter_width">
                                <select size="1" id="search_limit" name="search_limit" class="form-control pull-left" onchange="showSearchResult();" style="width:127px; margin-right: 5px;">    
                                    <option value="10" selected="selected">Show (10 items)</option>
                                    <option value="25">Show (25 items)</option>
                                    <option value="50">Show (50 items)</option>
                                </select>
                    </div>
                    <div class="styled-select filter_width">
                                <select size="1" id="search_order" name="search_order" class="form-control pull-left" onchange="showSearchResult();" style="width:127px;">    
                                    <option value="latest_first" selected="selected">Sort (latest first)</option>
                                    <option value="oldest_first">Sort (oldest first)</option>
                                    <option value="user">Sort (User)</option>
                                    <option value="project">Sort (Project)</option>
                                </select>
                    </div> 
                    <div class="clear">&nbsp;</div>
                </div>
                <?php /* ?>
                <div id="recent_content" style="overflow: auto; overflow-x: hidden; height: 100%; background: #fff">                
                    <img style="position:absolute; z-index:15; top:20%; left:25%" src="<?=base_url(); ?>images/ajax-loader.gif">
                </div>
                 * 
                 */
                ?>
                <div id="recent_content" style="overflow: auto; overflow-x: hidden; height: 100%; background-color: inherit !important" class="dd">                
                    <img style="position:absolute; z-index:15; top:20%; left:25%" src="<?=base_url(); ?>images/ajax-loader.gif">
                </div>
            </div>
            <!-- /row -->
        </div>
    </div>
    <!-- /panel -->
</div>

