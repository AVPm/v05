<?php
// ****************************************************************************
// 
//     User's project list view on top of page in horizontal scroll div
//
// ****************************************************************************

/*
 * User own projects list
 */
$select_projectId=(isset($select_projectId) && $select_projectId>0) ? $select_projectId : 0;
$userid = $this->session->userdata('user_id');
?>
<script>
    function resizeProjectDiv(){
        var width=$('.projectlist').width();
        width=(width+14)*$('.projectlist').length;
        $('#projectListingContainer').css('width', width+'px');
    }
    $(document).ready(function(){
        $("#projectListing").niceScroll();
        resizeProjectDiv();
    });
    $(window).resize(function() {
        //$("#projectListing").niceScroll();
        resizeProjectDiv();
    });
</script>
<?php
// add project popup
    if($userid>0):
?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addProject_div" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Add Project</h4>
                        </div>
                        <div class="modal-body">
                                <?php
                                echo form_open_multipart(base_url("user/login"), array('onsubmit'=>'return chkaddProject()', 'id'=>'add_project'));
                                //echo form_label('Identifier (unique):', 'identifier'); 
                                //echo '<br>';
                                echo '<label class="control-label">Identifier (unique):</label>';
                                $data = array(
                                                'name'        => 'identifier',
                                                'id'          => 'identifier',
                                                'onchange'    => 'check_identifier()',
                                                'class'  =>  'form-control',
                                              );

                                echo form_input($data) .'&nbsp; <span id="identifier_msg" style="display:none"></span>';
                                echo '<br>';

                                //echo form_label('Title:', 'title'); 
                                //echo '<br>';
                                echo '<label class="control-label">Title:</label>';
                                $data = array(
                                                'name'        => 'title',
                                                'id'          => 'title',
                                                'class'  =>  'form-control',
                                              );

                                echo form_input($data);
                                echo '<br>';

                                //echo '<label for="view_only_mode" style="float: left;">Allow access in View-only mode:</label>';
                                echo '<label class="control-label">Allow access in View-only mode:</label>';
                                //echo '<input type="checkbox" style="float: left; width: auto; height: auto; margin: 2px 0px 0px 5px;" name="view_only_mode" id="view_only_mode" value="1">';
                                //echo '<br>';
                                echo '<input type="checkbox" value="1" name="view_only_mode" id="view_only_mode">
                                       <label class="margin-zero" for="view_only_mode"><span></span></label>';
                                echo '<br>';
                                //echo form_label('Upload image:', 'upload_image'); 
                                //echo '<br>';
                                echo '<label class="control-label">Upload Image:</label>';
                                /*
                                $data = array(
                                                'name'        => 'upload_image',
                                                'id'          => 'upload_image',
                                                'style'       => 'height:25px;',
                                                'class'       => 'filestyle',
                                                'data-icon'   => 'false'
                                              );
                                 * 
                                 */

                                //echo form_upload($data);
                                echo '<input type="file" class="filestyle" data-icon="false" name="upload_image" id="upload_image" />';
                                echo '<br>';

                                echo form_close();
                                ?>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#add_project').submit();">Save Project</button>
                        </div>
                </div>
        </div>
</div>
<?php 
endif; 
// add project popup end
?>


<?php
// edit project popup
    if($userid>0):
?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="editprojectDiv" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Edit Project</h4>
                                <a style="position: absolute; top: 12px; right: 45px;" class="off" onfocus="this.blur();" href="<?=base_url("project/remove/".$select_projectId); ?>">Remove Project</a>
                        </div>
                        <div class="modal-body">
                                <?php
                                echo form_open_multipart(base_url("project/update"), array('onsubmit'=>'return chkupdateProject()', 'id'=>'edit_project'));
                                //echo form_label('Identifier (unique):', 'identifier'); 
                                //echo '<br>';
                                echo '<label class="control-label">Identifier (unique):</label>';
                                $data = array(
                                                'name'        => 'identifier',
                                                'id'          => 'edit_identifier',
                                                'onchange'    => 'check_identifier()',
                                                'class'  =>  'form-control',
                                                'value'       => @$view_project_detail[0]['identifier']
                                              );

                                echo form_input($data) .'&nbsp; <span id="edit_identifier_msg" style="display:none"></span>';
                                echo '<br>';

                                //echo form_label('Title:', 'title'); 
                                //echo '<br>';
                                echo '<label class="control-label">Title:</label>';
                                $data = array(
                                                'name'        => 'title',
                                                'id'          => 'edit_title',
                                                'value'       => @$view_project_detail[0]['title'],
                                                'class'  =>  'form-control',
                                              );

                                echo form_input($data);
                                echo '<br>';

                                //echo '<label for="view_only_mode" style="float: left;">Allow access in View-only mode:</label>';
                                echo '<label class="control-label">Allow access in View-only mode:</label>';
                                //echo '<input type="checkbox" style="float: left; width: auto; height: auto; margin: 2px 0px 0px 5px;" name="view_only_mode" id="view_only_mode" value="1">';
                                //echo '<br>';
                                $checked=($view_project_detail[0]['view_only_mode']=='1') ? 'checked="checked"' : '';
                                
                                echo '<input type="checkbox" value="1" name="view_only_mode" id="view_only_mode" '.$checked.'>
                                       <label class="margin-zero" for="view_only_mode"><span></span></label>';
                                echo '<br>';
                                //echo form_label('Upload image:', 'upload_image'); 
                                //echo '<br>';
                                $data = array(
                                                'name'        => 'thumbnail',
                                                'id'          => 'thumbnail',
                                                'value'       => @$view_project_detail[0]['thumbnail'],
                                                'type'        => 'hidden'
                                              );

                                echo form_input($data);

                                $data = array(
                                                'name'        => 'project_id',
                                                'id'          => 'project_id',
                                                'value'       => $select_projectId,
                                                'type'        => 'hidden'
                                              );

                                echo form_input($data);

                                echo '<label class="control-label">Upload Image:</label>';
                                /*
                                $data = array(
                                                'name'        => 'upload_image',
                                                'id'          => 'upload_image',
                                                'style'       => 'height:25px;',
                                                'class'       => 'filestyle',
                                                'data-icon'   => 'false'
                                              );
                                 * 
                                 */

                                //echo form_upload($data);
                                echo '<input type="file" class="filestyle" data-icon="false" name="upload_image" id="edit_upload_image" />';
                                echo '<br>';

                                echo form_close();
                                ?>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#edit_project').submit();">Save Project</button>
                        </div>
                </div>
        </div>
</div>
<?php 
endif; 
// edit project popup end
?>


<!-- project form start -->
<?php if($userid>0): ?>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="sort_projects" class="modal fade modal-full-pad in">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Sort Projects</h4>
                        </div>
                        <div class="modal-body">
                                <div id="containmentbox" class="sort_area">
                                    <ul id="sortprojectbox" class="sortable ui-sortable" style="">
                                        <?php
                                        foreach ($user_projects as $key) {
                                             //$thumbnail=($album['thumbnail']!='') ? base_url().'uploads/'.$album['thumbnail'] : base_url().'images/imageicon.png';
                                             $thumbnail=($key['thumbnail']!='') ? V04_PATH.'uploads/'.$key['thumbnail'] : V04_PATH.'uploads/projecticon160.png';
                                             echo '<li id="id_'.$key['id'].'" style="">
                                                     <img width="160" height="160" class="selector" src="'.$thumbnail.'">
                                                     <div class="album_name">'.$key['title'].'</div>
                                                 </li>';
                                        }
                                        if(!empty($project_participated)){
                                             foreach ($project_participated as $key) {
                                                 $thumbnail=($key['thumbnail']!='') ? V04_PATH.'uploads/'.$key['thumbnail'] : V04_PATH.'uploads/projecticon160.png';
                                                 echo '<li id="id_'.$key['id'].'" style="">
                                                         <img width="160" height="160" class="selector" src="'.$thumbnail.'">
                                                         <div class="album_name">'.$key['title'].'</div>
                                                     </li>';
                                            }
                                        }
                                        ?>

                                     </ul>  
                                       <div class="clear">&nbsp;</div>
                             	
                                   </div>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="saveOrder('projects');">Save Projects</button>
                        </div>
                </div>
        </div>
</div>

<?php endif; ?>
<!-- project sorting form ends -->

<div class="col-md-9 nopad-right" id="projectList">
        <!-- panel -->
        <div class="panel panel-piluku">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php //if($project_status!='viewer' && $project_status!='none'){ ?>
                    <?php //if($userid>0):  ?>
                        <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#addProject_div" id="showImgAdd">Add</a> | 
                        <?php if($select_projectId>0): ?>
                        <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#editprojectDiv">Edit</a> | 
                        <?php endif; ?>
                        <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#sort_projects">Sort</a>
                        <?php 
                    //endif;
                    ?>
                    <?php // } ?>
                </h3>
          </div>
        <div class="panel-body" style="height: 230px;">
            <div class="row main-chart-parent">
               
                <?php
                    if(!empty($user_projects) || !empty($project_participated)){
                ?>
                <div style="height: 215px; margin-right: 5px; " id="projectListing">
                    <div style=" width: 3025px; " id="projectListingContainer">
                        
                        <?php
                              foreach ($user_projects as $key) {
                                $thumbnail=($key['thumbnail']!='') ? $key['thumbnail'] : 'projecticon160.png';
                                $class=($key['id']==$select_projectId) ? 'outerprojects_select' : 'outerprojects';
                            ?>    
                            <div id="projectlist_<?=$key['id']; ?>" style="float: left; padding-right: 10px;" class="projectlist">
                                      <div class="<?=$class; ?>">
                                          <a onfocus="this.blur();" class="pic dd" href="<?=base_url(); ?>project/view/<?=$key['id']; ?>"><img alt="" height="160" width="160" src="<?=V04_PATH; ?>uploads/<?=$thumbnail; ?>"></a>
                                          <div style="margin-left:5px;">
                                                <div>
                                                    <label>name: </label>
                                                      <?=$key['title']; ?>
                                                </div>

                                                <div class="clear"></div>

                                                <div>
                                                    <label>id: </label>
                                                      <?=$key['identifier']; ?>
                                                </div>
                                            </div>

                                      </div>

                              </div>
                   
                    
                          <?php
                              }
                           ?>
                
                    <?php
                              if(!empty($project_participated)){
                                  foreach ($project_participated as $key) {
                                      $thumbnail=($key['thumbnail']!='') ? $key['thumbnail'] : 'projecticon160.png';
                                      $class=($key['id']==$select_projectId) ? 'outerprojects_select' : 'outerprojects';
                                  ?>    
                                    <div id="projectlist_<?=$key['id']; ?>" style="float: left; padding-right: 10px;" class="projectlist">
                                            <div class="<?=$class; ?>">
                                                <a onfocus="this.blur();" class="pic" href="<?=base_url(); ?>project/view/<?=$key['id']; ?>"><img alt="" height="160" width="160" src="<?=V04_PATH; ?>uploads/<?=$thumbnail; ?>"></a>

                                                <div style="margin-left:5px;">
                                                    <div>
                                                        <label>name: </label>
                                                          <?=$key['title']; ?>
                                                    </div>

                                                    <div class="clear"></div>

                                                    <div>
                                                        <label>id: </label>
                                                          <?=$key['identifier']; ?>
                                                    </div>
                                                </div>
                                            </div>

                                    </div>
                                <?php
                                    }
                              }



                            ?>
                    </div>
                            

                          
                </div>
                
                <?php
                 }
                  
                ?>
            </div>
            <!-- /row -->
        </div>
    </div>
    <!-- /panel -->
</div>


<script>
  $(function() {
     $( ".sortable" ).sortable();
     $( ".sortable" ).disableSelection();

   });
</script>