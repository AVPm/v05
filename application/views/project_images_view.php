<?php
// ****************************************************************************
// 
//     User's project images view
//
// ****************************************************************************
session_start();
$sessionid=session_id();
$currAlbumId='';
$albumInfoArr=  array();
if(!empty($albumsArr)){
    $newarr = current($albumsArr);
    $currAlbumId=$newarr['id'];
    
    foreach($albumsArr as $id => $value){
        if($value['id']==$curr_albumid){
            $albumInfoArr['thumbnail']=$value['thumbnail'];
            $albumInfoArr['name']=$value['name'];
            $albumInfoArr['description']=$value['description'];
        }
    }
}
?>

<script>
    var curr_project=<?=$projectId; ?>;
    var curr_album=<?=$curr_albumid; ?>;
</script>

<!-- add album popup starts -->

<div class="popup" id="add_album" style="width: 640px;">
    <div class="popup_head">
            <span>Add Album</span>	
    </div>
    <div id="adduserHtml">
        <div class="popup_top">
                <img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
                <div class="run">To add an Album you have to enter a Name in the form below. You can also supply a picture to have a thumbnail image for the Album.</div>
                <div class="clear">&nbsp;</div> 
                <div class="clear" id="userProjectmsg"></div>
        </div>
        <div class="popup_forms">
            <form action="" enctype="multipart/form-data" method="post" name="addalbum" id="addalbum">
             <?php
                /*
                 * hidden field with value of project id
                 */
                    $data = array(
                    'name'        => 'projectid_hidden',
                    'id'          => 'projectid_hidden',
                    'value'       => $projectId,
                    'type'        => 'hidden',
                    );

                    echo form_input($data);
                    
                    echo form_label('Album Name: ', 'album_name'); 
                    echo '<br>';
                    $data = array(
                                    'name'        => 'album_name',
                                    'id'          => 'album_name',
                                  );
                    echo form_input($data);
                    
                    echo '<br>';
                    
                    echo form_label('Album Description: ', 'album_info'); 
                    echo '<br>';
                    $data = array(
                                    'name'        => 'album_info',
                                    'id'          => 'album_info',
                                  );
                    echo form_input($data);
                    ?>
                    <br>
                    Thumbnail:<br>
                    <div style="position:relative; width:310px; height:19px;">
                                    <div style="position:relative;	text-align: left; -moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                                            <input type="file" onchange="javascript:copyField();" ondblclick="javascript:deleteField();" size="30" name="thumbnail" id="fileselecter" style="-moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                                    </div>
                        <div style="position:absolute; top:0px; left:0px;z-index: 1; width:311px; ">
                            <input type="text" style="width:281px; float:left;" id="fakefilefield"><img width="27" height="19" border="0" alt="" style="position: absolute; top:4px; left:281px;" src="<?=base_url(); ?>images/folder.png">
                        </div>
                    </div>
                    <br>Upscale Images:<br><input type="checkbox" onfocus="this.blur();" value="11" name="flags[]" class="checkbox"><br>		</form>

        </div>
    </div>
    <div class="popup_bottom">
            <a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('add_album')">Cancel</a>
            <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="$('#addalbum').submit();">Submit</a>
            <div class="clear">&nbsp;</div> 
    </div>		
</div>
<!-- add album popup ends -->


<!-- edit album popup starts -->
<div class="popup" id="edit_album_form" style="width: 640px;">
    <div class="popup_head">
        <span>Edit Album</span>
        <a class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removealbum(<?=$curr_albumid; ?>, 'images_album')">Remove Album</a>
    </div>
	<div class="popup_top_project">
            <?php $thumbnail=($albumInfoArr['thumbnail']!='') ? base_url().'uploads/'.$albumInfoArr['thumbnail'] : base_url().'images/imageicon.png'; ?>
		<img alt="" src="<?=$thumbnail;?>">
		<div class="run"></div>
		<div class="clear">&nbsp;</div> 
	</div>
	<div class="popup_forms">
            <?php
            
                echo form_open_multipart("", array('onsubmit'=>'return chkeditalbum()', 'id'=>'editalbum'));
                /*
                 * hidden field with value of project id
                 */
                $data = array(
                'name'        => 'album_hidden',
                'id'          => 'album_hidden',
                'value'       => $curr_albumid,
                'type'        => 'hidden',
                );

                echo form_input($data);
                
                echo form_label('Album Name: ', 'edit_album_name'); 
                echo '<br>';
                $data = array(
                                'name'        => 'edit_album_name',
                                'id'          => 'edit_album_name',
                                'value'       => $albumInfoArr['name'],
                              );
                echo form_input($data);

                echo '<br>';
                
                echo form_label('Album Description: ', 'edit_album_description'); 
                echo '<br>';
                $data = array(
                                'name'        => 'edit_album_description',
                                'id'          => 'edit_album_description',
                                'value'       => $albumInfoArr['description'],
                              );
                echo form_input($data);

                echo '<br>';
                
                /*
                 * hidden field with value of project id
                 */
                $data = array(
                'name'        => 'album_thumbnail_hidden',
                'id'          => 'album_thumbnail_hidden',
                'value'       => $albumInfoArr['thumbnail'],
                'type'        => 'hidden',
                );

                echo form_input($data);
                                
            ?>
            Thumbnail:<br>
      <div style="position:relative; width:310px; height:19px;">
            <div style="position:relative;	text-align: left; -moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
                    <input type="file" size="30" name="edit_thumbnail" id="fileselecters" style="-moz-opacity:0 ;	filter:alpha(opacity: 0);	opacity: 0;	z-index: 2;">
            </div>
            <div style="position:absolute; top:0px; left:0px;z-index: 1; width:311px; ">
            	<input type="text" style="width:281px; float:left;" id="fakefilefield"><img width="27" height="19" border="0" alt="" style="position: absolute; top:4px; left:281px;" src="<?=base_url(); ?>images/folder.png">
            </div>
        </div>
        <br>
        
		remove current Folder Thumbnail:<br>
                <?php
                $data = array(
                    'name'        => 'deletethumb',
                    'id'          => 'deletethumb',
                    'value'       => 'delete',
                    'class'       => 'checkbox'
                    );

                echo form_checkbox($data);
                ?>
        <br>
        <?php echo form_close(); ?>
	
	</div>
	<div class="popup_bottom">
            <a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('edit_album_form')">Cancel</a>
            <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="$('#editalbum').submit();">Submit</a>
            <div class="clear">&nbsp;</div> 
	</div>		
</div>
<!-- edit album popup ends -->


<!-- add images in album popup start -->
<div class="popup" id="add_image_popup">
<div class="popup_head">
  <span>Add Images</span>
  <a class="off" href="javascript:void(0)">Add Imageversions</a>  
	</div>	<div class="popup_top">
		<img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
		<div class="run">Upload Images. First browse for your image files, then start upload.</div>
		<div class="clear">&nbsp;</div> 
	</div>
	<div class="popup_forms">
        <form action="" enctype="multipart/form-data" method="post" id="form1">
	
	<div id="queue-status">
    <div id="spanButtonPlaceHolderwrapper"><span id="spanButtonPlaceHolder"></span></div>
    <span id="divStatus" style="display:none;">0 Files Uploaded</span> 
  </div>
	
	<div class="controls">
		<input type="button" onclick="swfu.startUpload();" value="Start upload" id="btnStart" class="controlbuttons" onfocus="this.blur();">
		<input type="button" onclick="swfu.cancelQueue();" value="Cancel all uploads" id="btnCancel" class="controlbuttons" onfocus="this.blur();">
	</div>
<div id="fsUploadProgress" class="fieldset flash">
		<h3>File Queue</h3>
		<span id="queue-is-empty">currently empty</span>
	</div>
	
<script type="text/javascript">	
var swfu;

var settings = {
        flash_url : "<?=base_url(); ?>includes/swfupload.swf",
        upload_url: "<?=base_url(); ?>project/album/imageupload",	// Relative to the SWF file
        post_params: {"PHPSESSID" : "<?=$sessionid; ?>", "album_id":<?=$currAlbumId?>},
        file_size_limit : "999 MB",
        file_types : "*.jpg;*.jpeg;*.gif;*.png",
        file_types_description : "Image Files",
        file_upload_limit : 999,
        file_queue_limit : 0,
        file_post_name : "file", 
        custom_settings : {
                progressTarget : "fsUploadProgress",
                cancelButtonId : "btnCancel",
                startButtonId : "btnStart"
        },
        debug: false,				

        // Button settings
        button_image_url: "<?=base_url(); ?>images/folder4x.png",	// Relative to the Flash file
        button_width: 150,
        button_height: 19,
        button_placeholder_id: "spanButtonPlaceHolder",
        button_text_left_padding : 30, 
        button_text : "<span class=\"whitetext\">Browse for Files<\/span>",
        button_text_style : ".whitetext { color: #FFFFFF;font-family:Arial, Helvetica, sans-serif; font-size:12px;}", 
        button_window_mode : SWFUpload.WINDOW_MODE.TRANSPARENT, 

        // The event handler functions are defined in handlers.js
        file_queued_handler : fileQueued,
        file_queue_error_handler : fileQueueError,
        file_dialog_complete_handler : fileDialogComplete,
        upload_start_handler : uploadStart,
        upload_progress_handler : uploadProgress,
        upload_error_handler : uploadError,
        upload_success_handler : uploadSuccess,
        upload_complete_handler : uploadComplete,
        queue_complete_handler : queueComplete	// Queue plugin event
};
swfu = new SWFUpload(settings);
</script>
		</form>
	
	</div>
	<div class="popup_bottom">
		
            <a class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="location.reload();">done</a>
		<div class="clear">&nbsp;</div> 
	</div> 
</div>
<!-- add images in album popup ends -->

<tr>
        <td id="content" style="height: 407px;">
          <div style="height: 407px;" id="content2">
            <div class="divbar" id="divbarid">
            </div>
            <table style="height: 369px;" id="centerTable" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tbody>
                <tr>
                  <td align="center" valign="middle">
                    
                    <div id="flashimagescaler">
                        <?php
                        if(isset($largeImgArr['fullimage']) && $largeImgArr['fullimage']!=''){
                          //  echo '<img src="/uploads/'.$largeImgArr['fullimage'].'" style="visibility:hidden; width:24px; height:24px;" id="large_img">';
                            echo '<script> var selectedImage ='.base_url().'"uploads/'.$largeImgArr['fullimage'].'";</script>';
                            /*
                             * echo '<script>setTimeout(placePlayer("/uploads/'.$largeImgArr['fullimage'].'"), 1000);</script>';
                             */
                            
                       
                        }
                        else{
                            echo '<strong>&nbsp;</strong>';
                        }
                            //echo '<img src="'.base_url().'uploads/'.$largeImgArr['fullimage'].'" id="imagescaler" >';
                        ?>
                        
                       
                       
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
              
            <!-- edit album's images popup -->
            <div class="popup" id="edit_album_image" style="width: 640px;">
                <div class="popup_head">    
                    <span>Edit Image</span>
                    <a class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removealbumimage(<?=$imageid; ?>)">Delete Image</a>
                </div>
                <div class="popup_top">
                    <img alt="" src="<?=base_url(); ?>uploads/<?=$image_thumb?>" title="<?=$image_title; ?>" width="98" height="98">
                    <div class="run">Edit an Image.</div>
                    <div class="clear">&nbsp;</div> 
                </div>
                <div class="popup_forms">
                    <?php 
                        echo form_open_multipart("", array('id'=>'editimage'));
                        
                        $data = array(
                                    'name'        => 'album_imageid_hidden',
                                    'id'          => 'album_imageid_hidden',
                                    'value'       => $imageid,
                                    'type'        => 'hidden',
                                    );

                        echo form_input($data);
                
                        echo form_label('Title: ', 'image_title'); 
                        echo '<br>';
                        $data = array(
                                        'name'        => 'image_title',
                                        'id'          => 'image_title',
                                        'value'       => $image_title,
                                      );
                        echo form_input($data);

                        echo '<br>';

                        echo form_label('additional Info: ', 'additional_info'); 
                        $data = array(
                                'name'        => 'additional_info',
                                'id'          => 'additional_info',
                                'rows'        => '7',
                                'cols'        => '36',
                                'value'       => $image_info
                              );

                            echo form_textarea($data);
                    ?>
                    <br><br>  
                    <?php echo form_close(); ?>
                </div>
                <div class="popup_bottom">
                        <a class="but_red floatleft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('edit_album_image')">cancel</a>
                        <a class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="$('#editimage').submit();">submit</a>
                        <div class="clear">&nbsp;</div> 
                </div>

            </div>
            
            <div class="divbarbottom" id="divbarbottomid">
              <a onfocus="this.blur();" href="javascript:void(0)" class="addbt" onclick="openDiv('add_image_popup');">
                &nbsp;
              </a>
                <a onfocus="this.blur();" href="javascript:void(0)" class="editbt" onclick="openDiv('edit_album_image');">
                &nbsp;
              </a>
              <a onfocus="this.blur();" href="javascript:void(0)" class="sortbt">
                &nbsp;
              </a>
              
            </div>
            
          </div>
        </td>
        <td style="height: 407px;" id="sidenavi">
          <div style="height: 407px;" id="sidenavi2">
            <div class="divbar">
              <a onfocus="this.blur();" href="javascript:void(0)" class="addbt" onclick="openDiv('add_album');">
                &nbsp;
              </a>
              <a onfocus="this.blur();" href="javascript:void(0)" class="editbt" onclick="openDiv('edit_album_form')">
                &nbsp;
              </a>
              <a onfocus="this.blur();" href="javascript:void(0)" class="sortbt">
                &nbsp;
              </a>
            </div>
            <div class="divcontent">
              <div style="height: 369px;" id="wn1">
                <div style="position: absolute; left: 0px; top: 0px; visibility: visible;" id="lyr1">
                  
                <?php
                if(!empty($albumsArr)){
                    $i=1;
                    $j=1;
                    foreach ($albumsArr as $album){
                        $classname=($curr_albumid==$album['id'] && $curr_albumid>0) ? 'linkon' : 'linkoff';
                        $i= ($i>2) ? 1 : $i;
                       
                        if( strlen($j)==1 )
                         {
                         $count='0'.$j;
                         }
                         elseif( strlen($j)>=2 )
                         {
                         $count=$j;
                         }
                        
                    ?>
                        <div id="sidenavi_<?=$album['id']; ?>" class="albumcontainer<?=$i;?>">


                        <div class="ac1">

                          <div class="ac1num">
                            <?=$count; ?>
                          </div>
                          <div class="ac1name">
                            <a href="<?=base_url(); ?>project/images/<?=$projectId; ?>/<?=$album['id']; ?>" class="<?=$classname; ?>" onfocus="this.blur();" id="linksidenavi_3650">
                              <?=$album['name']; ?>
                            </a>
                          </div>
                          <div class="ac1version">

                          </div>
                        </div>

                        <div class="ac2">
                          Album
                          <br>
                          Name
                        </div>
                        <?php
                            $thumbnail=($album['thumbnail']!='') ? base_url().'uploads/'.$album['thumbnail'] : base_url().'images/imageicon.png';
                        ?>

                        <div class="ac3">
                          <a href="<?=base_url(); ?>project/images/<?=$projectId; ?>/<?=$album['id']; ?>" onfocus="this.blur();">
                            <img src="<?=$thumbnail;?>" alt="" border="0" height="98" width="98">
                          </a>
                        </div>


                      </div>
                    <?php
                    $i++;
                    $j++;
                    }
                }
                ?>
              </div>
  </div>
  
  
  <div style="height: 369px;" id="scrollbar">
    <div id="up">
    </div>
    <div style="height: 369px;" id="track">
      <div style="left: 1px; top: 0px; height: 369px;" id="dragBar">
      </div>
    </div>
    <div id="down">
    </div>
  </div>
  
  
  
  
            </div>
            <div class="divbarbottom">
              <a onfocus="this.blur();" href="javascript:void(0)" class="addbt">
                &nbsp;
              </a>
              <a onfocus="this.blur();" href="javascript:void(0)" class="editbt">
                &nbsp;
              </a>
            </div>
            
          </div>
        </td>
      </tr>