<?php
// ****************************************************************************
// 
//     User's project detail view
//
// ****************************************************************************


?>
<script type="text/javascript" src="<?=base_url(); ?>includes/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        window.onload = function()
        {
            CKEDITOR.replace( 'comment', {
                    removePlugins: 'bidi,div,font,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley/maximize',
                    //removeButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',
                    format_tags: 'p;h1;h2;h3;pre;address',
                    height: 250
                    
            } );
            
            CKEDITOR.replace( 'comment_edit', {
                    removePlugins: 'bidi,div,font,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley/maximize',
                    //removeButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',
                    format_tags: 'p;h1;h2;h3;pre;address',
                    height: 250
                    
            } );
            
            CKEDITOR.on('dialogDefinition', function( ev ){
                var dialogName = ev.data.name;
                var dialogDefinition = ev.data.definition;
                if ( dialogName == 'link' ){
                    // Get a reference to the "Target" tab.
                            var targetTab = dialogDefinition.getContents( 'target' ); 
                            // Set the default value for the target field.
                            var targetField = targetTab.get( 'linkTargetType' );
                            targetField['default'] = '_blank';
                     }
            });
        }

        //////////end of validations////////////////
    </script>
    <?php
    /*
     * load user's project list on top in horizontal scroll div
     */
    $data['select_projectId']=$projectId;
    $this->view('project_list_view', $data);
    
    //store all userid from $project_users arry into a new array named $useridArr
    $useridArr=  array();
    foreach ($project_users as $value) {
        array_push($useridArr, $value['user_id']);
    }
    ?>

<!-- add blog comment form -->
    
    <div class="popup" id="addBlogComment" style="width: 640px;">
        <div class="popup_head">    
    		<span>Add Comment</span>    		
	</div>
        <!--
	<div class="popup_top">
		<img alt="" src="/images/popup_pic1.jpg">
		<div class="run">Add a Comment.</div>
		<div class="clear">&nbsp;</div> 
	</div>
        -->
	<div class="popup_forms" style="padding: 10px 15px 0 20px;">
        <?php
            echo form_open("", array('id'=>'addBlogcommentfrm')); 
            
            $data = array(
                        'name'        => 'projectid_blog_hidden',
                        'id'          => 'projectid_blog_hidden',
                        'value'       => $projectId,
                        'type'        => 'hidden',
                        );

            echo form_input($data);
            
            /*
            echo form_label('Stagename: ', 'stagename'); 
            echo '<br>';
            $data = array(
                            'name'        => 'stagename',
                            'id'          => 'stagename',
                            'value'       => $stage_name,
                            'readonly'    => 'readonly'
                          );
            echo form_input($data);

            echo '<br>';
             * 
             */
            
            echo form_label('Comment: ', 'comment'); 
            $data = array(
                    'name'        => 'comment',
                    'id'          => 'comment',
                    'rows'        => '7',
                    'cols'        => '36',
                  );

            echo form_textarea($data);
            
            echo form_close(); 
        ?>
	
	</div>
	<div class="popup_bottom">
            <a class="but_red floatLeft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('addBlogComment')">cancel</a>
            <a class="but_green floatRight" onfocus="this.blur();" href="javascript:void(0)" onclick="$('#addBlogcommentfrm').submit();">submit</a>
            <div class="clear">&nbsp;</div> 
	</div> 
    </div>
<!-- add blog comment form ends -->

<!-- edit blog comment form -->
<?php //if($last_commentid!=''): ?>
    <div class="popup" id="editBlogCommentDiv" style="width: 640px;">
        <div class="popup_head">    
    		<span>Edit Comment</span>
            <a class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removeblogcomment('<?=$last_commentid; ?>', '<?=$projectId; ?>')">Remove Comment</a>    		
	</div>
        <!--
	<div class="popup_top">
		<img alt="" src="/images/popup_pic1.jpg">
		<div class="run">Edit your last Comment.</div>
		<div class="clear">&nbsp;</div> 
	</div>
        -->
        <div class="popup_forms" style="padding: 10px 15px 0 20px;">
        <?php
        
            echo form_open("", array('id'=>'editblogcommentfrm'));
            $data = array(
                        'name'        => 'commentid_hidden',
                        'id'          => 'commentid_hidden',
                        'value'       => '',
                        'type'        => 'hidden',
                        );

            echo form_input($data);
            /*            
            echo form_label('Stagename: ', 'stagename'); 
            echo '<br>';
            $data = array(
                            'name'        => 'stagename',
                            'id'          => 'stagename',
                            'value'       => $stage_name,
                            'readonly'    => 'readonly'
                          );
            echo form_input($data);

            echo '<br>';
             * 
             */
            
            echo form_label('Comment: ', 'comment'); 
            $data = array(
                    'name'        => 'comment_edit',
                    'id'          => 'comment_edit',
                    'rows'        => '7',
                    'cols'        => '36',
                    'value'       => ''
                  );

            echo form_textarea($data);
            
            echo form_close(); 
            ?>
	
	</div>
	<div class="popup_bottom">
            <a class="but_red floatLeft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('editBlogCommentDiv')">cancel</a>
            <a class="but_green floatRight" onfocus="this.blur();" href="javascript:void(0)" onclick="$('#editblogcommentfrm').submit();">submit</a>
            <div class="clear">&nbsp;</div> 
	</div> 
    </div>
<?php //endif; ?>
<!-- edit blog comment form ends -->
    
  <hr class="hr_grey">
  <div class="clear">&nbsp;</div>
  <?php if($project_status!='viewer' && $project_status!='none'){ ?>
  <div style="position:relative" ><a class="but_down floatLeft" href="javascript:void(0)" id="project_users" onclick="showhideDivData('user_list', this.id)">Project Users</a>
      
      <div class="linkpositioner"><a class="addlink floatLeft" href="javascript:void(0)" onclick="openDiv('addUserProject');">Add</a></div>
      
  </div>
  <div class="clear"></div>
  
  <div class="popup" id="addUserProject" style="width: 640px;">
	<div class="popup_head">
		<span>MANAGE PROJECT USERS</span>	
	</div>
        <div id="adduserHtml">
            <div class="popup_top">
                    <img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
                    <div class="run">Add Users from your User list to the currently selected Project, add Users from currently selected Project to your personal User list or remove Users from Project User list.</div>
                    <div class="clear">&nbsp;</div> 
                    <div class="clear" id="userProjectmsg"></div>
            </div>
            <div class="popup_forms" style="padding: 10px;">
                <div class="popup_forms_full" style="position:relative;" id="varcontent">
                    <div class="leftholder" id="leftholder">
                        <div style="background:#666;position:relative;" id="myusers">My Users</div> 
                      <?php
                      /*
                       * user listing of login user
                       */
                      foreach ($user_listing as $key) {
                          $bgcolor=(in_array($key['child_userid'], $useridArr)) ? 'style="background:#666666;"' : '';
                      ?>
                      <div id="myuser_nameDiv_<?=$key['child_userid'] ?>" class="nameholder" <?=$bgcolor; ?>><?=$key['stage_name']; ?> (<?=$key['email']; ?>)</div>
                      
                      <div id="myuser_linkDiv_<?=$key['child_userid'] ?>" class="linkholder" <?=$bgcolor; ?>>
                        <?php 
                            if(!in_array($key['child_userid'], $useridArr)){ ?>
                            <a href="javascript:void(0)" onfocus="this.blur();" class="ajaxlinks" onclick="adduserpoject('<?=$key['child_userid'] ?>')">add user &gt;</a>
                        <?php
                            }
                            else{
                                echo '&nbsp';
                            }
                        ?>
                      </div>
                      <?php
                      }
                      ?>
                      
                    </div>   
                      
                <div class="rightholder" id="rightholder">
                  <div style="background:#666; float:clear;" id="project_users">Project Users</div>
                    <?php
                    /*
                     * user listing of that users whose are added in project
                     */
                    foreach ($project_users as $key) {
                    ?>
                    <div class="nameholder" id="projectuser_nameDiv_<?=$key['user_id'] ?>"><?=$key['stage_name']; ?> (<?=$key['email']; ?>)</div>
                    <div class="linkholder" id="projectuser_linkDiv_<?=$key['user_id'] ?>"><a href="javascript:void(0)" onfocus="this.blur();" class="ajaxlinks" onclick="removeuserpoject(<?=$key['user_id'] ?>)">remove user &gt;</a></div>
                    <?php
                    }
                    ?>
                
                <?php
                /*
                 * hidden field with value of project id
                 */
                    $data = array(
                    'name'        => 'projectid_hidden',
                    'id'          => 'projectid_hidden',
                    'value'       => $projectId,
                    'type'        => 'hidden',
                    );

                    echo form_input($data);
                ?>
              </div>
              <div class="clear"></div>		 
                   </div>

            </div>
        </div>
	<div class="popup_bottom">
		<a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('addUserProject')">Cancel</a>
                <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="location.reload();">Submit</a>
		<div class="clear">&nbsp;</div> 
	</div>		
    </div>
  <?php
    /*
     * list of all project users
     */
  ?>
    <div id="roleChange_msg" style="display: none"></div>
    <div id="user_list">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th style="width: 130px;">Stage Name</th>
                <th style="width: 190px;">Real Name</th>
                <th style="width: 220px;">Email</th>
                <th style="width: 160px;">Role</th>
<!--                <th style="width: 250px;">Date Added</th>-->
            </tr>
            <?php
               $options = array('1'=>'None', '2'=>'Viewer', '3'=>'Contributor', '4'=>'Co-Owner');

               foreach ($project_users as $key) {
            ?>
                   <tr>
                       <td class="whiteText"><?=$key['stage_name']; ?></td>
                       <td><span class="whiteText"><?=ucwords($key['last_name']); ?></span>, <span class="firstname_color"><?=ucwords($key['first_name']); ?></span></td>
                       <td class="emailcolor"><?=$key['email']; ?></td>
                       <td>
                       <?php 
                           $id = 'id="role_'.$key['user_id'].'" onchange="changeProjectUserRole(this.value, '.$key['id'].')"';
                           echo '<div class="styled-select">';
                           echo form_dropdown('role', $options, $key['role'], $id);
                           echo '</div>';
                       ?>
                       </td>
                       <?php /*<td class="datecolor"><?=date('d-m-Y h:i:s A', strtotime($key['added_date'])); ?></td>*/ ?>
                   </tr>
            <?php

               }
            ?>
         
         
        </table>
     </div>
   
    <hr class="hr_grey">
    <?php } ?> 
    <div style="position:relative" ><a class="but_down floatLeft" href="javascript:void(0)" id="blog_comments" onclick="showhideDivData('blog_list', this.id)">Blog</a>
        <?php if($project_status!='viewer' && $project_status!='none'){ ?>
        <div class="linkpositioner"><a class="addlink floatLeft" href="javascript:void(0)" onclick="openDiv('addBlogComment');">Add</a> 
            <?php
            /*
            if($last_commentid!=''): ?>
                <span class="floatLeft" style="color: #ffffff">&nbsp;|&nbsp;</span> <a class="addlink floatLeft" href="javascript:void(0)" onclick="openDiv('editBlogCommentDiv');">Edit</a>
            <?php endif;
             * 
             */
             ?>
        </div>
        <?php } ?>
    </div>
    <div class="clear"></div>
    <div style="position: relative">
        <div id="blog_list">
        <table cellpadding="0" cellspacing="0">
            <tr id="trheading">
                <th style="width: 130px;">Stage Name</th>
                <th style="width: 650px;">Message</th>
                <th style="width: 150px;">Date Added</th>
                <th style="width: 100px;">Action</th>
            </tr>
            <?php
            $lastid=0;
            if(count($blog)>0){ 
               foreach ($blog as $key=>$value) {
                    if($lastid==0){
                        $lastid=$value['id'];
                    }
            ?>
                    <tr id="userrow<?=$value['id']; ?>">
                        <td class="whiteText" style="vertical-align: top;"><?=$value['stage_name']; ?></td>
                       <td class="emailcolor" style="vertical-align: top;"><?=$value['comment']; ?></td>
                       <td style="vertical-align: top;"class="emailcolor"><?=date('d.m.y, h:i a', strtotime($value['added_date'])); ?></td>
                       <td>
                           <?php
                           if($value['user_id']==$this->session->userdata('user_id')){
                               echo '<a href="javascript:void(0)" onclick="showEditComment('.$value['id'].', '.$projectId.')">Edit</a> | <a href="javascript:void(0)" onclick="return removeblogcomment('.$value['id'].', '.$projectId.')">Delete</a>';
                           }
                           ?>
                           
                       </td>
                    </tr>
            <?php
                    
               }
               //echo '<script>getNewBlogComments('.$lastid.','.$projectId.');</script>';
            }
            //else{
                
            //}
            ?>
         
         
        </table>
     </div>
    </div>
<?php
echo '<script>getNewBlogComments('.$lastid.','.$projectId.');</script>';
?>