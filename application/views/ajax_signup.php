<?php
// ****************************************************************************
// 
//     Ajax file when user add any user in his/her user's list
//
// ****************************************************************************
?>
<div id="register_choice">
    <?php
    $data = array(
                'name'        => 'email_hidden',
                'id'          => 'email_hidden',
                'value'       => $email,
                'type'        => 'hidden',
              );

    echo form_input($data);
    ?>
    Click send email to user for registration <a href="javascript:void(0)" onclick="send_userRegistration_email()">Send Email</a> <br>
    Or Click to register if you want to register user yourself <a href="javascript:void(0)" onclick="showregistration()">Register</a>
</div>
<div id="user_registration" style="display: none" class="popup_forms">
<h4>User Registration</h4>
<?php echo validation_errors('<p class="error">'); ?>
	<?php 
                echo form_open(""); ?>
                
		<p>
                    <?php
			echo form_label('First Name', 'new_first_name'); 
                        $data = array(
                                        'name'        => 'new_first_name',
                                        'id'          => 'new_first_name',
                                        'class'       => 'form-control'
                                      );

                          echo form_input($data);
                        ?>
		</p>  
                <p>
                    <?php
			echo form_label('Last Name', 'new_last_name'); 
                        $data = array(
                                        'name'        => 'new_last_name',
                                        'id'          => 'new_last_name',
                                        'class'       => 'form-control'
                                      );

                          echo form_input($data);
                        ?>
		</p>
                
                <p>
                    <?php
			echo form_label('Your Email', 'new_email_address'); 
                        $data = array(
                                        'name'        => 'new_email_address',
                                        'id'          => 'new_email_address',
                                        'value'       => $email,   
                                        'readonly'    => 'readonly',
                                        'class'       => 'form-control'
                                      );

                          echo form_input($data);
                        ?>
		</p>
		<p>
                    <?php
			echo form_label('Password', 'new_password'); 
                        $data = array(
                                        'name'        => 'new_password',
                                        'id'          => 'new_password',
                                        'value'       => $password,     
                                        'class'       => 'form-control'
                                      );

                          echo form_input($data);
                        ?>
		</p>
		
		<p>
                    <?php
			echo form_label('Stage Name', 'new_stage'); 
                        $data = array(
                                        'name'        => 'new_stage',
                                        'id'          => 'new_stage',
                                        'class'       => 'form-control'
                                      );

                          echo form_input($data);
                        ?>
		</p> 
                
		<p>
                    <?php 
                    /*
                            $data = array(
                                'name' => 'new_sbt',
                                'id' => 'new_sbt',
                                'value' => 'Submit',
                                'content' => 'Submit',
                                'class' => 'btn btn-primary',
                                'onclick' => 'return chkNewUserSignup()',
                            );

                            echo form_button($data); 
                     * 
                     */
                    ?>
		</p>
	<?php echo form_close(); ?>
</div><!--<div class="reg_form">-->    
<script>
    $(document).ready(function(){
        $("#addUserDiv #adduserbtn").attr("onclick", "return chkNewUserSignup()");
    });
</script>