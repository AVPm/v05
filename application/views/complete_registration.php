<?php
// ******************************************************************************************************
// 
//     Complete Registration page view when user comes from link when any user add him/her in user's list
//
// ******************************************************************************************************
?>
<div id="content">
<?php echo @$activate_msg; ?>
<div class="reg_form">
<div class="form_title">Sign Up</div>
<div class="form_sub_title">Please complete your registration</div>
<?php echo validation_errors('<p class="error">'); ?>
	<?php //$data = array('onsubmit' => "return chkSignup()");
                echo form_open(""); 
                $data = array(
                    'name'        => 'userid_hidden',
                    'id'          => 'userid_hidden',
                    'value'       => $userid,
                    'type'        => 'hidden',
                  );

                echo form_input($data);
        ?>
               
		<p>
                    <?php
			echo form_label('First Name', 'first_name'); 
                        $data = array(
                                        'name'        => 'first_name',
                                        'id'          => 'first_name',
                                        'value'       => set_value('first_name'),                                        
                                      );

                          echo form_input($data);
                        ?>
		</p>  
                <p>
                    <?php
			echo form_label('Last Name', 'last_name'); 
                        $data = array(
                                        'name'        => 'last_name',
                                        'id'          => 'last_name',
                                        'value'       => set_value('last_name'),                                        
                                      );

                          echo form_input($data);
                        ?>
		</p>
                
                <p>
                    <?php
			echo form_label('Company Name', 'company_name'); 
                        $data = array(
                                        'name'        => 'company',
                                        'id'          => 'company',
                                        'value'       => set_value('company'),                                        
                                      );

                          echo form_input($data);
                        ?>
                </p>
                
                <p>
                    <?php
			echo form_label('Your Email', 'email_address'); 
                        $data = array(
                                        'name'        => 'email_address',
                                        'id'          => 'email_address',
                                        'value'       => $useremail,   
                                        'readonly'    => 'readonly',
                                      );

                          echo form_input($data);
                        ?>
		</p>
		<p>
                    <?php
			echo form_label('Password', 'password'); 
                        $data = array(
                                        'name'        => 'password',
                                        'id'          => 'password',
                                        'value'       => set_value('password'),                                        
                                      );

                          echo form_password($data);
                        ?>
		</p>
		<p>
                    <?php
			echo form_label('Confirm Password', 'con_password'); 
                        $data = array(
                                        'name'        => 'con_password',
                                        'id'          => 'con_password',
                                        'value'       => set_value('con_password'),                                        
                                      );

                          echo form_password($data);
                        ?>
		</p>
		<p>
                    <?php
			echo form_label('Stage Name', 'stage'); 
                        $data = array(
                                        'name'        => 'stage',
                                        'id'          => 'stage',
                                        'value'       => set_value('stage'),                                        
                                      );

                          echo form_input($data);
                        ?>
		</p> 
                
		<p>
                    <?php 
                            $data = array(
                                'name' => 'sbt',
                                'id' => 'sbt',
                                'value' => 'Submit',
                                'type' => 'submit',
                                'content' => 'Submit',
                                'class' => 'greenButton'
                            );

                            echo form_button($data); ?>
		</p>
	<?php echo form_close(); ?>
</div><!--<div class="reg_form">-->    
</div><!--<div id="content">-->