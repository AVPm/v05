<?php
$classDashboard='';
$classSubscription='';
$classFileManage='';
?>
<div class="left-bar ">
	<div class="admin-logo">
                <div class="logo-holder pull-left pricing-price logo-text" style="padding-top: 19px; padding-bottom: 0px; font-size: 27px;">
			<font style="color:#90d300">AVP</font>matrix	
		</div>
		<!-- logo-holder -->			
		<a href="#" class="menu-bar  pull-right"><i class="ti-menu"></i></a>
	</div>
	<!-- admin-logo -->
	<ul class="list-unstyled menu-parent" id="mainMenu">
		<li>
			<a href="<?=base_url('user'); ?>" class="waves-effect waves-light">
				<i class="icon ti-home"></i>
				<span class="text ">Dashboard</span>
			</a>
		</li>
                <?php
                if(isset($project_menu) && $project_menu=='yes'){
                    $currclassImages='';
                    $currclassVideos='';
                    $currclassFiles='';
                    $currclassWebpages='';
                ?>
                <?php if(isset($selected_menu) && $selected_menu=='images'){ $currclassImages='current'; } ?>
                <li class="<?=$currclassImages ?>">
                        <a class="waves-effect waves-light" href="<?=base_url('project/images/'.$projectId); ?>">
                                <i class="icon ti-image"></i>
                                <span class="text ">Images</span>
                        </a>
                </li>
                <?php if(isset($selected_menu) && $selected_menu=='videos'){ $currclassVideos='current'; } ?>
                <li class="<?=$currclassVideos ?>">
                        <a class="waves-effect waves-light" href="<?=base_url('project/videos/'.$projectId); ?>">
                                <i class="fa fa-file-video-o"></i>
                                <span class="text ">Videos</span>
                        </a>
                </li>
                <?php if(isset($selected_menu) && $selected_menu=='files'){ $currclassFiles='current'; } ?>
                <li class="<?=$currclassFiles ?>">
                        <a class="waves-effect waves-light" href="<?=base_url('project/files/'.$projectId); ?>">
                                <i class="fa fa-file-o"></i>
                                <span class="text ">Files</span>
                        </a>
                </li>
                <?php if(isset($selected_menu) && $selected_menu=='webpages'){ $currclassWebpages='current'; } ?>
                <li class="<?=$currclassWebpages ?>">
                        <a class="waves-effect waves-light" href="<?=base_url('project/webpages/'.$projectId); ?>">
                                <i class="fa fa-globe"></i>
                                <span class="text ">Webpages</span>
                        </a>
                </li>
                <?php    
                }
                ?>
                <?php if(isset($selected_menu_subscrip) && $selected_menu_subscrip=='yes'){ $classSubscription='current'; } ?>
                <li class="<?=$classSubscription; ?>">
			<a href="<?=base_url('user/subscriptions'); ?>" class="waves-effect waves-light">
				<i class="icon ti-money"></i>
				<span class="text ">Subscription</span>
			</a>
		</li>
                <?php if(isset($selected_menu_file_manage) && $selected_menu_file_manage=='yes'){ $classFileManage='current'; } ?>
                <li class="<?=$classFileManage; ?>">
			<a href="<?=base_url('user/file_management'); ?>" class="waves-effect waves-light">
				<i class="icon ti-folder"></i>
				<span class="text ">File Management</span>
			</a>
		</li>
                <?php /* ?>
		<li class="submenu">
			<a class="waves-effect waves-light" href="#layouts">
				<i class="icon ti-layout"></i>
				<span class="text">Unique Layouts</span>
				<i class="chevron ti-angle-right"></i>
			</a>
			<ul class="list-unstyled">
				<li><a href="#">Collapsed Sidebar</a></li>
				<li><a href="#">Menu with header</a></li>
				<li><a href="#">Horizontal Menu</a></li>					
				<li><a href="#">Right Sidebar</a></li>
				<li><a href="#">Boxed Layout</a></li>
				<li><a href="#">Static Sidebar</a></li>
			</ul>
		</li>
		<li class="submenu">
			<a class="waves-effect waves-light" href="#piluku_premium">
				<i class="icon ti-gift"></i>
				<span class="text">Piluku Premium</span>
				<i class="chevron ti-angle-right"></i>
			</a>
			<ul class="list-unstyled" id="piluku_premium">
				<li><a href="#">Widgets</a></li>
				<li><a href="#">Tasks</a></li>
				<li><a href="#">Mailbox</a></li>
				<li><a href="#">Profile</a></li>
				<li><a href="#">Invoice</a></li>
				<li><a href="#">Timeline</a></li>
				<li class='current'><a class='current' href="#">Pricing</a></li>
				<li><a href="#">Gallery</a></li>
				<li><a href="#">Masonry Gallery</a></li>
				<li><a href="#">Rotated Gallery</a></li>
			</ul>
		</li>
		<li>
			<a href="#">
				<i class="icon ti-smallcap"></i>
				<span class="text">Typography</span>
			</a>
		</li>				
		<li class="submenu">
			<a class="waves-effect waves-light" href="#components">
				<i class="icon ti-briefcase"></i>
				<span class="text">Components</span>
				<i class="chevron ti-angle-right"></i>
			</a>
			<ul class="list-unstyled" id="components">
				<li class="submenu">
					<a class="waves-effect waves-light" href="#alerts">
						Alerts
						<span class="pull-right drop-arrow">
							<i class="drop-indicator ti-angle-right chevron"></i>
						</span>
					</a>
					<ul class="list-unstyled" id="alerts">
						<li><a href="#">Basic Alerts</a></li>
						<li><a href="#">Sweet Alerts</a></li>
					</ul>
				</li>
				<li><a href="#">Progress Bars</a></li>
				<li><a href="#">Dropdowns</a></li>
				<li><a href="#">Info Boxes</a></li>
				<li><a href="#">Notifications</a></li>
				<li><a href="#">Buttons</a></li>
				<li><a href="#">Tree View</a></li>
				<li><a href="#">CSS3 Animations</a></li>
				<li><a href="#">Sliders</a></li>
				<li><a href="#">Nestable Lists</a></li>
				<li><a href="#">Carousel</a></li>
				<li><a href="#">Portlets</a></li>
				<li class="submenu">
					<a class="waves-effect waves-light" href="#icons">
						Icons
						<span class="pull-right drop-arrow">
							<i class="drop-indicator ti-angle-right chevron"></i>
						</span>
					</a>
					<ul class="list-unstyled" id="icons">
						<li><a href="#">Ion Icons</a></li>
						<li><a href="#">Font Awesome</a></li>
						<li><a href="#">Themify Icons</a></li>
					</ul>
				</li>
				<li><a href="#">Tooltips</a></li>
				<li><a href="#">Labels Badges</a></li>
				<li><a href="#">List Groups</a></li>
				<li><a href="#">Breadcrumbs</a></li>
				<li><a href="#">Tabs Accordions</a></li>
				<li><a href="#">File Manager</a></li>
				<li><a href="#">Modals</a></li>
				<li><a href="#">Pagination</a></li>

			</ul>
		</li>
		<li class="submenu">
			<a class="waves-effect waves-light" href="#forms_elements">
				<i class="icon ti-book"></i>
				<span class="text">Forms</span>
				<i class="chevron ti-angle-right"></i>
			</a>
			<ul class="list-unstyled">
				<li><a href="#">Dropzone File Upload</a></li>
				<li><a href="#">Form Validation</a></li>
				<li><a href="#">Form Wizard</a></li>
				<li><a href="#">Input Groups</a></li>
				<li><a href="#">Form Elements</a></li>
				<li><a href="#">Multiple File Upload</a></li>
				<li><a href="#">Image Crop Zoom</a></li>
				<li><a href="#">WYZIWIG &amp; Markdown</a></li>
			</ul>
		</li>
		<li class="submenu">
			<a class="waves-effect waves-light" href="#tables">
				<i class="icon ti-layout-grid2"></i>
				<span class="text">Tables</span>
				<i class="chevron ti-angle-right"></i>
			</a>
			<ul class="list-unstyled" id="tables">
				<li><a href="#">Basic Tables</a></li>
				<li><a href="#">Dynamic Tables</a></li>
				<li><a href="#">Editable Tables</a></li>
				<li><a href="#">Users Table</a></li>
			</ul>
		</li>
		<li class="submenu">
			<a class="waves-effect waves-light" href="#piluku_utility">
				<i class="icon ti-heart"></i>
				<span class="text">Piluku Utility</span>
				<i class="chevron ti-angle-right"></i>
			</a>
			<ul class="list-unstyled" id="piluku_utility">
				<li class="submenu">
					<a class="waves-effect waves-light" href="#register">
						Register
						<span class="pull-right drop-arrow">
							<i class="drop-indicator ti-angle-right chevron"></i>
						</span>
					</a>
					<ul class="list-unstyled" id="register">
						<li><a href="#">Modal One</a></li>
						<li><a href="#">Modal Two</a></li>
					</ul>
				</li>
				<li class="submenu">
					<a class="waves-effect waves-light" href="#login">
						Login
						<span class="pull-right drop-arrow">
							<i class="drop-indicator ti-angle-right chevron"></i>
						</span>
					</a>
					<ul class="list-unstyled" id="login">
						<li><a href="#">Modal One</a></li>
						<li><a href="#">Modal Two</a></li>
					</ul>
				</li>
				<li><a href="#">Forgot Password</a></li>
				<li><a href="#">Lock Screen</a></li>
				<li><a href="#">Lock Screen 2</a></li>
				<li><a href="#">FAQ</a></li>
				<li><a href="#">404</a></li>
				<li><a href="#">505</a></li>
				<li><a href="#">Template</a></li>
			</ul>
		</li>
		<li class="submenu">
			<a class="waves-effect waves-light" href="#charts">
				<i class="icon ti-bar-chart-alt"></i>
				<span class="text">Charts</span>
				<i class="chevron ti-angle-right"></i>
			</a>
			<ul class="list-unstyled" id="charts">
				<li><a href="#">Line &amp; Area Charts</a></li>
				<li><a href="#">Bar Charts</a></li>
				<li><a href="#">Pie Charts</a></li>
				<li><a href="#">NVD3 Charts</a></li>
			</ul>
		</li>
		<li class="submenu">
			<a class="waves-effect waves-light" href="#maps">
				<i class="icon ti-location-pin"></i>
				<span class="text">Maps</span>
				<i class="chevron ti-angle-right"></i>
			</a>
			<ul class="list-unstyled" id="maps">
				<li class="submenu">
					<a class="waves-effect waves-light" href="#google_maps">
						Google Maps
						<span class="pull-right drop-arrow">
							<i class="drop-indicator ti-angle-right chevron"></i>
						</span>
					</a>
					<ul class="list-unstyled" id="google_maps">
						<li><a href="#">Basic Maps</a></li>
						<li><a href="#">Markers Maps</a></li>
						<li><a href="#">Routes Maps</a></li>
					</ul>
				</li>
				<li><a href="#">Vector Maps</a></li>
			</ul>
		</li>

		<li class="submenu">
			<a class="waves-effect waves-light" href="#menu_levels">
				<i class="icon ti-layout-list-thumb"></i>
				<span class="text">Menu Levels</span>
				<i class="chevron ti-angle-right"></i>
			</a>
			<ul class="list-unstyled" id="menu_levels">
				<li class="submenu">
					<a class="waves-effect waves-light" href="#menu_level_one">Menu Level 1.1
						<span class="pull-right drop-arrow">
							<i class="drop-indicator ti-angle-right chevron"></i>
						</span>
					</a>
					<ul class="list-unstyled" id="menu_level_one">
						<li><a href="#">Menu Level 2.1</a></li>
						<li><a href="#">Menu Level 2.2</a></li>
						<li><a href="#">Menu Level 2.3</a></li>
					</ul>
				</li>
				<li><a href="#">Menu Level 1.2</a></li>
				<li><a href="#">Menu Level 1.3</a></li>
			</ul>
		</li>
                
                <?php */ ?>
	</ul>
</div>
<!-- left-bar -->