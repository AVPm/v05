</div>
<!-- wrapper -->

<script src='<?=base_url(); ?>assets/js/jquery-ui-1.10.3.custom.min.js'></script>
<script src='<?=base_url(); ?>assets/js/bootstrap.min.js'></script>
<script src='<?=base_url(); ?>assets/js/jquery.nicescroll.min.js'></script>
<script src='<?=base_url(); ?>assets/js/wow.min.js'></script>
<script src='<?=base_url(); ?>assets/js/jquery.loadmask.min.js'></script>
<script src='<?=base_url(); ?>assets/js/jquery.accordion.js'></script>
<script src='<?=base_url(); ?>assets/js/materialize.js'></script>
<script src='<?=base_url(); ?>assets/js/bic_calendar.js'></script>

<script src='<?=base_url(); ?>assets/js/core.js'></script>
<script src='<?=base_url(); ?>js/common.js'></script>

<script src="<?=base_url(); ?>assets/js/jquery.countTo.js"></script>
<script src='<?=base_url(); ?>assets/js/bootstrap-filestyle.js'></script>
<?php
if(isset($jsArray)):
    foreach ($jsArray as $js) {
        echo '<script type="text/javascript" src="'.base_url().'js/'.$js.'.js"></script>';
        echo "\n";
    }
endif;
?>
</body>
</html>