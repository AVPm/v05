<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
    <meta http-equiv="X-Frame-Options" content="allow">
  <title>AVPmatrix | <?=@$title ?></title>

  <!-- <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
  <link rel="icon" href="images/favicon.ico" type="image/x-icon"> -->
  <!-- Bootstrap CSS -->

  
        <!-- Hammer reload -->
        <?php 
        /*
          <script>
            setInterval(function(){
              try {
                if(typeof ws != 'undefined' && ws.readyState == 1){return true;}
                ws = new WebSocket('ws://'+(location.host || 'localhost').split(':')[0]+':35353')
                ws.onopen = function(){ws.onclose = function(){document.location.reload()}}
                ws.onmessage = function(){
                  var links = document.getElementsByTagName('link'); 
                    for (var i = 0; i < links.length;i++) { 
                    var link = links[i]; 
                    if (link.rel === 'stylesheet' && !link.href.match(/typekit/)) { 
                      href = link.href.replace(/((&|\?)hammer=)[^&]+/,''); 
                      link.href = href + (href.indexOf('?')>=0?'&':'?') + 'hammer='+(new Date().valueOf());
                    }
                  }
                }
              }catch(e){}
            }, 1000)
          </script>
          */
        ?>
        <!-- /Hammer reload -->
      
  <link rel='stylesheet' href='<?=base_url(); ?>assets/css/bootstrap.min.css'>
<link rel='stylesheet' href='<?=base_url(); ?>assets/css/material.css'>

<?php if($this->session->userdata('theme')!='' && $this->session->userdata('theme')!='default'){ ?>
<link rel='stylesheet' href='<?=base_url(); ?>assets/css/style-<?=$this->session->userdata('theme'); ?>.css'>
<?php } else{ ?>
<link rel='stylesheet' href='<?=base_url(); ?>assets/css/style.css'>
<?php } ?>

<link rel='stylesheet' href='<?=base_url(); ?>assets/css/animated-masonry-gallery.css'>
<link rel='stylesheet' href='<?=base_url(); ?>assets/css/rotated-gallery.css'>
<link rel='stylesheet' href='<?=base_url(); ?>assets/css/sweet-alerts/sweetalert.css'>
<link rel='stylesheet' href='<?=base_url(); ?>assets/css/jtree.css'>
<?php
$userid = $this->session->userdata('user_id');
if(isset($cssArray)):
    foreach ($cssArray as $css) {
        echo '<link rel="stylesheet" type="text/css" href="'.base_url().'css/'.$css.'.css" />';
        echo "\n";
    }
endif;
?>
  <script src='<?=base_url(); ?>assets/js/jquery.js'></script>
<script src='<?=base_url(); ?>assets/js/app.js'></script>
  <script>
    jQuery(window).load(function () {
      $('.piluku-preloader').addClass('hidden');
    });
    var base_url='<?=base_url(); ?>';  
    var V04_PATH='<?=V04_PATH; ?>';  
  </script>
</head>
<body>
  <div class="piluku-preloader text-center">
  <!-- <div class="progress">
      <div class="indeterminate"></div>
  </div> -->
  <div class="loader">Loading...</div>
</div>
    <div class="wrapper <?php echo $class= (isset($project_menu) && $project_menu=='yes' && !isset($showfulldiv)) ? 'mini-bar' : '' ?>">