<?php
// ****************************************************************************
// 
//     Registration page view 
//
// ****************************************************************************
?>

<!--<div class="signup_wrap">-->

<!-- Simple AnythingSlider -->

<ul id="slider">

        <li><img src="/images/slide-civil-1.jpg" alt=""></li>

        <li><img src="/images/slide-env-1.jpg" alt=""></li>

        <li><img src="/images/slide-civil-2.jpg" alt=""></li>

        <li><img src="/images/slide-env-2.jpg" alt=""></li>

</ul>

<!-- END AnythingSlider -->
<?php echo @$activate_msg; ?>
<center>
    <a href="javascript:void(0)" onclick="openDiv('formdiv');" class="singup_link">Signup</a>
    <div class="popup" id="formdiv" style="width: 640px;">
	<div class="popup_head">
		<span>Sign up</span>	
	</div>
	<div class="popup_top">
		<table cellspacing="0" cellpadding="0" border="0">
		<tbody><tr>
		<td width="35%" class="leftcolumn"><span class="hg">Plan:</span>
		</td>
		<td width="65%" class="rightcolumn">TRIAL
		</td>
		</tr>
		<tr>
		<td width="35%" class="leftcolumn"><span class="hg">Traffic:</span>
		</td>
		<td width="65%" class="rightcolumn">1000 GB
		</td>
		</tr>
		<tr>
		<td width="35%" class="leftcolumn"><span class="hg">Users:</span>
		</td>
		<td width="65%" class="rightcolumn">100
		</td>
		</tr>
		<tr>
		<td width="35%" class="leftcolumn"><span class="hg">30 Day Trial:</span>
		</td>
		<td width="65%" class="rightcolumn"><span class="hc">Free</span>
		</td>
		</tr>
		</tbody></table>
		<div class="run">Fill out the form below to sign up for a trial account.</div>
		<div class="clear"></div> 
	</div>
	<div class="popup_forms">
        <?php 
            echo validation_errors('<p class="error">');
            $data = array('onsubmit' => "return chkSignup()", 'id'=>'signup_form');
            echo form_open("user/registration", $data); ?>
            <div class="floatLeft">
                <?php
                echo form_label('First Name:', 'first_name'); 
                echo '<br>';
                $data = array(
                                'name'        => 'first_name',
                                'id'          => 'first_name',
                                'value'       => set_value('first_name'),
                                'class'       => 'smallinput',
                              );

                echo form_input($data);
                ?>
            </div>
            
            <div class="floatLeft"> 
            <?php
            echo form_label('Last Name:', 'last_name'); 
            echo '<br>';
            $data = array(
                            'name'        => 'last_name',
                            'id'          => 'last_name',
                            'value'       => set_value('last_name'),  
                            'class'       => 'smallinput',
                          );

            echo form_input($data);
            ?>
            </div>			
            <div class="clear">
                <?php
                    echo form_label('Company Name:', 'company_name'); 
                    echo '<br>';
                    $data = array(
                                    'name'        => 'company',
                                    'id'          => 'company',
                                    'value'       => set_value('company'),                                        
                                  );

                    echo form_input($data);
                    echo '<br>';
                ?>
            </div>
        <br>
        <br>

        <?=form_label('eMail Address:', 'email_address'); ?>&nbsp;&nbsp;<span class="hg">(used as login name)</span><br>
        <?php
        $data = array(
                      'name'        => 'email_address',
                      'id'          => 'email_address',
                      'value'       => set_value('email_address'),   
                      'onchange'    => 'return chk_useremail()',
                    );

        echo form_input($data);
        ?>
        &nbsp; <span id="useremail_msg" style="display: none">Avaliable</span>
          <br>	
          <?php
              echo form_label('Password:', 'password'); 
              echo '<br>';
              $data = array(
                              'name'        => 'password',
                              'id'          => 'password',
                              'value'       => set_value('password'),                                        
                            );

              echo form_password($data);
              echo '<br>';

              echo form_label('Password:', 'con_password'); 
              echo '&nbsp;&nbsp;<span class="hg">(Re-Enter)</span><br>';
              $data = array(
                              'name'        => 'con_password',
                              'id'          => 'con_password',
                              'value'       => set_value('con_password'),                                        
                            );

              echo form_password($data);
              echo '<br>';

              echo form_label('Stage Name:', 'stage'); 
              echo '<br>';
              $data = array(
                              'name'        => 'stage',
                              'id'          => 'stage',
                              'value'       => set_value('stage'),                                        
                            );

              echo form_input($data);

              echo form_close(); ?>
            </div>
            <div class="popup_bottom">
                    <a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('formdiv')">Cancel</a>
                    <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="$('#signup_form').submit();">Submit</a>
                    <div class="clear">&nbsp;</div> 
            </div>
    </div>
</center>


<?php
//old html of signup form
/*
<div class="reg_form">
<div class="form_title">Sign Up</div>
<div class="form_sub_title">It's free and anyone can join</div>
<?php echo validation_errors('<p class="error">'); ?>
	<?php $data = array('onsubmit' => "return chkSignup()");
                echo form_open("user/registration", $data); ?>
                <p>
                    <?php
			echo form_label('User Name', 'user_name'); 
                        $data = array(
                                        'name'        => 'user_name',
                                        'id'          => 'user_name',
                                        'value'       => set_value('user_name'),
                                        'onchange'    => 'return chk_username()',
                                        
                                      );

                          echo form_input($data);
                        ?>
                    &nbsp;
                    <span id="username_msg"></span>
		</p>
		<p>
                    <?php
			echo form_label('First Name', 'first_name'); 
                        $data = array(
                                        'name'        => 'first_name',
                                        'id'          => 'first_name',
                                        'value'       => set_value('first_name'),                                        
                                      );

                          echo form_input($data);
                        ?>
		</p>  
                <p>
                    <?php
			echo form_label('Last Name', 'last_name'); 
                        $data = array(
                                        'name'        => 'last_name',
                                        'id'          => 'last_name',
                                        'value'       => set_value('last_name'),                                        
                                      );

                          echo form_input($data);
                        ?>
		</p>
                
                <p>
                    <?php
			echo form_label('Company Name', 'company_name'); 
                        $data = array(
                                        'name'        => 'company',
                                        'id'          => 'company',
                                        'value'       => set_value('company'),                                        
                                      );

                          echo form_input($data);
                        ?>
                </p>
                
                <p>
                    <?php
			echo form_label('Your Email', 'email_address'); 
                        $data = array(
                                        'name'        => 'email_address',
                                        'id'          => 'email_address',
                                        'value'       => set_value('email_address'),   
                                        'onchange'    => 'return chk_useremail()',
                                      );

                          echo form_input($data);
                        ?>
                    &nbsp; <span id="useremail_msg" style="display: none">Avaliable</span>
		</p>
		<p>
                    <?php
			echo form_label('Password', 'password'); 
                        $data = array(
                                        'name'        => 'password',
                                        'id'          => 'password',
                                        'value'       => set_value('password'),                                        
                                      );

                          echo form_password($data);
                        ?>
		</p>
		<p>
                    <?php
			echo form_label('Confirm Password', 'con_password'); 
                        $data = array(
                                        'name'        => 'con_password',
                                        'id'          => 'con_password',
                                        'value'       => set_value('con_password'),                                        
                                      );

                          echo form_password($data);
                        ?>
		</p>
		<p>
                    <?php
			echo form_label('Stage Name', 'stage'); 
                        $data = array(
                                        'name'        => 'stage',
                                        'id'          => 'stage',
                                        'value'       => set_value('stage'),                                        
                                      );

                          echo form_input($data);
                        ?>
		</p> 
                
		<p>
                    <?php 
                            $data = array(
                                'name' => 'sbt',
                                'id' => 'sbt',
                                'value' => 'Submit',
                                'type' => 'submit',
                                'content' => 'Submit',
                                'class' => 'greenButton'
                            );

                            echo form_button($data); ?>
		</p>
	<?php echo form_close(); ?>
</div><!--<div class="reg_form">-->    
 * 
 */
?>
