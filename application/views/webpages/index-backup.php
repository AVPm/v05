<?php
// ****************************************************************************
// 
//     User's Webpages list detail view
//
// ****************************************************************************


?>
<style>
    .bottom_table tr th{
        font-size: 12px;
    }
</style>

<!-- add project HTML -->
<div class="popup" id="addProjectFolder_div" style="width: 820px;">
            
        <div class="popup_head">
            <a class="on" href="#" id="image_only">Add Webpage</a>
        </div>	
            
        <div class="popup_forms" style="padding: 0px">
            <form action="" enctype="multipart/form-data" method="post" id="form1">

                <div id="fileuploader">
                    <iframe src="<?=base_url('includes/file_uploader')  ?>" style="height: 250px; width:100%" frameborder="0" ></iframe> 
                </div>    


            </form>

        </div>
        <div class="popup_bottom">
            <a class="but_red floatLeft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('addProjectFolder_div')">cancel</a>
            <a class="but_green floatright" onfocus="this.blur();" href="javascript:void(0)" onclick="window.location.reload()">done</a>
                <div class="clear">&nbsp;</div> 
        </div> 
</div>


<div class="clear">&nbsp;</div>

<div></div>
<h4>Webpages Module</h4>
<div class="clear">&nbsp;</div>
<div>
    <a href="javascript:void(0)" style="color:#fff" onclick="openDiv('addProjectFolder_div')"><img src="<?=base_url('images/folder_add.png'); ?>"> Add Webpage</a>
</div>
<div class="clear">&nbsp;</div>
<form name="fileManageForm" id="fileManageForm" method="POST" action="">
    <input type="hidden" name="type" value="album" id="type" />
    <input type="button" name="deletechk" id="deletechk" value="Delete Selected" disabled="disabled">
<table class="bottom_table">
    <tbody>
        <tr>
            <th style="width: 5px; "><input type="checkbox" name="select_all_chk" id="select_all_chk" value="yes"></th>
            <th>Name</th>
            <th>DATE</th>
            <th>Action</th>
            
        </tr>
        <?php
        foreach ($list_wepages as $key => $value) {
        ?>
        <tr id="webpagerow_<?=$value['id']; ?>">
            <td class="tableheading" style="width: 5px;"><input type="checkbox" name="selectFolder[]" value="<?=$value['id']; ?>" class="checkbox"></td>
            <td class="tableheading">
                
                <span class="project_title" id="project_title_<?=$value['id']; ?>"><?=ucwords(strtolower($value['foldername'])); ?></span>
            </td>
            <td class="tableheading"><?=date('d-M-Y', strtotime($value['created_date']));?></td>
            <td class="tableheading"><a href="<?=base_url('user/webpages/'.$value['id']) ?>" target="_blank">view</a> | <a href="javascript:void(0)" >Delete</a></td>
        </tr>
        <?php
        }
        ?>
        
</tbody>
    </table>
</form>
<script>
    
    function chkaddProjectFolder(){
        var title=$('#project_album_title').val();
        if(title=='' || title==null){
            $('#project_album_title').css('border', '1px solid red');
            $('#project_album_title').focus();
            return false;
        }
    }
    
    function showrename(id, type){
        $('.project_title').show();
        $('.foldername').hide();
        $('#project_title_'+type+'_'+id).hide();
        $('#foldernametxt_'+type+'_'+id).show();
    }
    
    function removeProjectalbum(albumid, table){
        var r = confirm("Are you sure to remove this?");
        
        if (r == true) {
            $.ajax({
                url:base_url+"project/removealbum/",
                type: 'POST',
                data: 'albumid='+albumid+"&table="+table,
                cache: false,
                global: false,
                success:function(msg){ 
                    $('#projectrow_'+table+'_'+albumid).remove();
                }    			
            });
        } else {
            return false;
        }
    }
    
    function renamefolder(id, type, albumtype){
        var name=$('#foldernametxt_'+type+'_'+id).val();
        var id=$('#folderidtxt_'+type+'_'+id).val();
        $.ajax({
            url:base_url+"user/file_management/",
            type: 'POST',
            data: 'rename_folder='+name+'&type='+albumtype+'&id='+id,
            cache: false,
            global: false,
            success:function(msg){ 
                if(msg=='success'){
                    $('#project_title_'+type+'_'+id).html($('#foldernametxt_'+type+'_'+id).val());
                    $('#project_title_'+type+'_'+id).show();
                    $('#foldernametxt_'+type+'_'+id).hide();
                }
                else{

                }
                $('#adduserHtml').html(msg);
            }    			
        }); 
    }
    
</script>
