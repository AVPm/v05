<?php // echo '<pre>'; print_r($_SESSION); die; $_SESSION['payment_status'];//echo '<pre>'; print_r($userSubscriptions); die; ?>
<div class="panel-piluku">
        <div class="col-md-12 panel-piluku">
                <!--                        *** Pricing Tables ***-->
                <!--pricing table-->
                <div class="form-heading">
                        My subscriptions
                </div>
                <!--pricing table-->
                <!--                               row-->
                <div class="row panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                    <thead>
                                            <tr>
                                                    <th>#</th>
                                                    <th>Package Name</th>
                                                    <th>Order Date</th>
                                                    <th>Activation Date</th>
                                                    <th>Expiry Date</th>
                                                    <th>Users</th>
                                                    <th>Traffic</th>
                                            </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $i=1;
                                            foreach ($userSubscriptions as $key => $value) {
                                        ?>
                                            <tr>
                                                    <th scope="row"><?=$i; ?></th>
                                                    <td><?=strtoupper($value['name']); ?></td>
                                                    <td><?=date('d-M-Y', strtotime($value['order_date'])); ?></td>
                                                    <td>
                                                        <?php
                                                        if($value['status']=='1'){
                                                            echo date('d-M-Y', strtotime($value['activation_date'])); 
                                                        }
                                                        else{
                                                            echo '<a href="'.base_url('subscription/activate/'.$value['invoice']).'">Activate Now</a>';
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if($value['status']=='1')
                                                            echo date('d-M-Y', strtotime($value['expiry_date'])); 
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php 
                                                            echo 'Total: '. $value['users']; 
                                                            echo '<br />';
                                                            echo 'Remaining: '. $remainingUsers;
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                            echo 'Total: '.$value['traffic_storage'].' GB';
                                                            echo '<br />';
                                                            echo 'Remaining: '.number_format(($value['traffic_storage']-$trafficSize), 3);
                                                        ?>
                                                    </td>
                                            </tr>
                                        <?php
                                        $i++;
                                        }
                                        ?>
                                            
                                    </tbody>
                            </table>
                    </div>
                </div>
                <!--                                /row-->
                <!--pricing table-->
                <div class="form-heading">
                        Subscriptions Packages
                </div>
                <!--pricing table-->
                <!--                               row-->
                <div class="row panel-body">
					<!--                                   col md 4-->
                <?php
                foreach ($subscriptions as $key => $value) {
                ?>
                    <div class="col-md-3">
                            <div class="pricing-header">
                                    <?=strtoupper($value['name']); ?>
                            </div>
                            <div class="pricing-price">
                                <?php
                                if($value['price']==0){
                                    echo 'Free';
                                }
                                else{
                                    echo '$ '.str_pad($value['price'], 3, "0", STR_PAD_LEFT).'<span>/mo</span>';
                                }
                                ?>
                            </div>
                            <ul class="list-unstyled pricing-features">
                                    <li><span><?=$value['users']; ?></span> users</li>
                                    <li><span><?=$value['traffic_storage']; ?> GB</span> storage</li>
                                    <li>24/7 customer support</li>
                                    <li> <a class="btn btn-primary btn-lg" href="<?=base_url('subscription/buy/'.$value['id']); ?>">Buy Now</a></li>
                            </ul>
                    </div>
                <?php
                    //echo '<th>'.strtoupper($value['name']).'</th>';
                }
                ?>
                    
                
            </div>
                <!--                                /row-->
        </div>
</div>