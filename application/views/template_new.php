<?php
// ****************************************************************************
// 
//     Main single template view of site
//
// ****************************************************************************
session_start();
//echo '<pre>'; print_r($_SESSION); die;
/**
  * Include header view
  *
  */
$this->load->view('include/header_new', $jsArray='', $cssArray='');

/**
  * Include left side view
  *
  */
$this->load->view('include/left_side');

/**
  * Include page view files that you want to view
  *
  */
$name=($this->session->userdata('full_name')!='') ? ucwords(strtolower($this->session->userdata('full_name'))) : 'User';
$userid = ($this->session->userdata('user_id')>0) ? $this->session->userdata('user_id') : 0;
$user_details=getUserInfo($userid);
$userSubscriptionsDetails=getUserSubsriptionInfo($userid);
$themeSelect=($this->session->userdata('theme')!='') ? $this->session->userdata('theme') : 'default';
$defaultSelect=($themeSelect=='default') ? 'checked="checked"' : '';
$darkSelect=($themeSelect=='dark') ? 'checked="checked"' : '';
//theme settings
echo '<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="settings" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Theme Settings</h4>
                        </div>
                        <div class="modal-body">
                            <input type="radio" name="curr_theme" id="curr_theme" value="default" '.$defaultSelect.'>
                            <label for="curr_theme"><span></span>Default Theme</label>
                            
                            <input type="radio" name="curr_theme" id="dark_theme" value="dark" '.$darkSelect.'>
                            <label for="dark_theme"><span></span>Dark Theme</label>';
                        
                        
                    echo'</div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" id="sbt_theme" onclick="saveThemeOption()">Submit</button>
                        </div>
                </div>
        </div>
</div>';
$show=(isset($_SESSION['payment_status'])) ? 'style="display:none"' : '';
// edit profile
echo '<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit_profile" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <a href="javascript:void(0)" id="myModalEditUser">Edit User</a> | <a href="javascript:void(0)" id="myModalSubscription">Subscription</a>
                        </div>
                        <div id="user_profile_div" '.$show.'>
                            <div class="modal-body">';
                             echo form_open(base_url("user/updateProfile"), array('onsubmit'=>'return chkProfile()', 'id'=>'update_profile')); 
                            //echo form_label('Email: ', 'email_address'); 
                            echo '<label class="control-label">Email: </label>';
                            //echo '<br>';
                            $data_form = array(
                                            'name'        => 'email_address',
                                            'id'          => 'email_address',
                                            'value'       => $user_details['details']['email'],
                                            'onchange'    => 'return chk_useremail()',
                                            'class'  =>  'form-control',
                                          );
                            echo form_input($data_form);
                            echo'<span id="useremail_msg" style="display: none">Available</span>';
                            //echo form_label('New Password:', 'new_pass'); 
                            //echo '<br>';
                            echo '<label class="control-label">New Password: </label>';
                            $data_form = array(
                                            'name'        => 'new_pass',
                                            'id'          => 'new_pass',
                                            'value'       => set_value('new_pass'), 
                                            'class'  =>  'form-control'
                                          );
                            echo form_password($data_form);

                            //echo form_label('Current Password:', 'curr_pass');
                            //echo '<br>';
                            echo '<label class="control-label">Current Password: </label>';
                            $data_form = array(
                                            'name'        => 'curr_pass',
                                            'id'          => 'curr_pass',
                                            'value'       => set_value('curr_pass'), 
                                            'class'  =>  'form-control'
                                          );

                            echo form_password($data_form);

                            //echo form_label('First Name:', 'first_name'); 
                            //echo '<br>';
                            echo '<label class="control-label">New Password: </label>';
                            $data_form = array(
                                            'name'        => 'first_name',
                                            'id'          => 'first_name',
                                            'value'       => $user_details['details']['first_name'], 
                                            'class'  =>  'form-control'
                                          );

                            echo form_input($data_form);
                            //echo '<br>';

                            //echo form_label('Last Name:', 'last_name'); 
                            //echo '<br>';
                            echo '<label class="control-label">Last Name: </label>';
                            $data_form = array(
                                            'name'        => 'last_name',
                                            'id'          => 'last_name',
                                            'value'       => $user_details['details']['last_name'],  
                                            'class'  =>  'form-control'
                                          );

                            echo form_input($data_form);
                            //echo '<br>';

                            //echo form_label('Stage Name:', 'stage'); 
                            //echo '<br>';
                            echo '<label class="control-label">Stage Name: </label>';
                            $data_form = array(
                                            'name'        => 'stage',
                                            'id'          => 'stage',
                                            'value'       => $user_details['details']['stage_name'],
                                            'class'  =>  'form-control'
                                          );

                            echo form_input($data_form);
                            //echo '<br>';

                            //echo form_label('Company Name', 'company');
                            //echo '<br>';
                            echo '<label class="control-label">Company Name: </label>';
                            $data_form = array(
                                            'name'        => 'company',
                                            'id'          => 'company',
                                            'value'       => $user_details['details']['company_name'],    
                                            'class'  =>  'form-control'
                                          );

                            echo form_input($data_form);
                            //echo '<br>';
                            echo form_close();

                        echo'</div>
                            <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                    <button class="btn btn-primary" type="button" onclick="$(\'#update_profile\').submit();" id="adduserbtn">Edit User</button>
                            </div>
                        </div>';
                    $show=(!isset($_SESSION['payment_status'])) ? 'style="display:none"' : '';
                    echo'<div id="subscription_div" '.$show.'>
                              <div class="modal-body">';
                    if(isset($_SESSION['payment_status']) && $_SESSION['payment_status']=='success'){
                        echo '<div>
                                        Subscription Activate Successfully
                                </div>';
                    }
                    else if(isset($_SESSION['payment_status']) && $_SESSION['payment_status']=='cancel'){
                        echo '<div>
                                        Subscription Not Activated, Please try again.
                                </div>';
                    }
                        ?>
                        <!--pricing table-->
                <div class="form-heading">
                        My subscriptions
                </div>
                <!--pricing table-->
                <!--                               row-->
                <div class="row panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                    <thead>
                                            <tr>
                                                    <th>#</th>
                                                    <th>Package Name</th>
                                                    <th>Order Date</th>
                                                    <th>Activation Date</th>
                                                    <th>Expiry Date</th>
                                                    <th>Users</th>
                                                    <th>Traffic</th>
                                            </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $i=1;
                                            foreach ($userSubscriptionsDetails['userSubscriptions'] as $key => $value) {
                                        ?>
                                            <tr>
                                                    <th scope="row"><?=$i; ?></th>
                                                    <td><?=strtoupper($value['name']); ?></td>
                                                    <td><?=date('d-M-Y', strtotime($value['order_date'])); ?></td>
                                                    <td>
                                                        <?php
                                                        if($value['status']=='1'){
                                                            echo date('d-M-Y', strtotime($value['activation_date'])); 
                                                        }
                                                        else{
                                                            echo '<a href="'.base_url('subscription/activate/'.$value['invoice']).'">Activate Now</a>';
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if($value['status']=='1')
                                                            echo date('d-M-Y', strtotime($value['expiry_date'])); 
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php 
                                                            echo 'Total: '. $value['users']; 
                                                            echo '<br />';
                                                            echo 'Remaining: '. $userSubscriptionsDetails['remainingUsers'];
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                            echo 'Total: '.$value['traffic_storage'].' GB';
                                                            echo '<br />';
                                                            echo 'Remaining: '.number_format(($value['traffic_storage']-$userSubscriptionsDetails['trafficSize']), 3);
                                                        ?>
                                                    </td>
                                            </tr>
                                        <?php
                                        $i++;
                                        }
                                        ?>
                                            
                                    </tbody>
                            </table>
                    </div>
                </div>
                
                <div class="form-heading">
                        Subscriptions Packages
                </div>
                <!--pricing table-->
                <!--                               row-->
                <div class="row panel-body">
					<!--                                   col md 4-->
                    <?php
                    foreach ($userSubscriptionsDetails['subscriptions'] as $key => $value) {
                    ?>
                        <div class="col-md-3">
                                <div class="pricing-header">
                                        <?=strtoupper($value['name']); ?>
                                </div>
                                <div class="pricing-price">
                                    <?php
                                    if($value['price']==0){
                                        echo 'Free';
                                    }
                                    else{
                                        echo '$ '.str_pad($value['price'], 3, "0", STR_PAD_LEFT).'<span>/mo</span>';
                                    }
                                    ?>
                                </div>
                                <ul class="list-unstyled pricing-features">
                                        <li><span><?=$value['users']; ?></span> users</li>
                                        <li><span><?=$value['traffic_storage']; ?> GB</span> storage</li>
                                        <li>24/7 customer support</li>
                                        <li> <a class="btn btn-primary btn-lg" href="<?=base_url('subscription/buy/'.$value['id']); ?>">Buy Now</a></li>
                                </ul>
                        </div>
                    <?php
                        //echo '<th>'.strtoupper($value['name']).'</th>';
                    }
                    ?>


                </div>
                <?php
                        echo '</div>
                            <div class="modal-footer">
                                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            </div>
                        </div>
                </div>
        </div>
</div>';

echo '<div class="content" id="content">
	
	<div class="overlay"></div>			
	
	<div class="top-bar">
	<nav class="navbar navbar-default top-bar topheader-custom">
		<div class="menu-bar-mobile" id="open-left"><i class="ti-menu"></i>
		</div>

		<ul class="nav navbar-nav navbar-right top-elements">
			<li class="piluku-dropdown dropdown">
				<!-- @todo Change design here, its bit of odd or not upto usable -->

				<a href="#" class="dropdown-toggle avatar_width" data-toggle="dropdown" role="button" aria-expanded="false"><span class="avatar-holder"><img src="'.base_url('assets/images/avatar/four.png').'" alt=""></span><span class="avatar_info">'.$name.'</span><span class="drop-icon"><!-- <i class="ion ion-chevron-down"></i> --></span></a>
				<ul class="dropdown-menu dropdown-piluku-menu animated fadeInUp wow avatar_drop neat_drop dropdown-right" data-wow-duration="1500ms" role="menu">
					<li>
						<a href="#" data-target="#settings" data-toggle="modal"><i class="ion-android-settings"></i>Settings</a>
					</li>
					
					<li>
						<a href="#" data-target="#edit_profile" data-toggle="modal" id="edit_profile_link"> <i class="ion-android-create"></i>Edit profile</a>
					</li>
					<li>
						<a href="'.base_url('user/logout').'" class="logout_button"><i class="ion-power"></i>Logout</a>
					</li>   
				</ul>
			</li>
			<li class="chat_btn">
				<a href="#" class="right-bar-toggle flatRed">
					<i class="ion-ios-people-outline"></i>                              
				</a>
			</li>
		</ul>

	</nav>

</div>
<!-- /top-bar -->
	
	<div class="main-content">';
	$this->load->view($main_content, $data);
  echo '</div>';
echo '</div>';

//echo '<pre>'; print_r($_SESSION); die;

if(isset($_SESSION['payment_status'])){
    //die('sss');
?>
                <script>
                    $(document).ready(function(){
                        $('#edit_profile_link').click();
                        //$('#myModalSubscription').click();
                    })
                    
                </script>
<?php
unset($_SESSION['payment_status']);
}

/**
  * Include footer view
  *
  */
$this->load->view('include/footer_new');

?>