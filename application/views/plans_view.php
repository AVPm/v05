<?php
// ****************************************************************************
// 
//     User Plans view
//
// ****************************************************************************

?>
<html>

<head>
<meta charset="utf-8">
<title>AVPmatrix</title>
<link rel="icon" type="image/png" href="img/favicon.png" />
<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"  />
<link rel="stylesheet" type="text/css" href="/css/style.css"  />
<link rel="stylesheet" type="text/css" href="/css/image_page.css"  />
<link rel="stylesheet" href="/includes/mindmaps-master/src/css/common.css">
<link rel="stylesheet" href="/includes/mindmaps-master/src/css/app.css">
<link rel="stylesheet" href="/includes/mindmaps-master/src/css/Aristo/jquery-ui-1.8.7.custom.css" />
<link rel="stylesheet" href="/includes/mindmaps-master/src/css/minicolors/jquery.miniColors.css">

<script id="template-float-panel" type="text/x-jquery-tmpl">
<div class="ui-widget ui-dialog ui-corner-all ui-widget-content float-panel no-select">
  <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
    <span class="ui-dialog-title">${title}</span>
    <a class="ui-dialog-titlebar-close ui-corner-all" href="#" role="button">
      <span class="ui-icon"></span>
    </a>
  </div>
  <div class="ui-dialog-content ui-widget-content">
  </div>
</div>
</script>

<script id="template-notification" type="text/x-jquery-tmpl">
<div class="notification">
  {{if closeButton}}
  <a href="#" class="close-button">x</a>
  {{/if}}
  {{if title}}
  <h1 class="title">{{html title}}</h1>
  {{/if}}
  <div class="content">{{html content}}</div>
</div>
</script>

<script id="template-open-table-item" type="text/x-jquery-tmpl">
<tr>
  <td><a class="title" href="#">${title}</a></td>
  <td>${$item.format(dates.modified)}</td>
  <td><a class="delete" href="#">delete</a></td>
</tr>
</script>

<script id="template-open" type="text/x-jquery-tmpl">
<div id="open-dialog" class="file-dialog" title="Open mind map">
  <h1><span class="highlight">New!</span> From the Cloud: Dropbox and more</h1>
  <p>Open, save and share your mind maps online in your favorite cloud storage. Supports Google Drive, Dropbox and more! Powered by <a href="http://www.filepicker.io" target="_blank">filepicker.io</a>.</p>
  <button id="button-open-cloud">Open</button>
  <span class="cloud-loading">Loading...</span>
  <span class="cloud-error error"></span>
  <div class="seperator"></div>
  <h1>Local Storage</h1>
  <p>This is a list of all mind maps that are saved in your browser's local storage. Click on the title of a map to open it.</p>
  <table class="localstorage-filelist">
    <thead>
      <tr>
        <th class="title">Title</th>
        <th class="modified">Last Modified</th>
        <th class="delete"></th>
      </tr>
    </thead>
    <tbody class="document-list"></tbody>
  </table>
  <div class="seperator"></div>
  <h1>From file</h1>
  <p>Choose a mind map from your computer's hard drive.</p>
  <div class="file-chooser">
    <input type="file" />
  </div>
</div>
</script>

<script id="template-save" type="text/x-jquery-tmpl">
<div id="save-dialog" class="file-dialog" title="Save mind map">
  <h1><span class="highlight">New!</span> In the Cloud: Dropbox and more</h1>
  <p>Open, save and share your mind maps online in your favorite cloud storage. Supports Google Drive, Dropbox and more! Powered by <a href="http://www.filepicker.io" target="_blank">filepicker.io</a>.</p>
  <button id="button-save-cloudstorage">Save</button>
  <span class="cloud-error error"></span>
  <div class="seperator"></div>
  <h1>Local Storage</h1>
  <p>You can save your mind map in your browser's local storage. Be aware that this is still experimental: the space is limited and there is no guarantee that the browser will keep this document forever. Useful for frequent backups in combination with cloud storage.</p>
  <button id="button-save-localstorage">Save</button>
  <input type="checkbox" class="autosave" id="checkbox-autosave-localstorage">
  <label for="checkbox-autosave-localstorage">Save automatically every minute.</label>
  <div class="seperator"></div>
  <h1>To file</h1>
  <p>Save the mind map as a file on your computer.</p>
  <div id="button-save-hdd">Save</div>
</div>
</script>

<script id="template-navigator" type="text/x-jquery-tmpl">
<div id="navigator">
  <div class="active">
    <div id="navi-content">
      <div id="navi-canvas-wrapper">
        <canvas id="navi-canvas"></canvas>
        <div id="navi-canvas-overlay"></div>
      </div>
      <div id="navi-controls">
        <span id="navi-zoom-level"></span>
        <div class="button-zoom" id="button-navi-zoom-out"></div>
        <div id="navi-slider"></div>
        <div class="button-zoom" id="button-navi-zoom-in"></div>
      </div>
    </div>
  </div>
  <div class="inactive">
  </div>
</div>
</script>


<script id="template-inspector" type="text/x-jquery-tmpl">
<div id="inspector">
  <div id="inspector-content">
    <table id="inspector-table">
      <tr>
        <td>Font size:</td>
        <td><div
            class="buttonset buttons-very-small buttons-less-padding">
            <button id="inspector-button-font-size-decrease">A-</button>
            <button id="inspector-button-font-size-increase">A+</button>
          </div></td>
      </tr>
      <tr>
        <td>Font style:</td>
        <td><div
            class="font-styles buttonset buttons-very-small buttons-less-padding">
            <input type="checkbox" id="inspector-checkbox-font-bold" /> 
            <label
            for="inspector-checkbox-font-bold" id="inspector-label-font-bold">B</label>
              
            <input type="checkbox" id="inspector-checkbox-font-italic" /> 
            <label
            for="inspector-checkbox-font-italic" id="inspector-label-font-italic">I</label> 
            
            <input
            type="checkbox" id="inspector-checkbox-font-underline" /> 
            <label
            for="inspector-checkbox-font-underline" id="inspector-label-font-underline">U</label> 
            
            <input
            type="checkbox" id="inspector-checkbox-font-linethrough" />
             <label
            for="inspector-checkbox-font-linethrough" id="inspector-label-font-linethrough">S</label>
          </div>
        </td>
      </tr>
      <tr>
        <td>Font color:</td>
        <td><input type="hidden" id="inspector-font-color-picker"
          class="colorpicker" /></td>
      </tr>
      <tr>
        <td>Branch color:</td>
        <td><input type="hidden" id="inspector-branch-color-picker"
          class="colorpicker" />
          <button id="inspector-button-branch-color-children" title="Apply branch color to all children" class="right buttons-small buttons-less-padding">Inherit</button>
        </td>
      </tr>
    </table>
  </div>
</div>
</script>

<script id="template-export-map" type="text/x-jquery-tmpl">
<div id="export-map-dialog" title="Export mind map">
  <h2 class='image-description'>To download the map right-click the
    image and select "Save Image As"</h2>
  <div id="export-preview"></div>
</div>
</script>

</head>
<body>
    <div id="top_header">	
        <div>
            <a href="<?=base_url(); ?>"><img src="<?=base_url(); ?>images/transparent.gif" style="height:33px; width:100px;"></a>
            <div class="topnav">
<!--                    <a class="on" href="http://v04.avpmatrix.com/" onfocus="this.blur();">Dashboard</a>-->
                <?php $classDash=(!isset($selected_menu)) ? 'on' : ''; ?>
                <a onfocus="this.blur();" href="<?=base_url(); ?>" class="<?=$classDash; ?>">Dashboard</a>
                <?php
                    if(isset($top_menu)){
                        foreach ($top_menu as $link=>$menu) {
                            $class=(isset($selected_menu) && $selected_menu==$menu) ? 'on' : '';
                            echo '<a onfocus="this.blur();" href="'.base_url($link).'" class="'.$class.'">
                            '.$menu.'</a>';
                        }
                    }
                ?>                  
            </div>
        </div>

        <div class="topform">
          <form action="" method="post">
            <img width="110" height="24" src="<?=base_url(); ?>images/project_id.gif" style="margin:2px 0 0 0; float:left;"><input type="text" class="top_id_input" size="18" name="identifier_project" value="">
          </form>
        </div>
    </div>
  <div id="print-area">
    <p class="print-placeholder">Please use the print option from the
      mind map menu</p>
  </div>
  <!-- DEBUG -->
  <div id="debug-warning"></div>
  <!-- /DEBUG -->
  <div id="container">
    <div id="topbar">
      <div id="toolbar">
        <div id="logo" class="logo-bg">
          <span></span>
        </div>

        <div class="buttons">
          <span class="buttons-left"> </span> <span class="buttons-right">
          </span>
        </div>

      </div>
    </div>
    <div id="canvas-container">
      <div id="drawing-area" class="no-select"></div>
    </div>
    <div id="bottombar">
      <div id="about">
        <a href="about.html" target="_blank">About mindmaps</a> <span
          style="padding: 0 4px;">|</span> <a style="font-weight: bold" href="https://spreadsheets.google.com/a/drichard.org/spreadsheet/viewform?formkey=dEE3VzFWOFp6ZV9odEhhczVBUUdzc2c6MQ"
          target="_blank">Feedback</a>
      </div>
      <div id="statusbar">
        <div
          class="buttons buttons-right buttons-small buttons-less-padding"></div>
      </div>
    </div>
  </div>

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
  <script src="//api.filepicker.io/v0/filepicker.js"></script>

  <!-- DEBUG -->
  <!-- set debug flag for all scripts. Will be removed in production -->
  <script type="text/javascript">
    var mindmaps = mindmaps || {};
    mindmaps.DEBUG = true;
  </script>
  <!-- /DEBUG -->

  <!-- JS:LIB:BEGIN -->
  <script src="/includes/mindmaps-master/src/js/libs/jquery-ui-1.8.11.custom.min.js"></script>
  <script src="/includes/mindmaps-master/src/js/libs/dragscrollable.js"></script>
  <script src="/includes/mindmaps-master/src/js/libs/jquery.hotkeys.js"></script>
  <script src="/includes/mindmaps-master/src/js/libs/jquery.mousewheel.js"></script>
  <script src="/includes/mindmaps-master/src/js/libs/jquery.minicolors.js"></script>
  <script src="/includes/mindmaps-master/src/js/libs/jquery.tmpl.js"></script>
  <script src="/includes/mindmaps-master/src/js/libs/swfobject.js"></script>
  <script src="/includes/mindmaps-master/src/js/libs/downloadify.min.js"></script>
  <script src="/includes/mindmaps-master/src/js/libs/events.js"></script>

  <script src="/includes/mindmaps-master/src/js/MindMaps.js"></script>
  <script src="/includes/mindmaps-master/src/js/Command.js"></script>
  <script src="/includes/mindmaps-master/src/js/CommandRegistry.js"></script>
  <script src="/includes/mindmaps-master/src/js/Action.js"></script>
  <script src="/includes/mindmaps-master/src/js/Util.js"></script>
  <script src="/includes/mindmaps-master/src/js/Point.js"></script>
  <script src="/includes/mindmaps-master/src/js/Document.js"></script>
  <script src="/includes/mindmaps-master/src/js/MindMap.js"></script>
  <script src="/includes/mindmaps-master/src/js/Node.js"></script>
  <script src="/includes/mindmaps-master/src/js/NodeMap.js"></script>
  <script src="/includes/mindmaps-master/src/js/UndoManager.js"></script>
  <script src="/includes/mindmaps-master/src/js/UndoController.js"></script>
  <script src="/includes/mindmaps-master/src/js/ClipboardController.js"></script>
  <script src="/includes/mindmaps-master/src/js/ZoomController.js"></script>
  <script src="/includes/mindmaps-master/src/js/ShortcutController.js"></script>
  <script src="/includes/mindmaps-master/src/js/HelpController.js"></script>
  <script src="/includes/mindmaps-master/src/js/FloatPanel.js"></script>
  <script src="/includes/mindmaps-master/src/js/Navigator.js"></script>
  <script src="/includes/mindmaps-master/src/js/Inspector.js"></script>
  <script src="/includes/mindmaps-master/src/js/ToolBar.js"></script>
  <script src="/includes/mindmaps-master/src/js/StatusBar.js"></script>
  <script src="/includes/mindmaps-master/src/js/CanvasDrawingTools.js"></script>
  <script src="/includes/mindmaps-master/src/js/CanvasView.js"></script>
  <script src="/includes/mindmaps-master/src/js/CanvasPresenter.js"></script>
  <script src="/includes/mindmaps-master/src/js/ApplicationController.js"></script>
  <script src="/includes/mindmaps-master/src/js/MindMapModel.js"></script>
  <script src="/includes/mindmaps-master/src/js/NewDocument.js"></script>
  <script src="/includes/mindmaps-master/src/js/OpenDocument.js"></script>
  <script src="/includes/mindmaps-master/src/js/SaveDocument.js"></script>
  <script src="/includes/mindmaps-master/src/js/MainViewController.js"></script>
  <script src="/includes/mindmaps-master/src/js/Storage.js"></script>
  <script src="/includes/mindmaps-master/src/js/Event.js"></script>
  <script src="/includes/mindmaps-master/src/js/Notification.js"></script>
  <script src="/includes/mindmaps-master/src/js/StaticCanvas.js"></script>
  <script src="/includes/mindmaps-master/src/js/PrintController.js"></script>
  <script src="/includes/mindmaps-master/src/js/ExportMap.js"></script>
  <script src="/includes/mindmaps-master/src/js/AutoSaveController.js"></script>
  <script src="/includes/mindmaps-master/src/js/FilePicker.js"></script>
  <!-- JS:LIB:END -->

  <!-- PRODUCTION
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push([ '_setAccount', 'UA-23624804-1' ]);
  _gaq.push([ '_trackPageview' ]);
  (function() {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl'
        : 'http://www')
        + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
  })();
</script>
/PRODUCTION -->
</body>
</html>

