<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" dir="ltr" lang="en">
<head>
<title><?php echo $title; ?></title>
<script>
    var base_url='<?=base_url(); ?>';  
</script>
<script src="<?php echo base_url();?>js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/common.js"></script>
<?php
if(isset($jsArray)):
    foreach ($jsArray as $js) {
        echo '<script type="text/javascript" src="'.base_url().'js/'.$js.'.js"></script>';
        echo "\n";
    }
endif;
?>

<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css"  />
<?php
if(isset($cssArray)):
    foreach ($cssArray as $css) {
        echo '<link rel="stylesheet" type="text/css" href="'.base_url().'css/'.$css.'.css" />';
        echo "\n";
    }
endif;
?>
</head>
<body>
      <div class="header">
         	<div class="login">
                    <?php
                        $attributes = array('id' => 'loginform');
                        echo form_open("user/login", $attributes); 
                    ?>		
         		
         		<div>Email address:</div>
                        <?php
                            $data = array(
                                        'name'        => 'email',
                                        'id'          => 'email',
                                        'size'        => '22',
                                      );

                            echo form_input($data);
                        ?>
         		<div>Password:</div>
                        <?php
                            $data = array(
                                    'name'        => 'pass',
                                    'id'          => 'pass',
                                    'size'        => '22',
                                  );

                            echo form_password($data);
                        ?>
                    </form>	
                    <div class="login_signup_div">
                        <div class="signup_button">
                            <a href="#" class="button" onclick="return openDiv('formdiv');">Sign up</a>
                        </div>
                        <div id="ordiv">OR</div>
                        <div class="signin_button"><a href="#" class="button" onclick="$('#loginform').submit();">Sign in</a>
                        </div> 
                    </div>
           </div>
         </div>
        
    <div id="distance_intro"></div>  
      <div id="containerDiv">
      <?php //echo @$activate_msg; ?>
      
         <div class="center">
            <span class="heading_col" id="introheading">Organize Files</span><br>
            <img src="<?=base_url(); ?>images/intro_org-files.jpg" alt="" id="introimage" />
         </div>
         
                   <div class="left_container">
                       <div id="intro1">
                            <span class="heading_white">Todd Shepherd</span><br>
                            <span class="heading_gray">CTO</span><br>
                            <span class="heading_col">Studeo</span><br><br>          
                            <img alt="" src="<?=base_url(); ?>images/partner_pic3.jpg" width="100" height="100"><br />
                            <div class="intro_left_text">
                                "These online tools have allowed us a quick and effecient way to impress our clients."
                            </div>
                        </div>

                         <div id="intro2" style="display: none">
                            <span class="heading_white">Richard Kerris</span><br>
                            <span class="heading_gray">CTO</span><br>
                            <span class="heading_col">Lucas Film</span><br><br>          
                            <img alt="" src="<?=base_url(); ?>images/logo_lucas-film.jpg" width="100" height="100"><br />
                            <div class="intro_left_text">
                                "This is how online production should look and work. It was about time!"
                            </div>
                        </div>

                         <div id="intro3" style="display: none">
                            <span class="heading_white">Matt Cupal</span><br>
                            <span class="heading_gray">President</span><br>
                            <span class="heading_col">Sorenson Media</span><br><br>          
                            <img alt="" src="<?=base_url(); ?>images/logo_sorenson-media.jpg" width="100" height="100"><br />
                            <div class="intro_left_text">
                                "A perfect example of how our video technologies are put to use in an innovative manner"
                            </div>
                        </div>

                         <div id="intro4" style="display: none">
                            <span class="heading_white">Todd Rundgren</span><br>
                            <span class="heading_gray">Musician | Producer</span><br>
                            <span class="heading_col">Panacea</span><br><br>          
                            <img alt="" src="<?=base_url(); ?>images/logo_todd-rundgren.jpg" width="100" height="100"><br />
                            <div class="intro_left_text">
                                "Fancy shmancy online stuff. But ya know what? This stuff just works and looks cool"
                            </div>
                        </div>

                         <div id="intro5" style="display: none">
                            <span class="heading_white">Jefferson Coombs</span><br>
                            <span class="heading_gray">Senior VP</span><br>
                            <span class="heading_col">Campbell & Ewald</span><br><br>          
                            <img alt="" src="<?=base_url(); ?>images/logo_campbell-ewald.jpg" width="100" height="100"><br />
                            <div class="intro_left_text">
                                "As an Ad Agency we constantly have to show what we do and look good. This helps us do it."
                            </div>
                        </div>

   			
                    </div>         
                    <div class="right_container">
                            <a class="home_thumb_img" id="img_1"><span>Manage Users</span><br /><img alt="" src="<?=base_url(); ?>images/intro_man-users_thumb.jpg"></a>
                            <a class="home_thumb_img" id="img_2"><span>Organize Images</span><br /><img alt="" src="<?=base_url(); ?>images/intro_org-images_thumb.jpg"></a>
                            <a class="home_thumb_img" id="img_3"><span>Stream Video</span><br /><img alt="" src="<?=base_url(); ?>images/intro_stream-video_thumb.jpg"></a>
                            <a class="home_thumb_img" id="img_4"><span>Manage Account</span><br /><img alt="" src="<?=base_url(); ?>images/intro_man-account_thumb.jpg"></a>
                            <a class="link" href="#">&nbsp;</a>
                            <br class="clear"><br>
                            <h1>Welcome</h1>
                            <div class="largetext"><span class="heading_col">AVP</span>matrix is an online application for organizing and sharing Files, Images and Videos.<br><br>Whether
    you want to manage an entire production and present your work to a 
    client, or whether you simply want to have a web-based portfolio of your
    data, <span class="heading_col">AVP</span>matrix is all you need.</div>
                    </div>
		    
		    
		    
      </div>      

      
      
       
      
      <div class="bottom_div">
          <table class="bottom_table">
                <tbody>
                    <tr>
                        <th class="tableheading">Plan:</th>
                        <?php
                        foreach ($subscriptions as $key => $value) {
                            echo '<th>'.strtoupper($value['name']).'</th>';
                        }
                        ?>
                        
                    </tr>
                    <tr>
                            <td class="tableheading">Traffic:</td>
                            <?php
                            foreach ($subscriptions as $key => $value) {
                                echo '<td>'.$value['traffic_storage'].' GB</td>';
                            }
                            ?>
                    </tr>
                    <tr>
                            <td class="tableheading">Users:</td>
                            <?php
                            foreach ($subscriptions as $key => $value) {
                                echo '<td>'.$value['users'].'</td>';
                            }
                            ?>				
                    </tr>
                    <tr>
                            <td class="tableheading">Monthly:</td>
                           
                            <?php
                            foreach ($subscriptions as $key => $value) {
                                if($value['price']==0){
                                    echo '<td><span class="heading_col">Free</span></td>';
                                }
                                else{
                                    echo '<td>$ '.str_pad($value['price'], 3, "0", STR_PAD_LEFT).'</td>';
                                }
                                
                            }
                            ?>
                    </tr>
                    <tr>
                            <td class="tableheading">&nbsp;</td>
                            <?php
                            foreach ($subscriptions as $key => $value) {
                                echo '<td><a href="javascript:void(0)" onfocus="this.blur();" onclick="buySubscription('.$value['id'].');">Sign Up Now</a></td>';
                            }
                           
                            /*
                            <td><a href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('formdiv');">Sign Up Now</a></td>
                            <td>TBA</td>
                            <td>TBA</td>
                            <td>TBA</td>
                            <td>TBA</td>	
                             * 
                             * 
                             */		
                            ?>
                    </tr>
            </tbody>
          </table>
      </div>
    
    <!-- signup form overlay -->
    <div class="popup" id="formdiv" style="width: 640px;">
	<div class="popup_head">
		<span>Sign up</span>	
	</div>
	<div class="popup_top">
		<table cellspacing="0" cellpadding="0" border="0">
		<tbody><tr>
		<td width="38%" class="leftcolumn"><span class="heading_gray">Plan:</span>
		</td>
                        <td width="62%" class="rightcolumn" id="pack_plan">TRIAL
		</td>
		</tr>
		<tr>
		<td width="38%" class="leftcolumn"><span class="heading_gray">Traffic:</span>
		</td>
                    <td width="62%" class="rightcolumn" id="pack_traffic">1000 GB
		</td>
		</tr>
		<tr>
		<td width="38%" class="leftcolumn"><span class="heading_gray">Users:</span>
		</td>
                    <td width="62%" class="rightcolumn" id="pack_users">100
		</td>
		</tr>
		<tr>
                    <td width="38%" class="leftcolumn"><span class="heading_gray" id="pack_validity">30 Day Trial:</span>
		</td>
                    <td width="62%" class="rightcolumn" ><span class="heading_col" id="pack_amount">Free</span>
		</td>
		</tr>
		</tbody></table>
		<div class="run">Fill out the form below to sign up for a trial account.</div>
		<div class="clear"></div> 
	</div>
	<div class="popup_forms">
        <?php 
            echo validation_errors('<p class="error">');
            $data = array('onsubmit' => "return chkSignup()", 'id'=>'signup_form');
            echo form_open(base_url("user/registration"), $data); ?>
            <div class="floatleft">
                <span>First Name:</span>
                <?php
                //echo form_label('First Name:', 'first_name'); 
                echo '<br>';
                $data = array(
                                'name'        => 'first_name',
                                'id'          => 'first_name',
                                'value'       => set_value('first_name'),
                                'class'       => 'smallinput',
                              );

                echo form_input($data);
                
                //subscription pack id hidden value
                $data = array(
                    'name'        => 'subscriptionId',
                    'id'          => 'subscriptionId',
                    'value'       => '1',
                    'type'        => 'hidden',
                  );

                echo form_input($data);
                ?>
                
            </div>
            
            <div class="floatleft"> 
                <span>Last Name:</span>
            <?php
            //echo form_label('Last Name:', 'last_name'); 
            echo '<br>';
            $data = array(
                            'name'        => 'last_name',
                            'id'          => 'last_name',
                            'value'       => set_value('last_name'),  
                            'class'       => 'smallinput',
                          );

            echo form_input($data);
            ?>
            </div>			
            <div class="clear">
                <span>Company Name:</span>
                <?php
                    //echo form_label('Company Name:', 'company_name'); 
                    echo '<br>';
                    $data = array(
                                    'name'        => 'company',
                                    'id'          => 'company',
                                    'value'       => set_value('company'),                                        
                                  );

                    echo form_input($data);
                    echo '<br>';
                ?>
            </div>
        <br>
        <br>
        <span>eMail Address:</span>&nbsp;&nbsp;<span class="hg">(used as login name)</span><br />
        <?php
        $data = array(
                      'name'        => 'email_address',
                      'id'          => 'email_address',
                      'value'       => set_value('email_address'),   
                      'onchange'    => 'return chk_useremail()',
                    );

        echo form_input($data);
        ?>
        &nbsp; <span id="useremail_msg" style="display: none">Avaliable</span>
          <br>	
          <span>Password:</span>
          <?php
              //echo form_label('Password:', 'password'); 
              echo '<br>';
              $data = array(
                              'name'        => 'password',
                              'id'          => 'password',
                              'value'       => set_value('password'),                                        
                            );

              echo form_password($data);
              echo '<br>';
              
              echo '<span>Password:</span>';
              //echo form_label('Password:', 'con_password'); 
              echo '&nbsp;&nbsp;<span class="heading_gray">(Re-Enter)</span><br>';
              $data = array(
                              'name'        => 'con_password',
                              'id'          => 'con_password',
                              'value'       => set_value('con_password'),                                        
                            );

              echo form_password($data);
              echo '<br>';
              
              echo '<span>Stage Name:</span>';

              //echo form_label('Stage Name:', 'stage'); 
              echo '<br>';
              $data = array(
                              'name'        => 'stage',
                              'id'          => 'stage',
                              'value'       => set_value('stage'),                                        
                            );

              echo form_input($data);

              echo form_close(); ?>
            </div>
            <div class="popup_bottom">
                    <a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('formdiv')">Cancel</a>
                    <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="$('#signup_form').submit();">Submit</a>
                    <div class="clear">&nbsp;</div> 
            </div>
    </div>
    
    <?php
    if($this->session->userdata('user_registration')=='success'){
    ?>
    <!-- Registration success overlay -->
    <div id="registration_success" class="popup" style="width:640px; display: block">
        <div class="popup_head">    
                <span>Info</span>
        </div>
        <div class="popup_top">
                <img src="<?=base_url(); ?>images/popup_pic1.jpg" alt="">
                <div class="run">Account successfully created. An activation email has been sent to your email. Please Check your email and active your account.</div>
                <div class="clear">&nbsp;</div> 
        </div>
        <div class="popup_forms">
        </div>
        <div class="popup_bottom">		
            <a href="javascript:void(0)" onfocus="this.blur();" class="but_green floatright" onclick="closeDiv('registration_success')">ok</a>
                <div class="clear">&nbsp;</div> 
        </div> 
    </div>
    <script>
        positionPopup('registration_success');
    </script>
    <?php
    $this->session->unset_userdata('user_registration');
    }
    ?>
    
    
    <?php
    if($this->session->flashdata('success_msg')=='Your profile is activated. You can login now'){
    ?>
    <!-- account activation overlay -->
    <div id="registration_success" class="popup" style="width:640px; display: block">
        <div class="popup_head">    
                <span>Info</span>
        </div>
        <div class="popup_top">
                <img src="<?=base_url(); ?>images/popup_pic1.jpg" alt="" />
                <div class="run">Your account is successfully activated, Please login to continue.</div>
                <div class="clear">&nbsp;</div> 
        </div>
        <div class="popup_forms">
        </div>
        <div class="popup_bottom">		
            <a href="javascript:void(0)" onfocus="this.blur();" class="but_green floatright" onclick="closeDiv('registration_success')">ok</a>
                <div class="clear">&nbsp;</div> 
        </div> 
    </div>
    <script>
        positionPopup('registration_success');
    </script>
    <?php
    }
    ?>
    <img id="loader_img" src="<?=base_url(); ?>images/ajax-loader.gif" style="display: none; z-index: 15" />
    <div id="cover"> </div>
</body>
</html>