<?php
// ****************************************************************************
// 
//     Welcome view after user login
//
// ****************************************************************************

?>
<script>
    function resizeuserlist(){
        var wh = $(window).height();
        //wh;
        var topmenuh=$('.top-bar').height();
        //topmenuh;
        var headingDivh=$('#usersList .panel-heading').height();

        var filter=$('#projectList').height();

        var h=wh-topmenuh-headingDivh-filter;
        h;
        //filter;
        //h;
        $('#usersList .panel-body').css('height', h+'px');
        $('#usersList #users_listing').css('height', h+'px');
    }
    
    $(document).ready(function(){
        resizeuserlist();
        //showSearchResult();
        $("#users_listing").niceScroll();
        $("#comments_listing").niceScroll();
    });
    $(window).resize(function() {
        $("#users_listing").niceScroll();
        $("#comments_listing").niceScroll();
        resizeuserlist();
    });
</script>

<script type="text/javascript" src="<?=base_url(); ?>includes/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    window.onload = function()
    {
        CKEDITOR.replace( 'comment', {
                removePlugins: 'bidi,div,font,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley/maximize',
                //removeButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',
                format_tags: 'p;h1;h2;h3;pre;address',
                height: 250

        } );

        CKEDITOR.replace( 'comment_edit', {
                removePlugins: 'bidi,div,font,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley/maximize',
                //removeButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',
                format_tags: 'p;h1;h2;h3;pre;address',
                height: 250

        } );

        CKEDITOR.on('dialogDefinition', function( ev ){
            var dialogName = ev.data.name;
            var dialogDefinition = ev.data.definition;
            if ( dialogName == 'link' ){
                // Get a reference to the "Target" tab.
                        var targetTab = dialogDefinition.getContents( 'target' ); 
                        // Set the default value for the target field.
                        var targetField = targetTab.get( 'linkTargetType' );
                        targetField['default'] = '_blank';
                 }
        });
    }

    //////////end of validations////////////////
</script>
<?php
/*
 * load user's project list on top in horizontal scroll div
 */

//$this->view('project_list_view_new', $data);

//store all userid from $project_users arry into a new array named $useridArr
$useridArr=  array();
foreach ($project_users as $value) {
    array_push($useridArr, $value['user_id']);
}
?>
    <?php
    /*
     * load user's project list on top in horizontal scroll div
     */
    $data['select_projectId']=$projectId;
    $this->view('project_list_view_new');
    if($this->session->userdata('user_id')!='' && !strstr($_SERVER['REQUEST_URI'], 'complete_registration')){
        $this->view('search_side_view_new');
    }
    ?>

<!-- Manage project users div start -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addUserProject" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Manage Project Users</h4>
                        </div>
                        <div class="modal-body">
                                <div class="leftholder" id="leftholder">
                                    <div style="background:#666;position:relative;" id="myusers">My Users</div> 
                                        <?php
                                        /*
                                         * user listing of login user
                                         */
                                        foreach ($user_listing as $key) {
                                            $bgcolor=(in_array($key['child_userid'], $useridArr)) ? 'style="background:#666666;"' : '';
                                        ?>
                                        <div id="myuser_nameDiv_<?=$key['child_userid'] ?>" class="nameholder" <?=$bgcolor; ?>><?=$key['stage_name']; ?> (<?=$key['email']; ?>)</div>

                                        <div id="myuser_linkDiv_<?=$key['child_userid'] ?>" class="linkholder" <?=$bgcolor; ?>>
                                          <?php 
                                              if(!in_array($key['child_userid'], $useridArr)){ ?>
                                              <a href="javascript:void(0)" onfocus="this.blur();" class="ajaxlinks" onclick="adduserpoject('<?=$key['child_userid'] ?>')">add user &gt;</a>
                                          <?php
                                              }
                                              else{
                                                  echo '&nbsp';
                                              }
                                          ?>
                                        </div>
                                        <?php
                                        }
                                        ?>

                                      </div>   

                                  <div class="rightholder" id="rightholder">
                                    <div style="background:#666; float:clear;" id="project_users">Project Users</div>
                                      <?php
                                      /*
                                       * user listing of that users whose are added in project
                                       */
                                      foreach ($project_users as $key) {
                                      ?>
                                      <div class="nameholder" id="projectuser_nameDiv_<?=$key['user_id'] ?>"><?=$key['stage_name']; ?> (<?=$key['email']; ?>)</div>
                                      <div class="linkholder" id="projectuser_linkDiv_<?=$key['user_id'] ?>"><a href="javascript:void(0)" onfocus="this.blur();" class="ajaxlinks" onclick="removeuserpoject(<?=$key['user_id'] ?>)">remove user &gt;</a></div>
                                      <?php
                                      }
                                      ?>

                                  <?php
                                  /*
                                   * hidden field with value of project id
                                   */
                                      $data = array(
                                      'name'        => 'projectid_hidden',
                                      'id'          => 'projectid_hidden',
                                      'value'       => $projectId,
                                      'type'        => 'hidden',
                                      );

                                      echo form_input($data);
                                  ?>
                                </div>
                        </div>
                    <div class="clear"></div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="location.reload();" >Add User</button>
                        </div>
                </div>
        </div>
</div>

<!-- Manage project users div end -->
<?php
    if($userLimit=='yes'):
?> 
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addUserDiv" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Add User</h4>
                        </div>
                        <div class="modal-body">
                                <div id="adduserHtml">
                                    <div class="popup_top">
                                            <img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg" style="float: left">
                                            <div class="run" style="float: left; margin-left: 5px; margin-top: 30px;">Enter the email address of the user you want to add.</div>
                                            
                                    </div>
                                    <div class="clear">&nbsp;</div> 
                                    <div class="popup_forms">
                                         <?php
                                            echo form_label('Email:', 'email'); 
                                            echo '<br>';
                                            $data = array(
                                                            'name'        => 'addUser_email',
                                                            'id'          => 'addUser_email',
                                                            'class'       => 'form-control',
                                                            'placeholder' => 'Email'
                                                          );

                                            echo form_input($data);
                                            echo '<br>';
                                            ?>


                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="checkAddUser()" id="adduserbtn">Add User</button>
                        </div>
                </div>
        </div>
</div>

<?php
    endif;
?>

<!-- add comment form start -->
<?php if($userid>0): ?>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addBlogComment" class="modal fade modal-full-pad in">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Add Comment</h4>
                        </div>
                        <div class="modal-body">
                                <div id="containmentbox" class="sort_area">
                                    <?php
                                    echo form_open("", array('id'=>'addBlogcommentfrm')); 

                                    $data = array(
                                                'name'        => 'projectid_blog_hidden',
                                                'id'          => 'projectid_blog_hidden',
                                                //'value'       => $curr_albumid,
                                                'value'       => $projectId,
                                                'type'        => 'hidden',
                                                );

                                    echo form_input($data);
                                    /*            
                                    echo form_label('Stagename: ', 'stagename'); 
                                    echo '<br>';
                                    $data = array(
                                                    'name'        => 'stagename',
                                                    'id'          => 'stagename',
                                                    'value'       => $user_stagename,
                                                    'readonly'    => 'readonly'
                                                  );
                                    echo form_input($data);

                                    echo '<br>';
                                     * 
                                     */

                                   // echo form_label('Comment: ', 'comment'); 
                                    $data = array(
                                            'name'        => 'comment',
                                            'id'          => 'comment',
                                            'rows'        => '7',
                                            'cols'        => '36',
                                          );

                                    echo form_textarea($data);

                                    echo form_close(); 
                                    ?>
                                   </div>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#addBlogcommentfrm').submit();">Save Comment</button>
                        </div>
                </div>
        </div>
</div>

<?php endif; ?>
<!-- add comment form ends -->


<!-- edit comment form start -->
<?php if($userid>0): ?>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="editBlogCommentDiv" class="modal fade modal-full-pad in">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Edit Comment</h4>
                                <a style="position: absolute; top: 12px; right: 45px;" class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removeblogcomment('<?=$last_commentid; ?>', '<?=$projectId; ?>')">Remove Comment</a>
                        </div>
                        <div class="modal-body">
                                <div id="containmentbox" class="sort_area">
                                    <?php
                                    echo form_open("", array('id'=>'addBlogcommentfrm')); 

                                    $data = array(
                                                'name'        => 'commentid_hidden',
                                                'id'          => 'commentid_hidden',
                                                'value'       => '',
                                                'type'        => 'hidden',
                                                );

                                    echo form_input($data);
                                    
                                    $data = array(
                                            'name'        => 'comment_edit',
                                            'id'          => 'comment_edit',
                                            'rows'        => '7',
                                            'cols'        => '36',
                                          );

                                    echo form_textarea($data);

                                    echo form_close(); 
                                    ?>
                                   </div>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#editblogcommentfrm').submit();">Save Comment</button>
                        </div>
                </div>
        </div>
</div>

<?php endif; ?>
<!-- edit comment form ends -->

<div id="roleChange_msg" style="display: none"></div>
<div style="" id="user_list" class="col-md-9 nopad-right pull-left">
        <!-- panel -->
        <div class="panel panel-piluku">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Users
                    <a id="showImgAdd" data-target="#addUserProject" data-toggle="modal" onfocus="this.blur();" href="javascript:void(0)" style="margin-left: 15px;">Add</a>
                </h3>
          </div>
        <div style="height: 230px;" class="panel-body">
            <div class="row main-chart-parent">
               
                            <div id="users_listing" style="height: 220px;">
                                    <table cellpadding="0" cellspacing="0" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Stage Name</th>
                                                <th>Real Name</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <!--<th>Action</th>-->
                                            </tr>
                                            
                                        </thead>
                                        
                                        <?php
                                           $options = array('1'=>'None', '2'=>'Viewer', '3'=>'Contributor', '4'=>'Co-Owner');
                                           $i=1;
                                           foreach ($project_users as $key) {
                                        ?>
                                                <tr>
                                                    <td><?=$i; ?></td>
                                                    <td class="whiteText"><?=$key['stage_name']; ?></td>
                                                    <td><span class="whiteText"><?=ucwords($key['last_name']); ?></span>, <span class="firstname_color"><?=ucwords($key['first_name']); ?></span></td>
                                                    <td class="emailcolor"><?=$key['email']; ?></td>
                                                    <td>
                                                    <?php 
                                                        $id = 'id="role_'.$key['user_id'].'" onchange="changeProjectUserRole(this.value, '.$key['id'].')" class="form-control"';
                                                        echo '<div class="styled-select">';
                                                        echo form_dropdown('role', $options, $key['role'], $id);
                                                        echo '</div>';
                                                    ?>
                                                    </td>
                                                    <?php /*
                                                    <td><a href="javascript:void(0)" onclick="removeUser(<?=$key['child_userid']; ?>)">Remove</a></td>
                                                     * 
                                                     */ ?>
                                                </tr>
                                        <?php
                                            $i++;
                                           }
                                        ?>

                                                
                                    </table>
                                 </div>
                
                            </div>
            <!-- /row -->
        </div>
    </div>
    <!-- /panel -->
</div>

<div style="" id="blog_list" class="col-md-9 nopad-right pull-left">
        <!-- panel -->
        <div class="panel panel-piluku">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Blog
                    <a id="showImgAdd" data-target="#addBlogComment" data-toggle="modal" onfocus="this.blur();" href="javascript:void(0)" style="margin-left: 15px;">Add</a>
                </h3>
          </div>
        <div style="height: 230px;" class="panel-body">
            <div class="row main-chart-parent">
               
                                <div id="comments_listing" style="height: 220px;">
                                    <table cellpadding="0" cellspacing="0" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Stage Name</th>
                                                <th>Message</th>
                                                <th>Date Added</th>
                                                <th>Action</th>
                                            </tr>
                                            
                                        </thead>
                                        
                                        <?php
                                        $lastid=0;
                                        if(count($blog)>0){ 
                                           foreach ($blog as $key=>$value) {
                                                if($lastid==0){
                                                    $lastid=$value['id'];
                                                }
                                        ?>
                                                <tr id="userrow<?=$value['id']; ?>">
                                                   <td class="whiteText" style="vertical-align: top;"><?=$value['stage_name']; ?></td>
                                                   <td class="emailcolor" style="vertical-align: top;"><?=$value['comment']; ?></td>
                                                   <td style="vertical-align: top;"class="emailcolor"><?=date('d.m.y, h:i a', strtotime($value['added_date'])); ?></td>
                                                   <td>
                                                       <?php
                                                       if($value['user_id']==$this->session->userdata('user_id')){
                                                           echo '<a href="javascript:void(0)" onclick="showEditComment('.$value['id'].', '.$projectId.')">Edit</a> | <a href="javascript:void(0)" onclick="return removeblogcomment('.$value['id'].', '.$projectId.')">Delete</a>';
                                                       }
                                                       ?>

                                                   </td>
                                                </tr>
                                        <?php

                                           }
                                           //echo '<script>getNewBlogComments('.$lastid.','.$projectId.');</script>';
                                        }
                                        
                                      
                                        ?>
                                                
                                    </table>
                                 </div>
                
                            </div>
            <!-- /row -->
        </div>
    </div>
    <!-- /panel -->
</div>
<script>
    $( window ).load(function() {
        getNewBlogComments('<?=$lastid; ?>','<?=$projectId; ?>');
    });
</script>
<?php
//echo '<script></script>';
?>

    