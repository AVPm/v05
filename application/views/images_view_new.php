<?php
// ****************************************************************************
// 
//     User's project images view
//
// ****************************************************************************
//session_start();
$sessionid=session_id();
$currAlbumId='';
$albumInfoArr=  array();
$userid=($this->session->userdata('user_id')>0) ? $this->session->userdata('user_id') : '';
if(!empty($albumsArr)){
    $newarr = current($albumsArr);
    $currAlbumId=$newarr['id'];
    
    foreach($albumsArr as $id => $value){
        if($value['id']==$curr_albumid){
            $albumInfoArr['thumbnail']=$value['thumbnail'];
            $albumInfoArr['name']=$value['name'];
            $albumInfoArr['description']=$value['description'];
        }
    }
}

?>
<link rel="stylesheet" type="text/css" href="<?=base_url(); ?>/css/project_page.css"  />
<?php /*<script type="text/javascript" src="<?=base_url(); ?>/js/script_supersized_mod.js"></script> */ ?>
<script type="text/javascript" src="<?=base_url(); ?>/js/supersized.js"></script>
<?php /*<script type="text/javascript" src="<?=base_url(); ?>/js/pixastic.js"></script>*/ ?>
<script src="<?=base_url(); ?>/js/avp_new.js" type="text/javascript"></script>
    <script src="<?=base_url(); ?>/js/jquery.ui.touch-punch.min.js"></script>
<script>
    window['flags'] = { upscale: <?=@$image_upscale; ?> };
    var curr_project='<?=@$projectId; ?>';
    var curr_album='<?=@$curr_albumid; ?>';
    var base_url='<?=base_url(); ?>';  
    var imageid='<?=@$imageid; ?>';

    var upload_url= "<?=base_url(); ?>project/album/imageupload";	// Relative to the SWF file
    var image_version="<?=@$imageversion; ?>";
    var imageid_version="<?=@$imageid ?>";
    //var album_id="<?=$curr_albumid; ?>";
    var pageAction='imageupload';
    var V04_PATH='<?=V04_PATH; ?>';  
</script>
<script>
    $(document).ready(function(){
        resizeDiv();
        $("#albumlisting").niceScroll();
        $("#commentListing").niceScroll();
        $("#imagesListing").niceScroll();
    });
    
    $( window ).resize(function() {
        resizeDiv();
    });
</script>
<script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<script type="text/javascript">
    window.onload = function()
    {
        CKEDITOR.replace( 'comment', {
                removePlugins: 'bidi,div,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley/maximize',
                //removeButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',
                format_tags: 'p;h1;h2;h3;pre;address',
                height: 250

        } );
        
        CKEDITOR.replace( 'comment_edit', {
                removePlugins: 'bidi,div,forms,flash,horizontalrule,iframe,justify,table,tabletools,smiley/maximize',
                //removeButtons: 'Anchor,Underline,Strike,Subscript,Superscript,Image',
                format_tags: 'p;h1;h2;h3;pre;address',
                height: 250

        } );
        

        CKEDITOR.on('dialogDefinition', function( ev ){
            var dialogName = ev.data.name;
            var dialogDefinition = ev.data.definition;
            if ( dialogName == 'link' ){
                // Get a reference to the "Target" tab.
                        var targetTab = dialogDefinition.getContents( 'target' ); 
                        // Set the default value for the target field.
                        var targetField = targetTab.get( 'linkTargetType' );
                        targetField['default'] = '_blank';
                 }
        });
    }

    //////////end of validations////////////////
</script>

<?php
// add album popup
    if($userid>0):
?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="add_album" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Add Album</h4>
                        </div>
                        <div class="modal-body">
                                <form action="" enctype="multipart/form-data" method="post" name="addalbum" id="addalbum">
                                <?php
                                   /*
                                    * hidden field with value of project id
                                    */
                                       $data = array(
                                       'name'        => 'projectid_hidden',
                                       'id'          => 'projectid_hidden',
                                       'value'       => $projectId,
                                       'type'        => 'hidden',
                                       );

                                       echo form_input($data);

                                       //echo form_label('Album Name: ', 'album_name'); 
                                       echo '<label class="control-label">Album Name:</label>';
                                       echo '<br>';
                                       $data = array(
                                                       'name'        => 'album_name',
                                                       'id'          => 'album_name',
                                                       'class'  =>  'form-control',
                                                     );
                                       echo form_input($data);

                                       echo '<br>';

                                       //echo form_label('Album Description: ', 'album_info'); 
                                       echo '<label class="control-label">Album Description:</label>';
                                       echo '<br>';
                                       $data = array(
                                                       'name'        => 'album_info',
                                                       'id'          => 'album_info',
                                                       'class'  =>  'form-control',
                                                     );
                                       echo form_input($data);
                                       ?>
                                       <br>
                                       <label class="control-label">Thumbnail:</label><br>
                                       <input type="file" class="filestyle" data-icon="false" name="thumbnail" id="fileselecter" />
                                       <br><label class="control-label">Upscale Images:</label><br><input type="checkbox" value="1" name="upscale" id="upscale">
                                       <label class="margin-zero" for="upscale"><span></span></label>
                                       <br>
                                                        
                                       <?php /* Thumbnail Type: <br><input type="radio" onfocus="this.blur();" value="png" name="thumbnail_type" class="radio_thumbnailtype" checked="checked">PNG &nbsp; <input type="radio" onfocus="this.blur();" value="jpg" name="thumbnail_type" class="radio_thumbnailtype">JPG<br>*/ ?>
                               </form>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#addalbum').submit();">Save Album</button>
                        </div>
                </div>
        </div>
</div>
<?php 
endif; 
// add album popup end
?>

<?php
//edit album popup
    if(!empty($albumInfoArr) && $userid>0):
?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit_album" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Edit Album</h4>
                                <a style="position: absolute; top: 12px; right: 45px;" class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removealbum(<?=$curr_albumid; ?>, 'images')">Remove Album</a>
                        </div>
                        <div class="modal-body">
                                <?php

                                    echo form_open_multipart("", array('id'=>'editalbum'));
                                    /*
                                     * hidden field with value of project id
                                     */
                                    $data = array(
                                    'name'        => 'album_hidden',
                                    'id'          => 'album_hidden',
                                    'value'       => $curr_albumid,
                                    'type'        => 'hidden',
                                    );

                                    echo form_input($data);

                                    /*
                                     * input field for album name
                                     */
                                    //echo form_label('Album Name: ', 'edit_album_name'); 
                                    echo '<label class="control-label">Album Name:</label>';
                                    echo '<br>';
                                    $data = array(
                                                    'name'        => 'edit_album_name',
                                                    'id'          => 'edit_album_name',
                                                    'value'       => $albumInfoArr['name'],
                                                    'class'  =>  'form-control',
                                                  );
                                    echo form_input($data);

                                    echo '<br>';

                                    /*
                                     * input field for album description
                                     */
                                    //echo form_label('Album Description: ', 'edit_album_description'); 
                                    echo '<label class="control-label">Album Description:</label>';
                                    echo '<br>';
                                    $data = array(
                                                    'name'        => 'edit_album_description',
                                                    'id'          => 'edit_album_description',
                                                    'value'       => $albumInfoArr['description'],
                                                    'class'  =>  'form-control',
                                                  );
                                    echo form_input($data);

                                    echo '<br>';

                                    /*
                                     * hidden field with value of album thumbnail
                                     */
                                    $data = array(
                                    'name'        => 'album_thumbnail_hidden',
                                    'id'          => 'album_thumbnail_hidden',
                                    'value'       => $albumInfoArr['thumbnail'],
                                    'type'        => 'hidden',
                                    );

                                    echo form_input($data);

                                ?>
                                <label class="control-label">Thumbnail:</label><br>
                                       <input type="file" class="filestyle" data-icon="false" name="edit_thumbnail" id="fileselecter" />
                            <br>

                                    <label class="control-label">Remove Current Folder Thumbnail:</label><br>
                                    
                                    <input type="checkbox" value="delete" name="deletethumb" id="deletethumb">
                                       <label class="margin-zero" for="deletethumb"><span></span></label>
                                       <br>
                                <label class="control-label">Upscale Images:</label><br>
                                    <?php
                                   // $upscaleChecked=($image_upscale=='true') ? 'checked' : '';
                                    $data = array(
                                        'name'        => 'upscaleimages',
                                        'id'          => 'upscaleimages',
                                        'value'       => '1',
                                        'class'       => 'checkbox'
                                        );

                                    if($image_upscale=='true')
                                        $data['checked']='checked';

                                    echo form_checkbox($data);
                                    
                                    echo '<label class="margin-zero" for="upscale"><span></span></label>';

                                    ?>
                            <?php echo form_close(); ?>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#editalbum').submit();">Edit Album</button>
                        </div>
                </div>
        </div>
</div>
<?php
    endif; 
//edit album popup ends
?>
<!-- album images sorting form start -->
<?php if($userid>0): ?>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="sort_album" class="modal fade modal-full-pad in">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Sort Album</h4>
                        </div>
                        <div class="modal-body">
                                <div id="containmentbox" class="sort_area">
                                    <ul id="sortbox" class="sortable ui-sortable" style="">
                                        <?php
                                        foreach ($albumsArr as $album){
                                             $thumbnail=($album['thumbnail']!='') ? V04_PATH.'uploads/'.$album['thumbnail'] : V04_PATH.'images/imageicon.png';
                                             echo '<li id="id_'.$album['id'].'" style="">
                                                     <img width="160" height="160" class="selector" src="'.$thumbnail.'">
                                                     <div class="album_name">'.$album['name'].'</div>
                                                 </li>';
                                        }
                                        ?>

                                     </ul>  
                                       <div class="clear">&nbsp;</div>
                             <script type="text/javascript">
                              var resultnumber = <?=count($albumsArr); ?>;
                              /*
                              $('#sortbox_album_images').sortable({
                                 revert: 100,
                                 containment: $('#containmentbox_album_images')
                              });
                              */


                             </script>	
                                   </div>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="saveOrder('images');">Save Album</button>
                        </div>
                </div>
        </div>
</div>

<?php endif; ?>
<!-- album images sorting form ends -->

<!-- add comment form start -->
<?php if($userid>0): ?>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addalbumCommentDiv" class="modal fade modal-full-pad in">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Add Comment</h4>
                        </div>
                        <div class="modal-body">
                                <div id="containmentbox" class="sort_area">
                                    <?php
                                    echo form_open("", array('id'=>'addcommentfrm')); 

                                    $data = array(
                                                'name'        => 'album_comment_hidden',
                                                'id'          => 'album_comment_hidden',
                                                //'value'       => $curr_albumid,
                                                'value'       => $imageid,
                                                'type'        => 'hidden',
                                                );

                                    echo form_input($data);
                                    /*            
                                    echo form_label('Stagename: ', 'stagename'); 
                                    echo '<br>';
                                    $data = array(
                                                    'name'        => 'stagename',
                                                    'id'          => 'stagename',
                                                    'value'       => $user_stagename,
                                                    'readonly'    => 'readonly'
                                                  );
                                    echo form_input($data);

                                    echo '<br>';
                                     * 
                                     */

                                   // echo form_label('Comment: ', 'comment'); 
                                    $data = array(
                                            'name'        => 'comment',
                                            'id'          => 'comment',
                                            'rows'        => '7',
                                            'cols'        => '36',
                                          );

                                    echo form_textarea($data);

                                    echo form_close(); 
                                    ?>
                                   </div>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#addcommentfrm').submit();">Save Comment</button>
                        </div>
                </div>
        </div>
</div>

<?php endif; ?>
<!-- add comment form ends -->


<!-- edit comment form start -->
<?php 
$lastCommentArr= array();
foreach ($album_comments as $key => $value) {
    if($imageid==$key){
        $lastCommentArr=$value[0];
        break;
    }
}
if($userid>0 && !empty($lastCommentArr)):

?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="editCommentDiv" class="modal fade modal-full-pad in">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Edit Comment</h4>
                                <a style="position: absolute; top: 12px; right: 45px;" class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removecomment('images',<?=@$lastCommentArr['id']; ?>)">Remove Comment</a>
                        </div>
                        <div class="modal-body">
                                <div id="containmentbox" class="sort_area">
                                    <?php
                                    echo form_open("", array('id'=>'editcommentfrm'));
                                    $data = array(
                                                'name'        => 'commentid_hidden',
                                                'id'          => 'commentid_hidden',
                                                'value'       => @$lastCommentArr['id'],
                                                'type'        => 'hidden',
                                                );

                                    echo form_input($data);

                                   // echo form_label('Comment: ', 'comment'); 
                                    $data = array(
                                            'name'        => 'comment_edit',
                                            'id'          => 'comment_edit',
                                            'rows'        => '7',
                                            'cols'        => '36',
                                            'value'       => @$lastCommentArr['comment']
                                          );

                                    echo form_textarea($data);

                                    echo form_close(); 
                                    ?>
                                   </div>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#editcommentfrm').submit();">Save Comment</button>
                        </div>
                </div>
        </div>
</div>

<?php endif; ?>
<!-- edit comment form ends -->

<?php
    if($userid>0):
        if($imageversion=='yes'){
            $versionClass='on';
            $imageClass='off';
        }
        else{
            $versionClass='off';
            $imageClass='on';
        }
?>
<!-- add images in album popup start -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="add_image_popup" class="modal fade modal-full-pad in">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Add Image</h4>
                                <a class="<?=$imageClass;?>" href="<?=base_url(); ?>project/images/<?=$projectId; ?>/<?=$curr_albumid; ?>/<?=$imageid;?>/addimage" id="image_only" style="position: absolute; top: 12px; right: 45px;">Add Images</a>
                                <a class="<?=$versionClass;?>" href="<?=base_url(); ?>project/images/<?=$projectId; ?>/<?=$curr_albumid; ?>/<?=$imageid;?>/imageversion" onclick="add_imageversion()" id="imageversion" style="position: absolute; top: 12px; right: 185px;">Add Imageversions</a>  
                        </div>
                        <div class="modal-body">
                                <div id="containmentbox" class="sort_area">
                                    <form action="" enctype="multipart/form-data" method="post" id="form1">
                                        <input type="hidden" name="image_version" id="image_version" value="no" />
                                        <input type="hidden" name="imageid_version" id="imageid_version" value="<?=$imageid; ?>" />

                                        <div id="fileuploader">
                                            <iframe src="<?=base_url('includes/file_uploader')  ?>" style="height: 250px; width:100%" frameborder="0" ></iframe> 
                                        </div>    


                                    </form>
                                       <div class="clear">&nbsp;</div>
                             
                                   </div>
                        </div>
                        <div class="modal-footer">
                                <button class="btn btn-default" type="button" onclick="window.location.href='/project/images/<?=$projectId; ?>/<?=$curr_albumid; ?>/<?=$imageid;?>';">Close</button>
                                <button class="btn btn-primary" type="button" id="donelink" style="display: none" onclick="window.location.href='<?=base_url(); ?>project/images/<?=$projectId; ?>/<?=$curr_albumid; ?>/<?=$imageid;?>';">Save Images</button>
                        </div>
                </div>
        </div>
</div>
    
<!-- add images in album popup ends -->
<?php
endif;
?>

<?php
//edit album images popup
    if(!empty($albumInfoArr) && $userid>0):
?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="edit_album_image" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Edit Image</h4>
                                <a style="position: absolute; top: 12px; right: 45px;" class="off" onfocus="this.blur();" href="javascript:void(0)" onclick="return removealbumimage('images', '<?=$imageid; ?>')">Remove Image</a>
                        </div>
                        <div class="modal-body">
                            <?php 
                                echo form_open_multipart("", array('id'=>'editimage'));

                                $data = array(
                                            'name'        => 'album_imageid_hidden',
                                            'id'          => 'album_imageid_hidden',
                                            'value'       => $imageid,
                                            'type'        => 'hidden',
                                            );

                                echo form_input($data);

                                //echo form_label('Title: ', 'image_title'); 
                                echo '<label class="control-label">Title:</label>';
                                echo '<br>';
                                $data = array(
                                                'name'        => 'image_title',
                                                'id'          => 'image_title',
                                                'value'       => $image_title,
                                                'class'  =>  'form-control',
                                              );
                                echo form_input($data);

                                echo '<br>';

                                //echo form_label('additional Info: ', 'additional_info'); 
                                echo '<label class="control-label">Additional Info:</label>';
                                $data = array(
                                        'name'        => 'additional_info',
                                        'id'          => 'additional_info',
                                        'rows'        => '7',
                                        'cols'        => '36',
                                        'value'       => $image_info,
                                        'class'  =>  'form-control',
                                      );

                                    echo form_textarea($data);
                            ?>
                            <br>
                            <?php echo form_close(); ?>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#editimage').submit();">Edit Image</button>
                        </div>
                </div>
        </div>
</div>
<?php
    endif; 
//edit album images popup ends
?>

<!-- album images sorting form start -->
<?php if($userid>0): ?>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="sorting_album_images" class="modal fade modal-full-pad in">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Sort Images</h4>
                        </div>
                        <div class="modal-body">
                                <div id="containmentbox" class="sort_area">
                                    <ul id="sortbox_album_images" class="sortable ui-sortable" style="">
                                        <?php
                                        foreach ($album_images as $albumImg){
                                            if($albumImg['image_version']==0):
                                                $title=($albumImg['title']!='') ? $albumImg['title'] : 'no title';
                                                echo '<li id="id_'.$albumImg['id'].'" style="">
                                                        <img width="160" height="160" class="selector" src="'.V04_PATH.'uploads/'.$albumImg['thumbnail'].'">
                                                        <div class="album_name">'.$title.'</div>
                                                    </li>';
                                            endif;
                                           }
                                        ?>

                                     </ul>  
                                       <div class="clear">&nbsp;</div>
                             <script type="text/javascript">
                              var resultnumber = <?=count($albumsArr); ?>;
                              /*
                              $('#sortbox_album_images').sortable({
                                 revert: 100,
                                 containment: $('#containmentbox_album_images')
                              });
                              */


                             </script>	
                                   </div>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="saveAlbumImagesOrder();">Save Images</button>
                        </div>
                </div>
        </div>
</div>

<?php endif; ?>
<!-- album images sorting form ends -->
<div class="col-md-9 nopad-right" id="centerImg" style="padding-left: 7px;">
        <!-- panel -->
        <div class="panel panel-piluku">
            <!--
            <div class="panel-heading">
                <h3 class="panel-title">
                    Center Image

                </h3>
            </div>
            -->
          <div class="panel-body" style="height: 371px;">
              <div class="row main-chart-parent" style="height: 100%">
                <table style="width:100%;height: 100%">
                    <tr>
                        <td align="center" valign="middle" style="vertical-align: middle; height: 100%; width: 100%">
                            <div id="centerLage_image" style="width:100%;height: 100%">
                                <?php
                                    if(isset($largeImgArr['fullimage']) && $largeImgArr['fullimage']!=''){
                                      //  echo '<img src="/uploads/'.$largeImgArr['fullimage'].'" style="visibility:hidden; width:24px; height:24px;" id="large_img">';
                                        echo '<script> var selectedImage ="'.V04_PATH.'uploads/'.$largeImgArr['fullimage'].'";</script>';
                                        /*
                                         * echo '<script>setTimeout(placePlayer("/uploads/'.$largeImgArr['fullimage'].'"), 1000);</script>';
                                         */


                                    }
                                    else{
                                        echo '<strong>&nbsp;</strong>';
                                    }
                                        //echo '<img src="'.base_url().'uploads/'.$largeImgArr['fullimage'].'" id="imagescaler" >';
                                    ?>
                            </div>
                        </td>
                    </tr>
                </table>
                
            </div>
            <!-- /row -->
        </div>
    </div>
    <!-- /panel -->
</div>
    <!-- col-md-6 -->

<div class="col-md-3 nopad-right" id="albumsList"style="padding-right: 9px;">
        <!-- panel -->
        <div class="panel panel-piluku">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if($project_status!='viewer' && $project_status!='none'){ ?>
                        <?php if($userid>0):  ?>
                            <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#add_album" > + Add </a>
                            <?php if($curr_albumid>0){ ?>
                            <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#edit_album"> I Edit </a>
                            <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#sort_album"> <> Sort </a>
                            <?php } 
                        endif;
                        ?>
                    <?php } ?>

              </h3>
          </div>
        <div class="panel-body" id="albumlisting" style="height: 371px;">
              <div class="row main-chart-parent" style="height: inherit; ">
                    <div id="centerlisting_albums">
                        <?php
                        if(!empty($albumsArr)){
                            $i=1;
                            $j=1;
                            foreach ($albumsArr as $album){
                                $classname=($curr_albumid==$album['id'] && $curr_albumid>0) ? 'linkons' : 'linkoff';
                                $i= ($i>2) ? 1 : $i;

                                if( strlen($j)==1 )
                                 {
                                 $count='0'.$j;
                                 }
                                 elseif( strlen($j)>=2 )
                                 {
                                 $count=$j;
                                 }

                            ?>
                        <div class="albumcontainer<?=$i;?>" id="sidenavi_<?=$album['id']; ?>" onclick="window.location.href='<?=base_url(); ?>project/images/<?=$projectId; ?>/<?=$album['id']; ?>'">


                                <div class="ac1">

                                  <div class="ac1num">
                                    <?=$count; ?>                          
                                  </div>
                                  <div class="ac1name">
                                    <a onfocus="this.blur();" class="<?=$classname; ?>" href="<?=base_url(); ?>project/images/<?=$projectId; ?>/<?=$album['id']; ?>"><?=$album['name']; ?></a>
                                  </div>
                                  <div class="ac1version">

                                  </div>
                                </div>

                                <div class="ac2">
                                  Album
                                  <br>
                                  Name
                                </div>
                                <?php
                                    $thumbnail=($album['thumbnail']!='') ? V04_PATH.'uploads/'.$album['thumbnail'] : V04_PATH.'images/imageicon.png';
                                ?>
                                <div class="ac3">
                                  <a onfocus="this.blur();" href="<?=base_url(); ?>project/images/<?=$projectId; ?>/<?=$album['id']; ?>">
                                    <img width="98" height="98" border="0" alt="" src="<?=$thumbnail;?>">
                                  </a>
                                </div>


                              </div>
                            <?php
                                $i++;
                                $j++;
                                }
                            }
                            ?>
                    </div>  
            </div>
            <!-- /row -->
        </div>
    </div>
    <!-- /panel -->
</div>
    
<div class="col-md-9 nopad-right" id="imagesList" style="padding-left: 7px;">
        <!-- panel -->
        <div class="panel panel-piluku">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if($project_status!='viewer' && $project_status!='none'){ ?>
                    <?php if($userid>0):  ?>
                        <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#add_image_popup" id="showImgAdd"> + Add </a> 
                        <?php if($imageid>0){  ?>
                        <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#edit_album_image"> I Edit </a> 
                        <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#sorting_album_images" > <> Sort </a>  
                        <?php } 
                    endif;
                    ?>
                    <?php } ?>
                </h3>
          </div>
        <div class="panel-body" style="height: 152px;" id="imagesListing">
            <div class="row main-chart-parent">
                <?php
                if(!empty($album_images)){
                            $i=1;
                            $j=1;
                            $imageversionidArr=  array();
                            $imageidArr=array();
                            foreach ($album_images_versions as $key => $value) {
                                array_push($imageversionidArr, $value['image_version_id']);
                                array_push($imageidArr, $value['image_id']);
                            }
//                            echo '<pre>';
//                            print_r($imageversionidArr);
//                            die('s');
                            foreach ($album_images as $albumImg){
                                
                                if($albumImg['image_version']==0):
                                
                                    $classname=($imageid==$albumImg['id'] && $imageid>0) ? 'linkons' : 'linkoff';
                                    $i= ($i>2) ? 1 : $i;
                                    if( strlen($j)==1 ){
                                        $count='00'.$j;
                                    }
                                    elseif( strlen($j)==2 ){
                                        $count='0'.$j;
                                    }
                                    elseif( strlen($j)>=3 ){
                                        $count=$j;
                                    }
                            ?>
                        <div id="bottom_<?=$albumImg['id']; ?>" class="imagecontainer">
                            <div style="position:relative; width:113px;">
                             <div style="position:absolute" class="preloaderholder"></div> 
                                 <a href="<?=base_url(); ?>project/images/<?=$projectid; ?>/<?=$albumid ?>/<?=$albumImg['id']; ?>" onfocus="this.blur();" class="bottomlinks" id="link<?=$albumImg['id']; ?>">
                                     <img onmouseover="showComments(<?=$albumImg['id']; ?>)" height="112" border="0" id="medium3648" title="<?=$albumImg['title']; ?>" alt="" src="<?=V04_PATH; ?>uploads/<?=$albumImg['thumbnail']; ?>" />
                                 </a> 
                             &nbsp;<a id="textlink<?=$albumImg['id']; ?>" class="<?=$classname; ?>" href="<?=base_url(); ?>project/images/<?=$projectid; ?>/<?=$albumid ?>/<?=$albumImg['id']; ?>" style="margin-right: -3px;" onmouseover="showComments(<?=$albumImg['id']; ?>); showlargeImg(this.id);" onmouseout="placePlayer(selectedImage);" alt="<?=$albumImg['full_image']; ?>"><?=$count; ?></a>
                                 <?php
                                 echo '<div class="preloadedImages" style="background-image:url('.V04_PATH.'uploads/'.$albumImg['full_image'].')"></div>';
                                $k=2;
                                //if($albumImg['id']==$album_images_versions)
                                foreach ($album_images_versions as $key => $value) {
                                    $versionsclassname=($imageid==$value['image_id'] && $imageid>0) ? 'linkons' : 'linkoff';
                                    //if($imageid==$value['image_version_id'] && $imageid>0){
                                    //if(in_array($imageid, $imageversionidArr) || $imageid==$value['image_id']){
                                        $versionlink=base_url().'project/images/'.$projectid.'/'.$albumid.'/'.$value['image_id'].'" id="textlink'.$value['image_id'].'" alt="'.$value['full_image'];
                                        $versionImgHover='onmouseover="showlargeImg(this.id); showComments('.$value['image_id'].')"  onmouseout="placePlayer(selectedImage);"';
                                        echo '<div class="preloadedImages" style="background-image:url('.V04_PATH.'uploads/'.$value['full_image'].')"></div>';
                                   // }
                                   // else{
                                   //     $versionlink='javascript:void(0)';
                                   //     $versionImgHover='';
                                   // }
                                    if( strlen($k)==1 ){
                                        $countk='0'.$k;
                                    }
                                    else {
                                        $countk=$k;
                                    }
                                    if($albumImg['id']==$value['image_version_id']){
                                        echo '<span class="hg">|</span>';
                                        echo '<a class="'.$versionsclassname.'" href="'.$versionlink.'" '.$versionImgHover.'>.'.$countk.'</a>';
                                        $k++;
                                    }
                                }
                             ?>
                             
                             <?php
                                if($j==1){
                             ?>
                                <script type="text/javascript">
                                <?php //echo 'var selectedImage ='.$largeImage ?>
                                $(document).ready(function(){
                                 //alert($("#medium'.$id.'")[0].complete);
                                 //default image preloader
                                 jQuery("<img>").attr("src", selectedImage).load(function(){
                                   placePlayer(selectedImage);
                                 }).each(function() {
                                    if(this.complete) $(this).trigger("load");
                                 });             

                                });                              
                                </script>

                             <?php
                                }
                             ?>


                           </div>
                        </div>
                        <?php
                            $i++;
                            $j++;
                        endif;
                        }
                        
                    }
                    else{
                        echo '<div>&nbsp;</div>';
                    }
                    ?>
            </div>
            <!-- /row -->
        </div>
    </div>
    <!-- /panel -->
</div>
    
<div class="col-md-3 nopad-right" id="commentList" style="padding-right: 9px;">
        <!-- panel -->
        <div class="panel panel-piluku">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if($project_status!='viewer' && $project_status!='none'){ ?>
                        <?php if($userid>0 && $imageid>0): ?>
                        <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#addalbumCommentDiv"> + Add </a>
                        <a href="javascript:void(0)" onfocus="this.blur();" data-toggle="modal" data-target="#editCommentDiv"> I Edit </a>
                        <?php else:
                        echo '&nbsp;'    ;
                            endif; 
                        ?>
                    <?php } ?>
                </h3>
                
          </div>
        <div class="panel-body" style="height: 152px;" id="commentListing">
              <div class="row main-chart-parent" style="height: inherit">
                <?php
                $i=1;
                $html='';
                foreach ($album_comments as $key => $value) {
                    $display=($imageid==$key) ? 'display:block' : '';
                    $html.='<div id="commentsVideo'.$key.'" class="showCommentsVideo" style="'.$display.'">';
                    $j=1;
                    foreach ($value as $arrkey => $arrvalue) {
                        if( strlen($j)==1 ){
                            $count='0'.$j;
                        }
                        else{
                            $count=$j;
                        }
                        $html.= '<div class="commentnormal">
                                    <div class="commentnames">'.$count.' - '.$arrvalue['stage_name'].' ('.date('d.m.y, h:i A', strtotime($arrvalue['added_date'])).') :</div>
                                    <div class="commentmessage"> '.$arrvalue['comment'].'</div>
                                </div>';
                        $j++;
                    }
                    $html.='</div>';
                    
                }
                
                echo $html;
                
                ?>
            </div>
            <!-- /row -->
        </div>
    </div>
    <!-- /panel -->
</div>
<script>
        $(function() {
            $( ".sortable" ).sortable();
            $( ".sortable" ).disableSelection();

          });
          
        $( window ).load(function() {
            resizeDiv();
            <?php
                if($imageversion=='yes' || $imageversion=='addimage')
                    echo "$('#showImgAdd').click();";
            ?>
        });
        
</script>
