<?php
// ****************************************************************************
// 
//     User listing view of projects and users
//
// ****************************************************************************

?>
<div id="roleChange_msg" style="display: none"></div>
     <div id="user_list">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th style="width: 115px;">Stage Name</th>
                <th style="width: 165px;">Real Name</th>
                <th style="width: 220px;">Email</th>
                <th style="width: 160px;">Role</th>
                <th style="width: 250px;">Date Added</th>
            </tr>
            <?php
               $options = array('0'=>'None', '2'=>'Viewer', '3'=>'Contributor', '4'=>'Co-Owner');

               foreach ($userslist as $key) {
            ?>
                   <tr>
                       <td class="whiteText"><?=$key['stage_name']; ?></td>
                       <td><span class="whiteText"><?=ucwords($key['last_name']); ?></span>, <span class="firstname_color"><?=ucwords($key['first_name']); ?></span></td>
                       <td class="emailcolor"><?=$key['email']; ?></td>
                       <td>
                       <?php 
                           $id = 'id="role_'.$key['child_userid'].'" onchange="changeRole(this.value, '.$key['child_userid'].')"';
                           echo '<div class="styled-select">';
                           echo form_dropdown('role', $options, $key['role'], $id);
                           echo '</div>';
                       ?>
                       </td>
                       <td class="datecolor"><?=date('d-m-Y h:i:s A', strtotime($key['added_date'])); ?></td>
                   </tr>
            <?php

               }
            ?>
         
         
        </table>
     </div>