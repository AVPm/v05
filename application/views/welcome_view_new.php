<?php
// ****************************************************************************
// 
//     Welcome view after user login
//
// ****************************************************************************
?>
<script>
    function resizeuserlist(){
        var wh = $(window).height();
        //wh;
        var topmenuh=$('.top-bar').height();
        //topmenuh;
        var headingDivh=$('#usersList .panel-heading').height();

        var filter=$('#projectList').height();

        var h=wh-topmenuh-headingDivh-filter;
        h;
        //filter;
        //h;
        $('#usersList .panel-body').css('height', h+'px');
        $('#usersList #users_listing').css('height', h+'px');
    }
    
    $(document).ready(function(){
        resizeuserlist();
        //showSearchResult();
        $("#users_listing").niceScroll();
    });
    $(window).resize(function() {
        $("#users_listing").niceScroll();
        resizeuserlist();
    });
</script>
	
    <?php
    /*
     * load user's project list on top in horizontal scroll div
     */
    $this->view('project_list_view_new');
    if($this->session->userdata('user_id')!='' && !strstr($_SERVER['REQUEST_URI'], 'complete_registration')){
        $this->view('search_side_view_new');
    }
    ?>
<?php
    if($userLimit=='yes'):
?> 
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addUserDiv" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Add User</h4>
                        </div>
                        <div class="modal-body">
                                <div id="adduserHtml">
                                    <div class="popup_top">
                                            <img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg" style="float: left">
                                            <div class="run" style="float: left; margin-left: 5px; margin-top: 30px;">Enter the email address of the user you want to add.</div>
                                            
                                    </div>
                                    <div class="clear">&nbsp;</div> 
                                    <div class="popup_forms">
                                         <?php
                                            echo form_label('Email:', 'email'); 
                                            echo '<br>';
                                            $data = array(
                                                            'name'        => 'addUser_email',
                                                            'id'          => 'addUser_email',
                                                            'class'       => 'form-control',
                                                            'placeholder' => 'Email'
                                                          );

                                            echo form_input($data);
                                            echo '<br>';
                                            ?>


                                    </div>
                                </div>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="checkAddUser()" id="adduserbtn">Add User</button>
                        </div>
                </div>
        </div>
</div>

<?php
    endif;
?>
<div style="" id="usersList" class="col-md-9 nopad-right pull-left">
        <!-- panel -->
        <div class="panel panel-piluku">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Users
                    <a id="showImgAdd" data-target="#addUserDiv" data-toggle="modal" onfocus="this.blur();" href="javascript:void(0)" style="margin-left: 15px;">Add</a>
                </h3>
          </div>
        <div style="height: 230px;" class="panel-body">
            <div class="row main-chart-parent">
               
                                <div id="users_listing">
                                    <table cellpadding="0" cellspacing="0" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Stage Name</th>
                                                <th>Real Name</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>Action</th>
                                            </tr>
                                            
                                        </thead>
                                        
                                        <?php
                                           $options = array('0'=>'None', '2'=>'Viewer', '3'=>'Contributor', '4'=>'Co-Owner');
                                           $i=1;
                                           foreach ($user_listing as $key) {
                                        ?>
                                                <tr id="userrow<?=$key['child_userid']; ?>">
                                                    <td><?=$i; ?></td>
                                                    <td class="whiteText"><?=$key['stage_name']; ?></td>
                                                    <td><span class="whiteText"><?=ucwords($key['last_name']); ?></span>, <span class="firstname_color"><?=ucwords($key['first_name']); ?></span></td>
                                                    <td class="emailcolor"><?=$key['email']; ?></td>
                                                    <td>
                                                    <?php 
                                                        $id = 'id="role_'.$key['child_userid'].'" onchange="changeRole(this.value, '.$key['child_userid'].')" class="form-control"';
                                                        echo '<div class="styled-select">';
                                                        echo form_dropdown('role', $options, $key['role'], $id);
                                                        echo '</div>';
                                                    ?>
                                                    </td>
                                                    <td><a href="javascript:void(0)" onclick="removeUser(<?=$key['child_userid']; ?>)">Remove</a></td>
                                                </tr>
                                        <?php
                                            $i++;
                                           }
                                        ?>

                                                
                                    </table>
                                 </div>
                
                            </div>
            <!-- /row -->
        </div>
    </div>
    <!-- /panel -->
</div>


    