<?php
// ****************************************************************************
// 
//     Welcome view after user login
//
// ****************************************************************************
?>

	
    <?php
    /*
     * load user's project list on top in horizontal scroll div
     */
    $this->view('project_list_view');
    ?>
    <hr class="hr_grey">
    <div class="clear">&nbsp;</div>
    <!-- my own project listing close -->
    <div style="position:relative">
        <a class="but_down floatLeft" href="javascript:void(0)" id="drop_users" onclick="showhideDivData('user_list', this.id)">Users</a>
        <?php
        if($userLimit=='yes'):
        ?>
            <div class="linkpositioner floatLeft">
                <a href="javascript:void(0)" onclick="openDiv('addUserDiv');">Add</a> <!-- <span>|</span> <a href="javascript:void(0)">Edit</a> -->
            </div>
        <?php
        endif;
        ?>
    </div>
    
    <div class="clear">&nbsp;</div>
    
     <div id="edit_profile" class="popup" style="width: 640px;">
         
	<div class="popup_head">
		<span>Edit User</span>	
	</div>
	<div class="popup_top">
		<img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
		<div class="run">Update your user details in the form below. When changing email or password you need to enter the current password.</div>
		<div class="clear">&nbsp;</div> 
	</div>
	<div class="popup_forms">
            <?php echo form_open(base_url("user/updateProfile"), array('onsubmit'=>'return chkProfile()', 'id'=>'update_profile')); ?>
                        <?php
			echo form_label('Email: ', 'email_address'); 
                        echo '<br>';
                        $data = array(
                                        'name'        => 'email_address',
                                        'id'          => 'email_address',
                                        'value'       => $user_details['details']['email'],
                                        'onchange'    => 'return chk_useremail()',
                                      );
                        echo form_input($data);
                        ?>
                        &nbsp; <span id="useremail_msg" style="display: none">Available</span>
                        <br>
                        <?php
			echo form_label('New Password:', 'new_pass'); 
                        echo '<br>';
                        $data = array(
                                        'name'        => 'new_pass',
                                        'id'          => 'new_pass',
                                        'value'       => set_value('new_pass'),                                        
                                      );
                        echo form_password($data);
                        ?>
			<br>
                        <?php
			echo form_label('Current Password:', 'curr_pass');
                        echo '<br>';
                        $data = array(
                                        'name'        => 'curr_pass',
                                        'id'          => 'curr_pass',
                                        'value'       => set_value('curr_pass'),                                        
                                      );

                        echo form_password($data);
                        echo '<br>';
                        
			echo form_label('First Name:', 'first_name'); 
                        echo '<br>';
                        $data = array(
                                        'name'        => 'first_name',
                                        'id'          => 'first_name',
                                        'value'       => $user_details['details']['first_name'],                                        
                                      );

                        echo form_input($data);
                        echo '<br>';
                        
                        echo form_label('Last Name:', 'last_name'); 
                        echo '<br>';
                        $data = array(
                                        'name'        => 'last_name',
                                        'id'          => 'last_name',
                                        'value'       => $user_details['details']['last_name'],                                        
                                      );

                        echo form_input($data);
                        echo '<br>';
                        
                        echo form_label('Stage Name:', 'stage'); 
                        echo '<br>';
                        $data = array(
                                        'name'        => 'stage',
                                        'id'          => 'stage',
                                        'value'       => $user_details['details']['stage_name'],                                        
                                      );

                        echo form_input($data);
                        echo '<br>';
                        
                        echo form_label('Company Name', 'company');
                        echo '<br>';
                        $data = array(
                                        'name'        => 'company',
                                        'id'          => 'company',
                                        'value'       => $user_details['details']['company_name'],                                        
                                      );

                        echo form_input($data);
                        echo '<br>';
                        ?>
	</div>
	<div class="popup_bottom">
		<a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('edit_profile')">Cancel</a>
		<a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="$('#update_profile').submit();">Submit</a>
		<div class="clear">&nbsp;</div> 
	</div>		
         
        <?php echo form_close(); ?>
         
         <?php
         //old edit profile html
         /*
         <!-- <div style="position: absolute; left: 315px; top: 10px; font-weight: bold; cursor:pointer; color: #ff3333" onclick="return closeDiv('edit_profile')">X</div>-->
         <?php echo form_open("user/updateProfile", array('onsubmit'=>'return chkProfile()', 'id'=>'update_profile')); ?>
		        
		<p>
                    <?php
			echo form_label('First Name', 'first_name'); 
                        $data = array(
                                        'name'        => 'first_name',
                                        'id'          => 'first_name',
                                        'value'       => $user_details['details']['first_name'],                                        
                                      );

                          echo form_input($data);
                        ?>
		</p>  
                <p>
                    <?php
			echo form_label('Last Name', 'last_name'); 
                        $data = array(
                                        'name'        => 'last_name',
                                        'id'          => 'last_name',
                                        'value'       => $user_details['details']['last_name'],                                        
                                      );

                          echo form_input($data);
                        ?>
		</p>
                
                <p>
                    <?php
			echo form_label('Your Email', 'email_address'); 
                        $data = array(
                                        'name'        => 'email_address',
                                        'id'          => 'email_address',
                                        'value'       => $user_details['details']['email'],
                                        'onchange'    => 'return chk_useremail()',
                                      );

                          echo form_input($data);
                        ?>
                    &nbsp; <span id="useremail_msg" style="display: none">Available</span>
		</p>
		<p>
                    <?php
			echo form_label('New Password', 'new_pass'); 
                        $data = array(
                                        'name'        => 'new_pass',
                                        'id'          => 'new_pass',
                                        'value'       => set_value('new_pass'),                                        
                                      );

                          echo form_password($data);
                        ?>
		</p>
                <p>
                    <?php
			echo form_label('Current Password', 'curr_pass'); 
                        $data = array(
                                        'name'        => 'curr_pass',
                                        'id'          => 'curr_pass',
                                        'value'       => set_value('curr_pass'),                                        
                                      );

                          echo form_password($data);
                        ?>
		</p>
                <p>
                    <?php
			echo form_label('Company Name', 'company'); 
                        $data = array(
                                        'name'        => 'company',
                                        'id'          => 'company',
                                        'value'       => $user_details['details']['company_name'],                                        
                                      );

                          echo form_input($data);
                        ?>
		</p>
                <p>
                    <?php
			echo form_label('Stage Name', 'stage'); 
                        $data = array(
                                        'name'        => 'stage',
                                        'id'          => 'stage',
                                        'value'       => $user_details['details']['stage_name'],                                        
                                      );

                          echo form_input($data);
                        ?>
		</p>
                <p>
                    <a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('edit_profile')">Cancel</a>
                    <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="$('#update_profile').submit();">Submit</a>
                    
		</p>
	<?php echo form_close(); ?>
         */
         ?>
    </div>
     
    <?php
        if($userLimit=='yes'):
    ?> 
    <div class="popup" id="addUserDiv" style="width: 640px;">
	<div class="popup_head">
		<span>Add User</span>	
	</div>
        <div id="adduserHtml">
            <div class="popup_top">
                    <img alt="" src="<?=base_url(); ?>images/popup_pic1.jpg">
                    <div class="run">Enter the email address of the user you want to add.</div>
                    <div class="clear">&nbsp;</div> 
            </div>
            <div class="popup_forms">
                 <?php
                    echo form_label('Email:', 'email'); 
                    echo '<br>';
                    $data = array(
                                    'name'        => 'addUser_email',
                                    'id'          => 'addUser_email',
                                  );

                    echo form_input($data);
                    echo '<br>';
                    ?>


            </div>
        </div>
	<div class="popup_bottom">
		<a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('addUserDiv')">Cancel</a>
                <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="checkAddUser();">Submit</a>
		<div class="clear">&nbsp;</div> 
	</div>		
    </div> 
    <?php
        endif;
    ?>
    
    <?php 
    //old add user html form
     /*
     
     <div id="addUserDiv" class="popup" style="width: 322px;">
         <div>Add User</div>
         <!--<div style="position: absolute; left: 315px; top: 202.056.231.117 10px; font-weight: bold; cursor:pointer; color: #ff3333" onclick="return closeDiv('addUserDiv')">X</div>-->
         <div id="adduserHtml">
             <?php
                echo form_label('Email', 'email'); 
                $data = array(
                                'name'        => 'addUser_email',
                                'id'          => 'addUser_email',
                              );

                echo form_input($data);
                
             <p>
                 <a style="color:#ef0808; margin-left:55px;" href="javascript:void(0)" onclick="return closeDiv('addUserDiv')">Cancel</a>
                 <a style="color:#b1d400; margin-left:55px;" href="javascript:void(0)" onclick="checkAddUser();">Submit</a>
             </p>
         </div>
         
     </div>
     
     */
     ?>
     <div id="roleChange_msg" style="display: none"></div>
     <div id="user_list">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th style="width: 115px;">Stage Name</th>
                <th style="width: 190px;">Real Name</th>
                <th style="width: 220px;">Email</th>
                <th style="width: 160px;">Role</th>
<!--                <th style="width: 250px;">Date Added</th>-->
                <th style="width: 110px;">Action</th>
            </tr>
            <?php
               $options = array('0'=>'None', '2'=>'Viewer', '3'=>'Contributor', '4'=>'Co-Owner');

               foreach ($user_listing as $key) {
            ?>
                    <tr id="userrow<?=$key['child_userid']; ?>">
                        <td class="whiteText"><?=$key['stage_name']; ?></td>
                        <td><span class="whiteText"><?=ucwords($key['last_name']); ?></span>, <span class="firstname_color"><?=ucwords($key['first_name']); ?></span></td>
                        <td class="emailcolor"><?=$key['email']; ?></td>
                        <td>
                        <?php 
                            $id = 'id="role_'.$key['child_userid'].'" onchange="changeRole(this.value, '.$key['child_userid'].')"';
                            echo '<div class="styled-select">';
                            echo form_dropdown('role', $options, $key['role'], $id);
                            echo '</div>';
                        ?>
                        </td>
                        <?php /*<td class="datecolor"><?=date('d-m-Y h:i:s A', strtotime($key['added_date'])); ?></td>*/ ?>
                        <td><a href="javascript:void(0)" onclick="removeUser(<?=$key['child_userid']; ?>)">Remove</a></td>
                    </tr>
            <?php

               }
            ?>
         
         
        </table>
     </div>
     
     <div>
         
     </div>
    