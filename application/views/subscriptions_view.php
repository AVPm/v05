<?php
// ****************************************************************************
// 
//     User's subscriptions detail view
//
// ****************************************************************************


?>
<style>
    .bottom_table tr th{
        font-size: 12px;
    }
</style>
<div class="clear">&nbsp;</div>
<div><?=$place_order_msg; ?></div>
<h4>My subscriptions</h4>
<table class="bottom_table">
    <tbody>
        <tr>
            <th style="width: 50px; ">Sr. No.</th>
            <th>PACKAGE NAME</th>
            <th>ORDER DATE</th>
            <th>ACTIVATION DATE</th>
            <th>EXPIRY DATE</th>
            <th>USERS</th>
            <th>TRAFFIC</th>
            
        </tr>
        <?php
        $i=1;
        foreach ($userSubscriptions as $key => $value) {
       ?>
        <tr>
            <td class="tableheading"><?=$i; ?></td>
            <td class="tableheading"><?=strtoupper($value['name']); ?></td>
            <td class="tableheading"><?=date('d-M-Y', strtotime($value['order_date'])); ?></td>
            <td class="tableheading">
                <?php
                if($value['status']=='1'){
                    echo date('d-M-Y', strtotime($value['activation_date'])); 
                }
                else{
                    echo '<a href="'.base_url('subscription/activate/'.$value['invoice']).'">Activate Now</a>';
                }
                ?>
            </td>
            <td class="tableheading">
                <?php
                if($value['status']=='1')
                    echo date('d-M-Y', strtotime($value['expiry_date'])); 
                ?>
            </td>
            <td class="tableheading">
                <?php 
                    echo 'Total: '. $value['users']; 
                    echo '<br />';
                    echo 'Remaining: '. $remainingUsers;
                ?>
            </td>
            <td class="tableheading">
                <?php
                    echo 'Total: '.$value['traffic_storage'].' GB';
                    echo '<br />';
                    echo 'Remaining: '.number_format(($value['traffic_storage']-$trafficSize), 3);
                ?>
            </td>
        </tr>
        <?php
        $i++;
        }
        ?>
        <tr>
            
        </tr>
</tbody>
    </table>
<?php
//    echo '<pre>';
//    print_r($userSubscriptions);
//    echo '</pre>'
?>
<div class="clear">&nbsp;</div>
<h4>Subscriptions Packages</h4>
<div class="bottom_div">
    <?php
    /*
    <table class="bottom_table">
          <tbody>
              <tr>
                  <th class="tableheading">Plan:</th>
                  <th>SMALL</th><th>MEDIUM</th><th>LARGE</th><th>X-LARGE</th>                        
              </tr>
              <tr>
                      <td class="tableheading">Traffic:</td>
                      <td>1 GB</td><td>10 GB</td><td>100 GB</td><td>1000 GB</td>                    </tr>
              <tr>
                      <td class="tableheading">Users:</td>
                      <td>2</td><td>5</td><td>20</td><td>100</td>				
              </tr>
              <tr>
                      <td class="tableheading">Monthly:</td>

                      <td>$ 001</td><td>$ 002</td><td>$ 003</td><td>$ 004</td>                    </tr>
              <tr>
                      <td class="tableheading">&nbsp;</td>
                      <td><a href="javascript:void(0)">Buy Now</a></td>
                      <td><a href="javascript:void(0)">Buy Now</a></td>
                      <td><a href="javascript:void(0)">Buy Now</a></td>
                      <td><a href="javascript:void(0)">Buy Now</a></td>                    </tr>
      </tbody>
    </table>
     * 
     */
    ?>
    <table class="bottom_table">
                <tbody>
                    <tr>
                        <th class="tableheading">Plan:</th>
                        <?php
                        foreach ($subscriptions as $key => $value) {
                            echo '<th>'.strtoupper($value['name']).'</th>';
                        }
                        ?>
                        
                    </tr>
                    <tr>
                            <td class="tableheading">Traffic:</td>
                            <?php
                            foreach ($subscriptions as $key => $value) {
                                echo '<td>'.$value['traffic_storage'].' GB</td>';
                            }
                            ?>
                    </tr>
                    <tr>
                            <td class="tableheading">Users:</td>
                            <?php
                            foreach ($subscriptions as $key => $value) {
                                echo '<td>'.$value['users'].'</td>';
                            }
                            ?>				
                    </tr>
                    <tr>
                            <td class="tableheading">Monthly:</td>
                           
                            <?php
                            foreach ($subscriptions as $key => $value) {
                                if($value['price']==0){
                                    echo '<td><span class="heading_col">Free</span></td>';
                                }
                                else{
                                    echo '<td>$ '.str_pad($value['price'], 3, "0", STR_PAD_LEFT).'</td>';
                                }
                                
                            }
                            ?>
                    </tr>
                    <tr>
                            <td class="tableheading">&nbsp;</td>
                            <?php
                            foreach ($subscriptions as $key => $value) {
                                echo '<td><a href="'.base_url('subscription/buy/'.$value['id']).'">Buy Now</a></td>';
                            }
                           
                            /*
                            <td><a href="javascript:void(0)" onfocus="this.blur();" onclick="openDiv('formdiv');">Sign Up Now</a></td>
                            <td>TBA</td>
                            <td>TBA</td>
                            <td>TBA</td>
                            <td>TBA</td>	
                             * 
                             * 
                             */		
                            ?>
                    </tr>
            </tbody>
          </table>
</div>