<?php
// ****************************************************************************
// 
//     User's subscriptions detail view
//
// ****************************************************************************


?>
<?php //echo '<pre>'; print_r($userSubscriptions); die; ?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addProjectmodal" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Add Project</h4>
                        </div>
                        <div class="modal-body">
                                <?php
                                    echo form_open_multipart(base_url("user/file_management"), array('onsubmit'=>'return chkaddProjectFolder()', 'id'=>'add_project'));

                                    echo '<label for="title">Title:</label>'; 
                                    echo '<input type="text" id="project_folder_title" value="" name="project_folder_title" class="form-control" placeholder="project Name">';

                                    echo form_close();
                                ?>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#add_project').submit();">Save changes</button>
                        </div>
                </div>
        </div>
</div>
<div class="panel-piluku">
        <div class="col-md-12 panel-piluku">
                <!--                        *** Pricing Tables ***-->
                <!--pricing table-->
                <div class="form-heading">
                        File Management
                </div>
                
                <button class="btn btn-primary" data-target="#addProjectmodal" data-toggle="modal">
                        <i class="ion ion-plus"></i>
                        <span>Add Project</span>
                </button>
                
                <button class="btn btn-red disabled" id="deletechk">
                        <i class="ion ion-minus"></i>
                        <span>Delete Selected</span>
                </button>
                <!--pricing table-->
                <!--                               row-->
                <div class="row panel-body">
                        <div class="table-responsive">
                            <form name="fileManageForm" id="fileManageForm" method="POST" action="">
                                <input type="hidden" name="type" value="project" id="type" />
                                <table class="table table-bordered">
                                    <thead>
                                            <tr>
                                                    <th>
                                                        <input type="checkbox" name="select_all_chk" id="select_all_chk" value="yes">
                                                        <label class="margin-zero" for="select_all_chk"><span></span></label>
                                                    </th>
                                                    <th>Project Name</th>
                                                    <th>Type</th>
                                                    <th>Size</th>
                                                    <th>Date</th>
                                                    <th>Action</th>
                                            </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if(count($list_projects)>0){
                                            $i=1;
                                            foreach ($list_projects as $key => $value) {
                                                $filesize=$value['size']/1024;
                                                $filesize=round($filesize, 2);
                                                $filesize=$filesize.' MB';
                                                $status=$value['status'];
                                        ?>
                                            <tr id="projectrow_<?=$value['id']; ?>">
                                                    <th scope="row">
                                                        <?php /*
                                                        <input type="checkbox" name="selectFolder[]" value="<?=$value['id']; ?>" class="checkbox">
                                                         */ ?> 
                                                         
                                                        <input type="checkbox" aria-label="Checkbox for following text input" id="c<?=$value['id']; ?>" name="selectFolder[]" value="<?=$value['id']; ?>" class="checkbox">
                                                        <label class="margin-zero" for="c<?=$value['id']; ?>"><span></span></label>
                                                    </th>
                                                    <td>
                                                            <i class="fa fa-folder-open" ondblclick="window.location.href='<?=base_url('user/file_management/'.$value['id']);?>'"></i> 
                                                            <span class="project_title" id="project_title_<?=$value['id']; ?>" ondblclick="showrename(<?=$value['id']; ?>)">
                                                            <?=ucwords(strtolower($value['title'])); ?>
                                                        </span>
                                                        <input type="text" name="foldername" id="foldernametxt_<?=$value['id']; ?>" value="<?=$value['title']; ?>" style="display: none; width: 130px;" onchange="renamefolder(<?=$value['id']; ?>, 'project')" class="foldername form" />
                                                        <input type="hidden" name="folderid" id="folderidtxt_<?=$value['id']; ?>" value="<?=$value['id']; ?>" />
                                                    </td>
                                                    <td><?php echo 'Folder'; ?></td>
                                                    <td><?=$filesize; ?></td>
                                                    <td><?=date('d-M-Y', strtotime($value['added_date']));?></td>
                                                    <td>
                                                        <a href="<?=base_url('user/file_management/'.$value['id']);?>">Open</a> | 
                                                        <a href="javascript:void(0)" onclick="showrename(<?=$value['id']; ?>)">Edit</a> | 
                                                        <span class="deletetxt">
                                                            <?php if($status=='1'){ ?>
                                                            <a href="javascript:void(0)" onclick="deleteProject(<?=$value['id']; ?>)">Delete</a>
                                                            <?php } else{ ?>
                                                            <a href="javascript:void(0)" onclick="undoProject(<?=$value['id']; ?>)">Undo</a>
                                                            <?php } ?>
                                                        </span>
                                                    </td>
                                                 
                                            </tr>
                                        <?php
                                        $i++;
                                        }
                                        }
                                        ?>
                                            
                                    </tbody>
                            </table>
                            </form>
                    </div>
                </div>
                <!--                                /row-->
                
                <!--                               row-->
                <div class="row">
					<!--                                   col md 4-->
                
                
                </div>
                <!--                                /row-->
        </div>
</div>
<script>
    
    function chkaddProjectFolder(){
        var title=$('#project_folder_title').val();
        if(title=='' || title==null){
            $('#project_folder_title').css('border', '1px solid red');
            $('#project_folder_title').focus();
            return false;
        }
    }
    
    function showrename(id){
        $('.project_title').show();
        $('.foldername').hide();
        $('#project_title_'+id).hide();
        $('#foldernametxt_'+id).show();
    }
    
    function renamefolder(id, type){
        var name=$('#foldernametxt_'+id).val();
        var id=$('#folderidtxt_'+id).val();
        $.ajax({
            url:base_url+"user/file_management/",
            type: 'POST',
            data: 'rename_folder='+name+'&type='+type+'&id='+id,
            cache: false,
            global: false,
            success:function(msg){ 
                if(msg=='success'){
                    $('#project_title_'+id).html($('#foldernametxt_'+id).val());
                    $('#project_title_'+id).show();
                    $('#foldernametxt_'+id).hide();
                }
                else{

                }
                $('#adduserHtml').html(msg);
            }    			
        }); 
    }
    
    function deleteProject(id){
         var r = confirm("Are you sure to remove this?");
        if (r == true) {
            $.ajax({
                url:base_url+"project/remove/"+id+"/no",
                type: 'POST',
                cache: false,
                global: false,
                success:function(msg){ 
                    $('#projectrow_'+id+' .deletetxt').html('<a href="javascript:void(0)" onclick="undoProject('+id+')">Undo</a>');
                }    			
            }); 
        }
    }
    
    function undoProject(id){
         var r = confirm("Are you sure to undo this?");
        if (r == true) {
            $.ajax({
                url:base_url+"project/undo/"+id,
                type: 'POST',
                cache: false,
                global: false,
                success:function(msg){ 
                    $('#projectrow_'+id+' .deletetxt').html('<a href="javascript:void(0)" onclick="deleteProject('+id+')">Delete</a>');
                }    			
            }); 
        }
    }
</script>
