<?php
// ****************************************************************************
// 
//     User's subscriptions detail view
//
// ****************************************************************************


?>

<div class="panel-piluku">
        <div class="col-md-12 panel-piluku">
                <!--                        *** Pricing Tables ***-->
                <!--pricing table-->
                <div class="form-heading">
                        Aliases
                </div>
                
                
                <div class="row panel-body">
                    <a href="<?=base_url('user/file_management') ?>"><i class="ion-ios-home"></i></a>/<a href="<?=base_url('user/file_management/'); ?>">File Management</a>
                </div>
                <!--pricing table-->
                <!--                               row-->
                <div class="row panel-body">
                    <div id="frm">
                        <form method="post" name="aliases_frm" id="aliases_frm" class="form">
                            <input type="hidden" name="type" id="type" value="<?=$type; ?>" />
                            <input type="hidden" name="fileid" id="fileid" value="<?=$id; ?>" />
                            <div class="floatLeft">
                                <label>Select Project: </label>
                                <div class="styled-select filter_width">
                                    <div class="form-group">
                                        <select name="projects" id="projects" onchange="showAlbums('<?=$type ?>', this.value)" class="name_search form-control">
                                            <option value="">Select Project</option>
                                            <?php 
                                            foreach ($list_projects as $key => $value) {
                                                echo '<option value="'.$value['id'].'">'.$value['title'].'</option>';
                                            } 
                                            ?>
                                        </select>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="clear">&nbsp;</div>
                            <div class="floatLeft" id="album_list" style="display: none">
                                <label>Select <?=ucwords($type); ?> Albums: </label>
                                <div class="styled-select filter_width" id="albumsListing">

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--                                /row-->
                
                <!--                               row-->
                <div class="row">
					<!--                                   col md 4-->
                
                
                </div>
                <!--                                /row-->
        </div>
</div>
<script>
    function showAlbums(type, id){
        var html='There is some error';
        $.ajax({
            url:base_url+"user/getAlbums/",
            type: 'POST',
            data: 'type='+type+'&id='+id,
            cache: false,
            global: false,
            success:function(msg){ 
                html='';
                var myArray = jQuery.parseJSON(msg);
                if(myArray.length>0){ 
                    html+='<div class="form-group">';
                    html+='<select name="albums" id="albums" class="name_search form-control">';           
                    for(var i=0; i<myArray.length;i++){
                        html+='<option value='+myArray[i]['id']+'>'+myArray[i]['name']+'</option>';
                    }
                    html+='</select>';
                    html+='</div>';
                    $('#albumsListing').html(html);
                    if($('#savealiases').length==0){
                        $('#album_list').after('<div class="clear" id="savealiases"><input type="button" name="save_aliases" id="save_aliases" value="Save Aliases" class="btn btn-primary"></div>')
                    }
                    
                    
                }
                else{
                    $('#savealiases').remove();
                    $('#albumsListing').html('There is not any albums for this type')
                }
                
                $('#album_list').show();
            }    			
        });
    }
    
    $('#aliases_frm').on('click', '#save_aliases', function(){
        $('#aliases_frm').submit();
    })
    
</script>
<script src='<?=base_url(); ?>assets/js/select2.js'></script>
<script src='<?=base_url(); ?>assets/js/jquery.multi-select.js'></script>
<script src='<?=base_url(); ?>assets/js/form-elements.js'></script>