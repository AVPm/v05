<?php
// ****************************************************************************
// 
//     User's subscriptions detail view
//
// ****************************************************************************


?>
<?php //echo '<pre>'; print_r($userSubscriptions); die; ?>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addAlbumModal" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Add Album</h4>
                        </div>
                        <div class="modal-body">
                                <?php
                                    echo form_open_multipart(base_url("user/file_management/".$projectid), array('onsubmit'=>'return chkaddProjectFolder()', 'id'=>'add_project_album'));

                                    echo '<label for="project_album_title">Name:</label>'; 
                                    echo '<input type="text" id="project_album_title" value="" name="project_album_title" class="form-control" placeholder="Album Name">';
                                    echo '</br>';
                                    echo '<label for="project_album_type">Type:</label>'; 
                                    echo '<select id="project_album_type" name="project_album_type" class="form-control">
                                             <option value="images">Images</option>
                                             <option value="videos">Videos</option>
                                             <option value="files">Files</option>
                                         </select>';

                                    echo form_close();
                                ?>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="$('#add_project_album').submit();">Save</button>
                        </div>
                </div>
        </div>
</div>
<div class="panel-piluku">
        <div class="col-md-12 panel-piluku">
                <!--                        *** Pricing Tables ***-->
                <!--pricing table-->
                <div class="form-heading">
                        File Management
                </div>
                <div class="row panel-body">
                    <button class="btn btn-primary" data-target="#addAlbumModal" data-toggle="modal">
                            <i class="ion ion-plus"></i>
                            <span>Add Album</span>
                    </button>

                    <button class="btn btn-red disabled" id="deletechk">
                            <i class="ion ion-minus"></i>
                            <span>Delete Selected</span>
                    </button>
                </div>
                <div class="row panel-body">
                    <a href="<?=base_url('user/file_management') ?>"><i class="ion-ios-home"></i></a>/<a href="#"><?=$project_name; ?></a>
                </div>
                
               <!--pricing table-->
                <!--                               row-->
                <div class="row panel-body">
                        <div class="table-responsive">
                            <form name="fileManageForm" id="fileManageForm" method="POST" action="">
                                <input type="hidden" name="type" value="album" id="type" />
                                <table class="table table-bordered">
                                    <thead>
                                            <tr>
                                                    <th>
                                                        <input type="checkbox" name="select_all_chk" id="select_all_chk" value="yes">
                                                        <label class="margin-zero" for="select_all_chk"><span></span></label>
                                                    </th>
                                                    <th>Album Name</th>
                                                    <th>Type</th>
                                                    <th>Size</th>
                                                    <th>Date</th>
                                                    <th>Action</th>
                                            </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if(count($list_albums)>0){
                                            $i=1;
                                            foreach ($list_albums as $key => $value) {
                                                $filesize=$value['size']/1024;
                                                $filesize=round($filesize, 2);
                                                $filesize=$filesize.' MB';
                                                $status=$value['status'];
                                        ?>
                                            <tr id="projectrow_<?=$value['type'].'_'.$value['id']; ?>">
                                                    <th scope="row">
                                                        <?php /*
                                                        <input type="checkbox" name="selectFolder[]" value="<?=$value['id']; ?>" class="checkbox">
                                                         */ ?> 
                                                        <input type="checkbox" id="c<?=$value['type'].'_'.$value['id']; ?>" name="selectFolder[]" value="<?=$value['type'].'_'.$value['id']; ?>" class="checkbox">
                                                        <label class="margin-zero" for="c<?=$value['type'].'_'.$value['id']; ?>"><span></span></label>
                                                    </th>
                                                    <td>
                                                            <i class="fa fa-folder-open" ondblclick="window.location.href='<?=base_url('user/file_management/'.$projectid.'/'.$value['id'].'/'.$value['type']);?>'"></i> 
                                                            <span class="project_title" id="project_title_<?=$value['type'].'_'.$value['id']; ?>" ondblclick="showrename(<?=$value['id']; ?>, '<?=$value['type']; ?>')">
                                                            <?=ucwords(strtolower($value['name'])); ?>
                                                        </span>
                                                        <input type="text" name="foldername" id="foldernametxt_<?=$value['type'].'_'.$value['id']; ?>" value="<?=$value['name']; ?>" style="display: none; width: 130px;" onchange="renamefolder(<?=$value['id']; ?>, '<?=$value['type']; ?>', 'album-<?=$value['type']; ?>')" class="foldername" />
                                                        <input type="hidden" name="folderid" id="folderidtxt_<?=$value['type'].'_'.$value['id']; ?>" value="<?=$value['id']; ?>" />
                                                    </td>
                                                    <td><?php echo ucwords($value['type']).' Folder'; ?></td>
                                                    <td><?=$filesize; ?></td>
                                                    <td><?=date('d-M-Y', strtotime($value['added_date']));?></td>
                                                    <td>
                                                        <a href="<?=base_url('user/file_management/'.$projectid.'/'.$value['id'].'/'.$value['type']);?>">Open</a> | 
                                                        <a href="javascript:void(0)" onclick="showrename(<?=$value['id']; ?>, '<?=$value['type']; ?>')">Edit</a> | 
                                                        <span class="deletetxt">
                                                            <?php if($status=='1'){ ?>
                                                            <a href="javascript:void(0)" onclick="removeProjectalbum(<?=$value['id']; ?>, '<?=$value['type']; ?>')">Delete</a>
                                                            <?php } else{ ?>
                                                            <a href="javascript:void(0)" onclick="undoProjectalbum(<?=$value['id']; ?>, '<?=$value['type']; ?>')">Undo</a>
                                                            <?php } ?>
                                                        </span>
                                                    </td>
                                                 
                                            </tr>
                                        <?php
                                        $i++;
                                        }
                                        }
                                        ?>
                                            
                                    </tbody>
                            </table>
                            </form>
                    </div>
                </div>
                <!--                                /row-->
                
                <!--                               row-->
                <div class="row">
					<!--                                   col md 4-->
                
                
                </div>
                <!--                                /row-->
        </div>
</div>
<script>
    
    function chkaddProjectFolder(){
        var title=$('#project_album_title').val();
        if(title=='' || title==null){
            $('#project_album_title').css('border', '1px solid red');
            $('#project_album_title').focus();
            return false;
        }
    }
    
    function showrename(id, type){
        $('.project_title').show();
        $('.foldername').hide();
        $('#project_title_'+type+'_'+id).hide();
        $('#foldernametxt_'+type+'_'+id).show();
    }
    
    function removeProjectalbum(albumid, table){
        var r = confirm("Are you sure to remove this?");
        
        if (r == true) {
            $.ajax({
                url:base_url+"project/removealbum/",
                type: 'POST',
                data: 'albumid='+albumid+"&table="+table,
                cache: false,
                global: false,
                success:function(msg){ 
                    //$('#projectrow_'+table+'_'+albumid).remove();
                    $('#projectrow_'+table+'_'+albumid+' .deletetxt').html('<a href="javascript:void(0)" onclick="undoProjectalbum('+albumid+', \''+table+'\')">Undo</a>');
                }    			
            });
        } else {
            return false;
        }
    }
    
    function undoProjectalbum(albumid, table){
         var r = confirm("Are you sure to undo this?");
        if (r == true) {
            $.ajax({
                url:base_url+"project/undoalbum/",
                type: 'POST',
                data: 'albumid='+albumid+"&table="+table,
                cache: false,
                global: false,
                success:function(msg){ 
                    $('#projectrow_'+table+'_'+albumid+' .deletetxt').html('<a href="javascript:void(0)" onclick="removeProjectalbum('+albumid+', \''+table+'\')">Delete</a>');
                }    			
            }); 
        }
    }
    
    function renamefolder(id, type, albumtype){
        var name=$('#foldernametxt_'+type+'_'+id).val();
        var id=$('#folderidtxt_'+type+'_'+id).val();
        $.ajax({
            url:base_url+"user/file_management/",
            type: 'POST',
            data: 'rename_folder='+name+'&type='+albumtype+'&id='+id,
            cache: false,
            global: false,
            success:function(msg){ 
                if(msg=='success'){
                    $('#project_title_'+type+'_'+id).html($('#foldernametxt_'+type+'_'+id).val());
                    $('#project_title_'+type+'_'+id).show();
                    $('#foldernametxt_'+type+'_'+id).hide();
                }
                else{

                }
                $('#adduserHtml').html(msg);
            }    			
        }); 
    }
    
</script>
