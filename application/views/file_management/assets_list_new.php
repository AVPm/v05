<?php
// ****************************************************************************
// 
//     User's subscriptions detail view
//
// ****************************************************************************


?>
<script>
    var upload_url= "<?=base_url(); ?>project/album/<?=$download_type; ?>upload";
    var curr_album='<?=@$curr_albumid; ?>';
</script>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addAssetsModal" class="modal fade modal-full-pad">
        <div class="modal-dialog modal-full">
                <div class="modal-content">
                        <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span class="ti-close" aria-hidden="true"></span></button>
                                <h4 id="myModalLabel" class="modal-title">Add <?=ucwords($download_type); ?></h4>
                        </div>
                        <div class="modal-body">
                                <form action="" enctype="multipart/form-data" method="post" id="form1">

                                    <div id="fileuploader">
                                        <iframe src="<?=base_url('includes/file_uploader')  ?>" style="height: 250px; width:100%" frameborder="0" ></iframe> 
                                    </div>    


                                </form>
                        </div>
                        <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                <button class="btn btn-primary" type="button" onclick="window.location.reload()">Save</button>
                        </div>
                </div>
        </div>
</div>
<?php
if($this->session->flashdata('aliases_success')!=''){
?>
<div tabindex="-1" class="sweet-overlay" id="sweet-overlay" style="opacity: 1.14; display: block;"></div>
<div id="sweet-alert" class="sweet-alert showSweetAlert visible" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block; margin-top: -169px;"><div class="sa-icon sa-success animate" style="display: block;">
      <span class="sa-line sa-tip animateSuccessTip"></span>
      <span class="sa-line sa-long animateSuccessLong"></span>

      <div class="sa-placeholder"></div>
      <div class="sa-fix"></div>
    </div><h2><?=$this->session->flashdata('aliases_success'); ?></h2>
    
    <div class="sa-button-container">
        <button tabindex="1" onclick="closeDiv('sweet-overlay'); closeDiv('sweet-alert');" class="confirm" style="display: inline-block; background-color: rgb(174, 222, 244); box-shadow: 0px 0px 2px rgba(174, 222, 244, 0.8), 0px 0px 0px 1px rgba(0, 0, 0, 0.05) inset;">OK</button>
    </div>
</div>
<?php
    //echo '<div class="clear">'.$this->session->flashdata('aliases_success').'</div>';
    //echo '<div class="clear">&nbsp;</div>';
}
?>
<div class="panel-piluku">
        <div class="col-md-12 panel-piluku">
                <!--                        *** Pricing Tables ***-->
                <!--pricing table-->
                <div class="form-heading">
                        File Management
                </div>
                
                <div class="row panel-body">
                    <button class="btn btn-primary" data-target="#addAssetsModal" data-toggle="modal">
                            <i class="ion ion-plus"></i>
                            <span>Add <?=ucwords($download_type); ?></span>
                    </button>

                    <button class="btn btn-red disabled" id="deletechk">
                            <i class="ion ion-minus"></i>
                            <span>Delete Selected</span>
                    </button>
                </div>
                
                <div class="row panel-body">
                    <a href="<?=base_url('user/file_management') ?>"><i class="ion-ios-home"></i></a>/<a href="<?=base_url('user/file_management/'.$projectid); ?>"><?=ucwords(strtolower($project_name)); ?></a> / <a href="#"><?=ucwords(strtolower($album_name)); ?></a>
                </div>
                <!--pricing table-->
                <!--                               row-->
                <div class="row panel-body">
                        <div class="table-responsive">
                            <form name="fileManageForm" id="fileManageForm" method="POST" action="">
                                <input type="hidden" name="type" value="assets" id="type" />
                                <input type="hidden" name="assets_type" value="<?=$download_type; ?>" id="assets_type" />
                                <table class="table table-bordered">
                                    <thead>
                                            <tr>
                                                    <th>
                                                        <input type="checkbox" name="select_all_chk" id="select_all_chk" value="yes">
                                                        <label class="margin-zero" for="select_all_chk"><span></span></label>
                                                    </th>
                                                    <th>Album Name</th>
                                                    <th>Type</th>
                                                    <th>Size</th>
                                                    <th>Date</th>
                                                    <th>Action</th>
                                            </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if(count($list_albums)>0){
                                            $i=1;
                                            foreach ($list_albums as $key => $value) {
                                                if($value['name']!=''){
                                                    $filesize=$value['size']/1024;
                                                    $filesize=round($filesize, 2);
                                                    $filesize=$filesize.' MB';
                                                    $filename=(strlen($value['name'])>30) ? substr($value['name'], 0, 30).'...' : $value['name'];
                                                    $path_parts = pathinfo($value['name']);
                                                    //echo '<pre>'; print_r($path_parts); die();
                                                    $type= $path_parts['extension'];
                                                    switch ($type) {
                                                        case 'mp4':
                                                            $icon='mp4_icon.png';
                                                        break;

                                                        case 'docx':
                                                        case 'doc':
                                                            $icon='docx_icon.png';
                                                        break;

                                                        case 'xlsx':
                                                        case 'xlx':
                                                            $icon='xlsx_icon.png';
                                                        break;

                                                        case 'jpg':
                                                        case 'jpeg':
                                                            $icon='jpg_icon.png';
                                                        break;

                                                        case 'txt':
                                                            $icon='txt_icon.png';
                                                        break;

                                                        case 'pdf':
                                                            $icon='pdf_icon.png';
                                                        break;

                                                        case 'png':
                                                            $icon='png_icon.png';
                                                        break;

                                                        case 'gif':
                                                            $icon='gif_icon.png';
                                                        break;

                                                        default:
                                                            $icon='default_file_icon.png';
                                                        break;
                                                    }
                                                    $status=$value['status'];
                                            ?>
                                                <tr id="projectrow_<?=$value['id']; ?>">
                                                        <th scope="row">
                                                            <?php /*
                                                            <input type="checkbox" name="selectFolder[]" value="<?=$value['id']; ?>" class="checkbox">
                                                             */ ?> 
                                                            <input type="checkbox" id="c<?=$value['id']; ?>" name="selectFolder[]" value="<?=$value['id']; ?>" class="checkbox">
                                                            <label class="margin-zero" for="c<?=$value['id']; ?>"><span></span></label>
                                                        </th>
                                                        <td>
                                                                <img src="<?=base_url('images/'.$icon); ?>" height="24" width="24">
                                                                <span class="project_title" id="project_title_<?=$value['id']; ?>" ondblclick="showrename(<?=$value['id']; ?>)" title="<?=ucwords(strtolower($value['name'])); ?>"><?=ucwords(strtolower($filename)); ?></span>
                                                                <input type="text" name="foldername" id="foldernametxt_<?=$value['id']; ?>" value="<?=$value['name']; ?>" style="display: none; width: 130px;" onchange="renamefolder(<?=$value['id']; ?>, '<?=$value['type']; ?>')" class="foldername" />
                                                                <input type="hidden" name="folderid" id="folderidtxt_<?=$value['id']; ?>" value="<?=$value['id']; ?>" />
                                                        </td>
                                                        <td><?php echo $type; ?></td>
                                                        <td><?=$filesize; ?></td>
                                                        <td><?=date('d-M-Y', strtotime($value['added_date']));?></td>
                                                        <td>
                                                            <a href="<?=base_url('project/download/'.$download_type.'/'.$value['id']);?>" target="_blank">Download</a> | 
                                                            <a href="javascript:void(0)" onclick="showrename(<?=$value['id']; ?>)">Edit</a> | 
                                                            <span class="deletetxt">
                                                                <?php if($status=='1'){ ?>
                                                                <a href="javascript:void(0)" onclick="removeAlbumFile(<?=$value['id']; ?>, '<?=$value['type']; ?>')">Delete</a>
                                                                <?php } else{ ?>
                                                                <a href="javascript:void(0)" onclick="undoAlbumFile(<?=$value['id']; ?>, '<?=$value['type']; ?>')">Undo</a>
                                                                <?php } ?>
                                                            </span>
                                                            <span class="aliasestxt">
                                                                <?php if($value['aliases']==0 && $status=='1'){ ?>
                                                                | <a href="<?=base_url('user/aliases/'.$value['type'].'/'.$value['id']); ?>">Aliases</a>
                                                                <?php } ?>
                                                            </span>
                                                        </td>

                                                </tr>
                                            <?php
                                            $i++;
                                                }
                                                
                                        }
                                        }
                                        ?>
                                            
                                    </tbody>
                            </table>
                            </form>
                    </div>
                </div>
                <!--                                /row-->
                
                <!--                               row-->
                <div class="row">
					<!--                                   col md 4-->
                
                
                </div>
                <!--                                /row-->
        </div>
</div>
<script>
    
   
    function chkaddProjectFolder(){
        var title=$('#project_album_title').val();
        if(title=='' || title==null){
            $('#project_album_title').css('border', '1px solid red');
            $('#project_album_title').focus();
            return false;
        }
    }
    
    function showrename(id){
        $('.project_title').show();
        $('.foldername').hide();
        $('#project_title_'+id).hide();
        $('#foldernametxt_'+id).show();
    }
    
    function removeAlbumFile(albumid, table){
        var r = confirm("Are you sure to remove this?");
        
        if (r == true) {
            $.ajax({
                url:base_url+"project/removealbumimage/",
                type: 'POST',
                data: 'imageid='+albumid+"&type="+table,
                cache: false,
                global: false,
                success:function(msg){ 
                    //$('#projectrow_'+albumid).remove();
                    $('#projectrow_'+albumid+' .deletetxt').html('<a href="javascript:void(0)" onclick="undoAlbumFile('+albumid+', \''+table+'\')">Undo</a>');
                    $('#projectrow_'+albumid+' .aliasestxt').hide();
                }    			
            });
        } else {
            return false;
        }
    }
    
    function undoAlbumFile(albumid, table){
        var r = confirm("Are you sure to undo this?");
        
        if (r == true) {
            $.ajax({
                url:base_url+"project/undoalbumimage/",
                type: 'POST',
                data: 'imageid='+albumid+"&type="+table,
                cache: false,
                global: false,
                success:function(msg){ 
                    //$('#projectrow_'+albumid).remove();
                    $('#projectrow_'+albumid+' .deletetxt').html('<a href="javascript:void(0)" onclick="removeAlbumFile('+albumid+', \''+table+'\')">Delete</a>');
                    $('#projectrow_'+albumid+' .aliasestxt').show();
                }    			
            });
        } else {
            return false;
        }
    }
    
    function renamefolder(id, type){
        var name=$('#foldernametxt_'+id).val();
        var id=$('#folderidtxt_'+id).val();
        $.ajax({
            url:base_url+"user/file_management/",
            type: 'POST',
            data: 'rename_folder='+name+'&type='+type+'&id='+id,
            cache: false,
            global: false,
            success:function(msg){ 
                if(msg=='success'){
                    $('#project_title_'+id).html($('#foldernametxt_'+id).val());
                    $('#project_title_'+id).show();
                    $('#foldernametxt_'+id).hide();
                }
                else{

                }
                $('#adduserHtml').html(msg);
            }    			
        }); 
    }
    
</script>

<script src='<?=base_url(); ?>assets/js/sweet-alert/sweetalert.min.js'></script>
<script src='<?=base_url(); ?>assets/js/sweet-alerts.js'></script>
