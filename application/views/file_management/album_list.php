<?php
// ****************************************************************************
// 
//     User's album list detail view
//
// ****************************************************************************


?>
<style>
    .bottom_table tr th{
        font-size: 12px;
    }
    .tableheading{
        width: auto !important;
    }
    .bottom_table tr td{background: #242424 none repeat scroll 0 0 !important}
</style>

<!-- add project HTML -->
<div class="popup" id="addProjectFolder_div" style="width: 640px;">
    <div class="popup_head">    
           <span>Add a new Album</span>
    </div>
    
    <div class="popup_forms" style="padding:25px 15px 25px 25px">
       <?php
           echo form_open_multipart(base_url("user/file_management/".$projectid), array('onsubmit'=>'return chkaddProjectFolder()', 'id'=>'add_project_album'));
           
           echo '<label for="title">Name:</label>'; 
           echo '<input type="text" id="project_album_title" value="" name="project_album_title" style="margin-left:15px;">';
           echo '</br>';
           echo '<label for="title">Type:</label>'; 
           echo '<select id="project_album_type" name="project_album_type" style="margin-left:22px; color:#000">
                    <option value="images">Images</option>
                    <option value="videos">Videos</option>
                    <option value="files">Files</option>
                </select>';

           echo form_close();
       ?>

   </div>
   <div class="popup_bottom">
       <a class="but_red floatLeft" onfocus="this.blur();" href="javascript:void(0)" onclick="return closeDiv('addProjectFolder_div')">cancel</a>
       <a class="but_green floatRight" href="javascript:void(0)" onclick="$('#add_project_album').submit();">submit</a>
       <div class="clear">&nbsp;</div> 
   </div> 
</div>


<div class="clear">&nbsp;</div>

<div></div>
<h4>File Management</h4>
<div class="clear">&nbsp;</div>
<div>
    <a href="javascript:void(0)" style="color:#fff" onclick="openDiv('addProjectFolder_div')"><img src="<?=base_url('images/folder_add.png'); ?>"> Add Album</a>
</div>
<div class="clear">&nbsp;</div>
<div class="filepath"><a href="<?=base_url('user/file_management') ?>"><img src="<?=base_url('images/home_icon.png')?>" height="20" width="20"></a>/<a href="#"><?=$project_name; ?></a></div>
<div class="clear">&nbsp;</div>
<form name="fileManageForm" id="fileManageForm" method="POST" action="">
    <input type="hidden" name="type" value="album" id="type" />
    <input type="button" name="deletechk" id="deletechk" value="Delete Selected" disabled="disabled">
<table class="bottom_table">
    <tbody>
        <tr>
            <th style="width: 2px; "><input type="checkbox" name="select_all_chk" id="select_all_chk" value="yes"></th>
            <th style="width:350px;">Name</th>
            <th style="width:100px;">Type</th>
            <th style="width:100px;">Size</th>
            <th style="width:100px;">DATE</th>
            <th style="width:120px;">Action</th>
            
        </tr>
        <?php
        foreach ($list_albums as $key => $value) {
            $filesize=$value['size']/1024;
            $filesize=round($filesize, 2);
            $filesize=$filesize.' MB';
            $status=$value['status'];
        ?>
        <tr id="projectrow_<?=$value['type'].'_'.$value['id']; ?>">
            <td class="tableheading" style="width: 2px; text-align: center" align="center"><input type="checkbox" name="selectFolder[]" value="<?=$value['type'].'_'.$value['id']; ?>" class="checkbox"></td>
            <td class="tableheading"><img src="<?=base_url('images/closed_folder_yellow.png'); ?>" height="25" width="25" ondblclick="window.location.href='<?=base_url('user/file_management/'.$projectid.'/'.$value['id'].'/'.$value['type']);?>'">
                <span class="project_title" id="project_title_<?=$value['type'].'_'.$value['id']; ?>" ondblclick="showrename(<?=$value['id']; ?>, '<?=$value['type']; ?>')"><?=ucwords(strtolower($value['name'])); ?></span>
                <input type="text" name="foldername" id="foldernametxt_<?=$value['type'].'_'.$value['id']; ?>" value="<?=$value['name']; ?>" style="display: none; width: 130px;" onchange="renamefolder(<?=$value['id']; ?>, '<?=$value['type']; ?>', 'album-<?=$value['type']; ?>')" class="foldername" />
                <input type="hidden" name="folderid" id="folderidtxt_<?=$value['type'].'_'.$value['id']; ?>" value="<?=$value['id']; ?>" />
            </td>
            <td class="tableheading" style="text-align: right"><?php echo ucwords($value['type']).' Folder'; ?></td>
            <td class="tableheading" style="text-align: right"><?=$filesize; ?></td>
            <td class="tableheading" style="text-align: right"><?=date('d-M-Y', strtotime($value['added_date']));?></td>
            <td class="tableheading">
                <a href="<?=base_url('user/file_management/'.$projectid.'/'.$value['id'].'/'.$value['type']);?>">Open</a> | 
                <a href="javascript:void(0)" onclick="showrename(<?=$value['id']; ?>, '<?=$value['type']; ?>')">Edit</a> | 
                <span class="deletetxt">
                    <?php if($status=='1'){ ?>
                    <a href="javascript:void(0)" onclick="removeProjectalbum(<?=$value['id']; ?>, '<?=$value['type']; ?>')">Delete</a>
                    <?php } else{ ?>
                    <a href="javascript:void(0)" onclick="undoProjectalbum(<?=$value['id']; ?>, '<?=$value['type']; ?>')">Undo</a>
                    <?php } ?>
                </span>
                
            </td>
        </tr>
        <?php
        }
        ?>
        
</tbody>
    </table>
</form>
<script>
    
    function chkaddProjectFolder(){
        var title=$('#project_album_title').val();
        if(title=='' || title==null){
            $('#project_album_title').css('border', '1px solid red');
            $('#project_album_title').focus();
            return false;
        }
    }
    
    function showrename(id, type){
        $('.project_title').show();
        $('.foldername').hide();
        $('#project_title_'+type+'_'+id).hide();
        $('#foldernametxt_'+type+'_'+id).show();
    }
    
    function removeProjectalbum(albumid, table){
        var r = confirm("Are you sure to remove this?");
        
        if (r == true) {
            $.ajax({
                url:base_url+"project/removealbum/",
                type: 'POST',
                data: 'albumid='+albumid+"&table="+table,
                cache: false,
                global: false,
                success:function(msg){ 
                    //$('#projectrow_'+table+'_'+albumid).remove();
                    $('#projectrow_'+table+'_'+albumid+' .deletetxt').html('<a href="javascript:void(0)" onclick="undoProjectalbum('+albumid+', \''+table+'\')">Undo</a>');
                }    			
            });
        } else {
            return false;
        }
    }
    
    function undoProjectalbum(albumid, table){
         var r = confirm("Are you sure to undo this?");
        if (r == true) {
            $.ajax({
                url:base_url+"project/undoalbum/",
                type: 'POST',
                data: 'albumid='+albumid+"&table="+table,
                cache: false,
                global: false,
                success:function(msg){ 
                    $('#projectrow_'+table+'_'+albumid+' .deletetxt').html('<a href="javascript:void(0)" onclick="removeProjectalbum('+albumid+', \''+table+'\')">Delete</a>');
                }    			
            }); 
        }
    }
    
    function renamefolder(id, type, albumtype){
        var name=$('#foldernametxt_'+type+'_'+id).val();
        var id=$('#folderidtxt_'+type+'_'+id).val();
        $.ajax({
            url:base_url+"user/file_management/",
            type: 'POST',
            data: 'rename_folder='+name+'&type='+albumtype+'&id='+id,
            cache: false,
            global: false,
            success:function(msg){ 
                if(msg=='success'){
                    $('#project_title_'+type+'_'+id).html($('#foldernametxt_'+type+'_'+id).val());
                    $('#project_title_'+type+'_'+id).show();
                    $('#foldernametxt_'+type+'_'+id).hide();
                }
                else{

                }
                $('#adduserHtml').html(msg);
            }    			
        }); 
    }
    
</script>
