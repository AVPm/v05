<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * subscription model class file
 *
 */

class Subscription_model extends CI_Model {
    
    public function __construct()
    {
        /*
         * call the constructor of CI
         */
        parent::__construct();
    }
    
    /**
     * Function for get details of subscription pack
     * @param string $packid id of subscription pack
     *
     * @access public
     * 
     * @return details in form of multidimensional associative array
     **/  
    public function getPackDetails($packid) {
        $this->db->where('id', $packid);
        $query = $this->db->get('subscription');
        $data= array();
        //check number of rows of "projects" table
        if($query->num_rows() > 0)
        {
            $data = $query->result_array();
        }
        return $data;
    }
    
    /**
     * Function for insert order in sales table
     * @param string $packid id of subscription pack
     *
     * @access public
     * 
     * @return order id 
     **/  
    public function insertOrder($packId, $invoice) {
        $data = array(
                'userid' => $this->session->userdata('user_id'),
                'invoice'=>$invoice,
                'subscription_id' => $packId,
                'order_date' => date('Y-m-d H:i:s')
             );
        $this->db->insert('sales',$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    
     /**
        * Function for get all details from "users" tabel
        *
        * @param int $userid user's primary id from "users" table
        * 
        * @access public
        * 
        * @return array Containing a multidimensional associative array with the recordsets 
       **/
    public function user_details($userid) {
        $this->db->where('id', $userid);
        $this->db->select('first_name, last_name, company_name, stage_name, email');
        $q = $this->db->get('users');
        $data=  array();
        /* after we've made the queries from the database, we will store them inside a variable called $data, and return the variable to the controller */
        if($q->num_rows() > 0)
        {
          // we will store the results in the form of class methods by using $q->result()
          // if you want to store them as an array you can use $q->result_array()
          foreach ($q->result_array() as $row)
          {
            $data['details'] = $row;
          }
        }
        return $data;
    }
	
}
?>