<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');


function getUserInfo($userid) {
	$returndata=  array();
        if($userid>0){
            $ci=& get_instance();
            $ci->load->database();
            $ci->db->where('id', $userid);
            $ci->db->select('first_name, last_name, company_name, stage_name, email');
            $q = $ci->db->get('users');
            $data=  array();
            /* after we've made the queries from the database, we will store them inside a variable called $data, and return the variable to the controller */
            if($q->num_rows() > 0)
            {
              // we will store the results in the form of class methods by using $q->result()
              // if you want to store them as an array you can use $q->result_array()
              foreach ($q->result_array() as $row)
              {
                $data['details'] = $row;
              }
            }
            $returndata= $data;
        }
        return $returndata;
	
}

function getUserSubsriptionInfo($userid){
    $returndata=  array();
    if($userid>0){
        //$ci=get_instance();
        //$CI->load->model('user_model');
        
        $CI =& get_instance();
        $CI->load->model('user_model');
        //$CI->my_model->do_something();
        //die('coming soon');
        //$this->load->library('../controllers/project');
        //$this->user->welcome($roleid, $roleid2);
        //$place_order_msg=($this->session->flashdata('place_order')!='') ? $this->session->flashdata('place_order') : '';

        //$data['top_menu']= $this->project->topMenu();
        $userSubscriptions=$CI->user_model->getUserSubscriptions($userid);
        $remainingUsers=$CI->user_model->getRemainingUsers($userid);
        $trafficSize=$CI->user_model->getRemianingTraffic($userid);
        $trafficSize=number_format((($trafficSize/1024)/1024), 3);
        $subscriptions= $CI->user_model->subscriptions(1);
        //die('traffic: '.$trafficSize);
        //$data['selected_menu_subscrip']='yes';
        //$data['main_content']='subscriptions_view';
        //$data['main_content']='subscriptions_view_new';
        //$data['title']='Subscription';
        //$data['jsArray']=array('dashboard', 'jquery.mCustomScrollbar.concat.min');
        //$data['cssArray']=array('dashboard', 'jquery.mCustomScrollbar');
        //$data['data']=  array('userid'=>$userid, 'userSubscriptions'=>$userSubscriptions, 'remainingUsers'=>$remainingUsers, 'trafficSize'=>$trafficSize, 'subscriptions'=>$subscriptions);
        //$this->load->view('template', $data);
        //$this->load->view('template_new', $data);

        //$CI->my_model->remove_user($userid);
        $returndata['userSubscriptions']=$userSubscriptions;
        $returndata['remainingUsers']=$remainingUsers;
        $returndata['trafficSize']=$trafficSize;
        $returndata['subscriptions']=$subscriptions;
    }
    else{
        redirect('user');
    }
    return $returndata;
}
